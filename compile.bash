# Compiles four examples:
# OneMax problem
# MaxSAT problem
# TSP problem
# Protein Folding in a 2D square lattice HP model problem
#
# Please make sure that the directory for sourcepath and the java files (in fully qualified names) are compatible,
# that is, this file (compile.bash) must be on the same (parent) directory as the MAFRA framework.
#

javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/OneMaxTest.java
javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/MaxSATTest.java
javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/TSPTestElitistGAMuPlusLambda.java
javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/MultiMemePF2DSquareRel.java

# Compiled *.class files are placed under the ../tmp/classes/ directory.
#
# To run, e.g., OneMaxTest.class go to directory ../tmp/ and type in the command line:
# java -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.OneMaxTest
#
# Other executables may require additional arguments (to find out what the
# arguments are used for, look for 'argv[' in the above Test java files), e.g.:


# uf50-01.cnf is an instance of MAX-SAT in cnf format
#java  -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.MaxSATTest\ ~/FOLDER_WITH_INSTANCES/uf50-01.cnf

# eil51.tsp is an instance of TSP in TSPLib format
#java  -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.TSPTestElitistGAMuPlusLambda ~/FOLDER_WITH_INSTANCES/eil51.tsp TEST 12345 1000 1000 true

#java  -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.MultiMemePF2DSquareRel PPPHHPPHHHHPPHHHPHHPHHPHHHHPPPPPPPPHHHHHHPPHHHHHHPPPPPPPPPHPHHPHHHHHHHHHHHPPHHHPHHPHPPHPHHHPPPPPPHHH TEST 12345 2000 2000 50 true true 0.4 1 5 true 0.25 0.8 10000 true
