
package MAFRA.Population_Individual;


import java.util.*;
import java.awt.*;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;


public class StringIndividual extends Individual {

  private int numberGenerationsWithoutChange;
  private double temperature;


 public StringIndividual()
   {
     numberGenerationsWithoutChange=0;
     temperature = 0.0;
   }

 public StringIndividual(ProblemFactory aPF)
   {
     super(aPF);
     temperature = 0.0;
     numberGenerationsWithoutChange=0;
   }



 public boolean equals(Individual anIndividual)
   {

    return ((String)chromosome).equals((String)(anIndividual.getChromosome())) ;



   }





 public void setTemperature(double aTemp)
   {
     temperature = aTemp;
   }

 public double  getTemperature()
   {
     return temperature;
   }

 public void setChromosome(Object aChr)
   {

    chromosome = new String((String)aChr);



   }

 public int getNumberGenerationsWithoutChange()
   {
     return numberGenerationsWithoutChange;
   }

 public void setNumberGenerationsWithoutChange(int anInt)
   {
     numberGenerationsWithoutChange = anInt;
   }








 public Individual copyIndividual()
   {
     StringIndividual aNewOne;


     aNewOne = new StringIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());
     aNewOne.setChromosome(this.getChromosome());
     aNewOne.setNumberGenerationsWithoutChange(this.getNumberGenerationsWithoutChange());
     aNewOne.setTemperature(this.getTemperature());
     return (Individual)aNewOne;

   }


 public String chromosomeToString()
 {

      return (String)chromosome;

 }


 public void display()
   {
    System.out.println("|S="+size+",F="+(-fitness)+"|<"+chromosome+">");
   }



 public void display(TextArea myTextArea)
   {
     myTextArea.append("|S="+size+",F="+(-fitness)+"|<"+chromosome+">\n");
   }




}
