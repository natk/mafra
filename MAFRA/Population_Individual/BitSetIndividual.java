
package MAFRA.Population_Individual;

import java.util.BitSet;
import java.awt.TextArea;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;


public class BitSetIndividual extends Individual{

 public BitSetIndividual()
   {
   }

 public BitSetIndividual(ProblemFactory aPF)
   {
     super(aPF);
   }


 public void setChromosome(Object aChr)
   {
     chromosome = ((BitSet)aChr).clone();
   }


 public Individual copyIndividual()
   {
     BitSetIndividual aNewOne;
     BitSet           aNewC;

     aNewOne = new BitSetIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());

     aNewC = new BitSet();
     aNewC.or((BitSet)this.getChromosome()) ;

     aNewOne.setChromosome(aNewC);
     return (Individual)aNewOne;

   }



 public void display()
   {
    StringBuffer tmp;
    int i;
    BitSet aBS;
   
    aBS=(BitSet)(this.getChromosome());
    tmp = new StringBuffer();
    for (i=0;i<size;i++)
      {
	if(aBS.get(i))
	  {
	    tmp = tmp.append(1);
	  }
	else
	  {
	    tmp = tmp.append(0);
	  }
      }


     System.out.println("|S="+size+",F="+fitness+"|<"+tmp.toString()+">");
   }



 public void display(TextArea myTextArea)
   {
    StringBuffer tmp;
    int i;
    BitSet aBS;
   
    aBS=(BitSet)(this.getChromosome());
    tmp = new StringBuffer();
    for (i=0;i<size;i++)
      {
	if(aBS.get(i))
	  {
	    tmp = tmp.append(1);
	  }
	else
	  {
	    tmp = tmp.append(0);
	  }
      }


     myTextArea.append("|S="+size+",F="+fitness+"|<"+tmp.toString()+">\n");
   }



}
