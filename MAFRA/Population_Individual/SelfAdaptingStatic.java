
package MAFRA.Population_Individual;

/** Interface for all the subclasses of Individual that self adapt staticly.

  @version Memetic Algorithms Framework - V 1.0 - August 2000
  @author  Natalio Krasnogor
   */

public interface SelfAdaptingStatic{

  public void setHelperId(int helperId);
  public int getHelperId();

}
