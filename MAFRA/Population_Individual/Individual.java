package MAFRA.Population_Individual;

import Supporting.*;
import java.awt.TextArea;
import MAFRA.Visitor.*;
import MAFRA.Factory.*;
/** The Individual  class contains all the individual's  information.

    @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */

public  class Individual implements Supporting.Comparable, Cloneable, TemperatureAware{

  protected double fitness;
  protected Object chromosome;
  protected double size;

  /** Dummy constructor that allows for a later setting of the instance variables */
 public Individual()
   {
     fitness    = 0.0;
     chromosome = null;
     size       = 0.0;
   }

 /** Constructor that generates a new individual using the construction method provided in the problem factory. */
 public Individual(ProblemFactory aP)
   {

     chromosome = aP.newSolution();
     size       = aP.getSize();
     fitness    = aP.fitness(this);
   }


 public void setSize(double aSize)
   {
     size = aSize;
   }

 public double getSize()
  {
    return (size);
  }

 public void acceptVisitor(Visitor aVisitor){
   aVisitor.visitIndividual(this);
 }

 public void setFitness(double aFitness)
   {
     fitness = aFitness;
   }

 public double getFitness()
   {
     return fitness;
   }

 public void setChromosome(Object aChromosome)
   {
     chromosome = aChromosome;
   }

 public Object getChromosome()
  {
    return chromosome;
  }

 // this methods should be ovewriten in subclasses.
 public String chromosomeToString()
 {
   return "";
 }

 public void display()
   {};

 public void display(TextArea aTA)
   {};


   /** This method is part of the Comparable interface.
       @param rhs another individual to which compare the receiver fitness.
       @return 1 if the receiver fitness is smaller that rhs's fitness,-1 if it is BIGGER
               and 0 if both are equal.
       */
 public int compares(Supporting.Comparable rhs)
   {
     if (fitness<((Individual)rhs).getFitness())
       {
	 return (1);
       }
      else
	{
	  if(fitness>((Individual)rhs).getFitness())
	    {
	      return (-1);
	    }
	  else
	    {
	      return (0);
	    }
	}

   }


 /** This method is part of the Comparable interface.
     @param rhs another individual to which compare the receiver fitness.
     @return true if this object fitness is BIGGER than rhs's fitness.
             The semantic of this method has been inverted! */
 public  boolean lessThan(Supporting.Comparable rhs)
   {
     return(fitness>((Individual)rhs).getFitness());
   }


 /**
     @param rhs another individual to which compare the receiver fitness.
     @return true if this object fitness is BIGGER or EQUAL than rhs's fitness.
             The semantic of this method has been inverted! */
 public  boolean lessThanEq(Supporting.Comparable rhs)
   {
     return(fitness>=((Individual)rhs).getFitness());
   }


 /** This method is use to clone an Individual. It should be redefined by any subclass
     of Individual.*/
 public  Individual copyIndividual(){
   return null;
 }



 public boolean equals(Individual anIndividual)
   {
     // return ((fitness==anIndividual.getFitness()) && (size==anIndividual.getSize()) && (chromosome==anIndividual.getChromosome()));
 return (fitness==anIndividual.getFitness());
   }


 /** These methods are part of the TemperatureAware interface
     they must be redefined in subclasses */
   public void setTemperature(double aTemp)
     {

     }

   public double  getTemperature()
     {
       return 0.0;
     }


}
