

package MAFRA.Population_Individual;


import DataStructures.*;
import Exceptions.*;
import MAFRA.Visitor.*;
import MAFRA.Factory.*;


/** The Population class contains all the individuals and information regarding min,max,avg fitnesses,
    and acces methods.

    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public class StructuresPopulation{


 private List auxList;
  /* here we hold the individuals */
 private ListItr members;
 private double minFitness;
 private double maxFitness;
 private double sumFitness;
 private double membersNumber;
 private String name;


 public StructuresPopulation()
   {
     auxList       = new LinkedList( );
     members       = new LinkedListItr(auxList);
     membersNumber = 0;
     minFitness    = Double.POSITIVE_INFINITY;
     maxFitness    = Double.NEGATIVE_INFINITY;
     sumFitness    = 0.0;
     name          = null;
   };


 public StructuresPopulation(long aSize,IndividualFactory iF)
   {
     this();

     Individual anIndividual;
     long i;




     for (i=0;i<aSize;i++)
      {
	anIndividual = iF.createNewIndividual();
        this.addIndividual(anIndividual);
      }
     membersNumber = aSize;

   }
 /** This method resets min,max and total fitness of the population */
 public void resetFitnesses()
   {
     minFitness    = Double.POSITIVE_INFINITY;
     maxFitness    = Double.NEGATIVE_INFINITY;
     sumFitness    = 0.0;
   }

 public void setName(String aName)
   {
          name = aName;
   }

 public String getName()
   {
     return name;
   }

 public double getMaxFitness()
   {
     return maxFitness;
   }

 public double getMinFitness()
   {
     return minFitness;
   }

 public double getSumFitness()
   {
     return sumFitness;
   }
 

 public void first()
   {
     members.first();
   }

 public boolean isInList()
   {
     return members.isInList();
   }

 public void advance()
   {
     members.advance();
   }


 public void addIndividual(Individual anIndividual)
   {
     try
       {
	 members.insert((Object)anIndividual);
       }
     catch(ItemNotFound e)
       {
	  System.out.println("ERROR - exception "+e+" in addIndividual() in Population.java");
       }
	 membersNumber++;
	 updateMaxMinSumFitness(anIndividual);
   
   }

 /** This method add the individual to the population if it is not in yet. If it is added
     returns true, else false. */
 public boolean addIndividualIfNotInYet(Individual anIndividual)
   {
     boolean result=false;
     
     if(!isInPop(anIndividual))
       {
 	 members.zeroth();
	 addIndividual(anIndividual);
	 result= true;
       }
     else
       {
	   //	 	 System.out.println("Otro identico...");
	 result =false;
       }
	 
     return result;
    }

public boolean isInPop(Individual anIndividual)
  {
    Individual tmp;
    boolean found=false;

     for(members.first();members.isInList() && !found;members.advance())
	   {
	     tmp = (Individual)( members.retrieve());
	     found = tmp.equals(anIndividual);

	     // System.out.println(anIndividual.getFitness()+" == "+tmp.getFitness()+":"+found);
	   }


  
    return found;
  }

 public Individual retrieve()
   {
     Individual tmp;

       tmp = (Individual)members.retrieve();


     return tmp;
   }
 

 public void deleteIndividual(Individual anIndividual)
   {

     Individual tmp;
     double     fitnessTmp;
     boolean    OK;

     OK= true;
     try
       {
	 members.remove(anIndividual);
       }
     catch(ItemNotFound e)
       {
	  System.out.println("ERROR - exception "+e+" in deleteIndividual() in StructuresPopulation.java");
	  OK = false;
       }

     if(OK)
       {
	 membersNumber--;
	 sumFitness-=anIndividual.getFitness();

	 for(members.first();members.isInList();members.advance())
	   {
	     tmp = (Individual) members.retrieve();
	     fitnessTmp = tmp.getFitness();
	     if(maxFitness<fitnessTmp) 
	       {
		 maxFitness = fitnessTmp;
	       }
	     if(minFitness>fitnessTmp)
	       {
		 minFitness = fitnessTmp;
	       }
	   }
       }
     }


   

 public void acceptVisitor(Visitor aVisitor)
   {
     aVisitor.visitPopulation(this);
   }

 public void updateMaxMinSumFitness(Individual anIndividual)
   {
     double fitness;

     fitness = anIndividual.getFitness();
     if(maxFitness<fitness) 
       {
	 maxFitness = fitness;
       }
     if(minFitness>fitness)
       {
	 minFitness = fitness;
       }
     sumFitness +=fitness;

   }

 public double getAvgFitness()
   {
     return (sumFitness/membersNumber);
   }


 public double getMembersNumber()
   {
     return membersNumber;
   };



 /** Copies all the members of the receiving Population into dst 
 @param dst is the destiny Population where the members of the receiving Population will be copied. */
 public void copyTo(Population dst)
   {
   Individual tmpIndi;
   Individual fromPop;
   
   /* copies all  this.membersNumber individual */
       for(this.members.first();this.members.isInList();this.members.advance() )
	   {
	     fromPop = (Individual)(members.retrieve());
	     tmpIndi= fromPop.copyIndividual();
	     dst.addIndividual(tmpIndi);

	   }

      
      

   }

 /** Copies up to upTo members of the receiving Population into dst.
     @param dst is the destiny Population where the members of the receiving Population will be copied.
     @param upTo specify how many members are to be copied starting from the first(). */
 public void copyTo(Population dst, long upTo)
 {
   long i;
   Individual tmpIndi;
   Individual fromPop;

   i = 0;

   
   /* copies at most this.membersNumber individual */
       for(this.members.first();(this.members.isInList()&& (i<upTo));this.members.advance() )
	   {
	     fromPop = (Individual)(members.retrieve());
	     tmpIndi= fromPop.copyIndividual();
	     dst.addIndividual(tmpIndi);
	     i++;
	   }

      

 } 

 public Population keep(double aNum)
   {
     Population tmp;
     long i;
     
	 if(aNum<membersNumber)
	   {
	     tmp = new Population();
	     members.first();
	     for (i=0;i<aNum;i++)
	       {
		 tmp.addIndividual(((Individual)members.retrieve()).copyIndividual());
		 members.advance();
	       }
	   }
	 else
	   {
	     return this;
	   }
     	   
     return tmp;
       
   }

 
 protected ListItr getMembers()
   {
     return members;
   }

 public void extinct()
   {
     /*for(members.first();members.isInList();members.advance())
      {
	try
	  {
	    members.remove(members.retrieve());
	  }
	catch(ItemNotFound e)
	  {
	    System.out.println("ERROR: "+e+" in extinct() in Population.java");
	  }
      }*/
    auxList       = new LinkedList( );
    members       = new LinkedListItr(auxList);
    membersNumber = 0;
    minFitness    = Double.POSITIVE_INFINITY;
    maxFitness    = Double.NEGATIVE_INFINITY;
    sumFitness    = 0.0;

   }

}
