
package MAFRA.Population_Individual;


import java.util.*;
import java.awt.*;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;



public class PFStringIndividual extends StringIndividual{


  protected Vector points;
  protected Vector contacts;
  private static double scale;
  private static int diam;
  private static int origin_x;
  private static int origin_y;
  private Canvas myCanvas;




 public PFStringIndividual()
   {
    super();
    fitness = Double.NEGATIVE_INFINITY;
    chromosome = null;
    size    = 0 ;
    points = new Vector();
    contacts = new Vector();



     scale = 1;
     diam = 10;
     origin_x = 0;
     origin_y = 0;
     myCanvas = null;

   }

 public PFStringIndividual(ProblemFactory aPF)
   {
     super();
     points = new Vector();
     contacts = new Vector();
     size = aPF.getSize();
     chromosome = aPF.newSolution();
     fitness    = Double.NEGATIVE_INFINITY;

     scale = 1;
     diam = 10;
     origin_x = 0;
     origin_y = 0;
     myCanvas = null;

   }






 public Individual copyIndividual()
   {
     PFStringIndividual aNewOne;


     aNewOne = new PFStringIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());
     aNewOne.setChromosome(this.getChromosome());
     aNewOne.setNumberGenerationsWithoutChange(this.getNumberGenerationsWithoutChange());
     aNewOne.setTemperature(this.getTemperature());
     aNewOne.setPoints(this.getPoints());
     aNewOne.setContacts(this.getContacts());
     return (Individual)aNewOne;

   }


 public String chromosomeToString()
 {

      return (String)chromosome;

 }


 public void display()
   {
    System.out.println("|S="+size+",F="+fitness+"|<"+chromosome+">");
   }


public void removePoints()
{
 points.removeAllElements();
}


  public void removeContacts()
  {
    contacts.removeAllElements();
  }

public void setPoint(int x, int y, char hp,int diam)
{
		points.addElement(new XYPoint(x, y, hp,diam));
		//System.out.println(x+" "+y);
}


public void setContact(int residueI, int residueJ, char residueType)
  {
    contacts.addElement(new Contact(residueI,residueJ,residueType));
  }

public Vector getPoints()
{
 return points;
}

public Vector getContacts()
  {
	     return contacts;
  }

public void setPoints(Vector aVector)
{
 points = (Vector)(aVector.clone());
}


public void setContacts(Vector aVector)
{
    contacts = (Vector)(aVector.clone());
}

 public void display(TextArea myTextArea)
   {
     myTextArea.append("|S="+size+",F="+fitness+"|<"+chromosome+">\n");
   }


 public void display(Canvas aCanvas, String instance, boolean pintarBorrar)
   {
     int i;
     Graphics aGraphic;
     double x1;
     double y1;
     double x2;
     double y2;
     double first_x1;
     double first_y1;

     char monomer;

     myCanvas = aCanvas;
     aGraphic = myCanvas.getGraphics();

     scale = 30;
     // scale = (myCanvas.getHeight()-100)/size;
     origin_x = (int)(0.5*myCanvas.getWidth());
     origin_y = (int)(0.5*myCanvas.getHeight());

     // System.out.println("Display with scale="+scale+" origin_x="+origin_x+" origin_y="+origin_y+" getSize="+myCanvas.getSize());

     if(pintarBorrar)
     {
      aGraphic.setColor(Color.white);
     }
     else
     {
       aGraphic.setColor(Color.black);
     }

     aGraphic.drawString("Protein Structure Prediction Problem", 10, 35);
     aGraphic.drawString("HP instance  : " + instance, 10, 60);
     aGraphic.drawString("Length       : " + size, 10, 85);
     aGraphic.drawString("Energy       : " + fitness, 10, 110);


     if (points.size() > 0)
     {
       // Paints first residue
       x1 = (int)(((XYPoint)points.elementAt(0)).getX());
       y1 = (int)(((XYPoint)points.elementAt(0)).getY());
       first_x1 = (int)(((XYPoint)points.elementAt(0)).getX());
       first_y1 = (int)(((XYPoint)points.elementAt(0)).getY());
      // System.out.println("diam="+diam);
       monomer = ((XYPoint)points.elementAt(0)).getMonomer();
       if(pintarBorrar)
       {
        if (monomer == 'H')
	       aGraphic.setColor(Color.red);
        else
         aGraphic.setColor(Color.green);
       }
       else
       {
         aGraphic.setColor(Color.black);
       }
       aGraphic.fillOval((int)((x1)*scale  + origin_x - first_x1*scale  ), (int)((y1)*scale + origin_y - first_y1*scale ), diam, diam);

       // paints the remaining residues
       for (i = 1; i < points.size(); i++)
       {
        	monomer = ((XYPoint)points.elementAt(i)).getMonomer();
        	if(pintarBorrar)
        	{
        	 if (monomer == 'H')
        	  aGraphic.setColor(Color.red);
        	 else
        	  aGraphic.setColor(Color.green);
        	}
        	else
        	{
        	 aGraphic.setColor(Color.black);
        	}

        	// x2 =  (int) x1 + ( ( (((XYPoint)points.elementAt(i)).getX()) - x1 ) * scale );
        	// y2 =  (int) y1 + ( ( (((XYPoint)points.elementAt(i)).getY()) - y1 ) * scale );
        	x2 =  (int) ((XYPoint)points.elementAt(i)).getX();
        	y2 =  (int) ((XYPoint)points.elementAt(i)).getY();
          aGraphic.fillOval( (int) ((x2)*scale  + origin_x - first_x1*scale ), (int) ((y2)*scale + origin_y - first_y1*scale   ) , diam, diam);

          if(pintarBorrar)
        	{
        	 aGraphic.setColor(Color.blue);
        	}
        	else
        	{
        	 aGraphic.setColor(Color.black);
        	}
        	aGraphic.drawLine( (int)((x1)*scale  + origin_x - first_x1*scale + diam*0.5 ), (int)((y1)*scale + origin_y - first_y1*scale + diam*0.5), (int) ((x2)*scale  + origin_x - first_x1*scale + diam*0.5), (int) ((y2)*scale + origin_y - first_y1*scale + diam*0.5  )  );
          // System.out.println("origin_x="+origin_x+" scale="+scale+" XYx="+ ((XYPoint)points.elementAt(i)).getX()  +" x2="+x2);
          // System.out.println("origin_y="+origin_y+" scale="+scale+" XYy="+ ((XYPoint)points.elementAt(i)).getY()  +" y2="+y2);
          // System.out.println("");

          x1 = x2;
          y1 = y2;

      }
   }

 }


public void drawFirstMonomer(int xPos, int yPos, char hp){
    removePoints();
		setPoint(xPos, yPos, hp, diam);
	}

public void drawMonomer(int xPos, int yPos, char hp){
		setPoint(xPos, yPos, hp, diam);
	}




}
