

package MAFRA.Population_Individual;

import java.util.Hashtable;
import java.util.Vector;

import DataStructures.*;
import Exceptions.*;
import MAFRA.Visitor.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;


/** The PFStringsPopulation class is a subclass of population that adds a method to add
individuals if the structures they represent are compatible with those in the population

    @version Memetic Algorithms Framework - V 1.1 - October 2001
    @author  Natalio Krasnogor
    */
public class PFStringsPopulation extends Population{


    private static int ADD=1;
    private static int DELETE=0;
    protected Hashtable features;
    protected static double crowdingFeatureFactor=10.0;


 public PFStringsPopulation()
   {
       super();
       features = new Hashtable();
       //       crowdingFeatureFactor = 0.8;

   };



 public PFStringsPopulation(long aSize,IndividualFactory iF)
    {
	super(aSize,iF);
	features= new Hashtable();
	//	crowdingFeatureFactor = 0.8;
	updateAllPopulationFeatures();
    }

public static void setCrowdingFeatureFactor(double aF)
{
 crowdingFeatureFactor=aF;
}

public double getCrowdingFeatureFactor()
{
    return crowdingFeatureFactor;
}




/** This method overrides the equivalent one in the supperclass to ensure that the features of 
   the new individuals that are added are accounted for */
 public void addIndividual(Individual anIndividual)
   {
     try
       {
	 members.insert((Object)anIndividual);
	 updateFeatures((PFStringIndividual)anIndividual,ADD);
       }
     catch(ItemNotFound e)
       {
	  System.out.println("ERROR - exception "+e+" in addIndividual() in Population.java");
       }
	 membersNumber++;
	 updateMaxMinSumFitness(anIndividual);
   
   }



 /** This method add the individual to the population if it is compatible. If it is added
     returns true, else false. */
 public boolean addIndividualIfCompatible(Individual anIndividual)
   {
     boolean result=false;
     // System.out.println("IN PFStringPopulation:addIndividualIfCompatible");
     if(isCompatible((PFStringIndividual)anIndividual))
       {
 	 members.zeroth();
	 addIndividual((PFStringIndividual)anIndividual);
	 result= true;
       }
     else
       {
	   //  	 	 System.out.println("Otro identico...");
	 result =false;
       }
	 
     return result;
    }


public void updateFeatures(PFStringIndividual anIndividual, int ADD_OR_DELETE)
{
    Vector  contacts;
    String key;
    double actualVal;
    int i;

    contacts= anIndividual.getContacts();
    for(i=0;i<contacts.size();i++)
	  {
	      key =" "+((Contact)contacts.elementAt(i)).getLeftResidue()+"-"+((Contact)contacts.elementAt(i)).getRightResidue()+" ";
	    
		  
	      if(ADD_OR_DELETE==1)
		  {/* we are adding */
		      if(features.containsKey(key))
			  {
			      actualVal= ((Double)features.get(key)).doubleValue();
			      features.remove(key);
			      actualVal+=1.0;
			      features.put(key,new Double(actualVal));
			  }
		      else
			  {
			      actualVal = 1.0;
			      features.put(key,new Double(actualVal));
			  }
		  }
	      else/* it is a delete */
		  {
		      if(features.containsKey(key))
			  {
			      /* we are deleting */
			      actualVal= ((Double)features.get(key)).doubleValue();
			      features.remove(key);
			      actualVal-=1.0;
			      if(actualVal>0)
				  { /* we update the previous value with 1 less */
				      features.put(key,new Double(actualVal));
				  }
			  }
		  }
		      
	  }
}


public boolean isCompatible(PFStringIndividual anIndividual)
  {
      
      Vector  contacts;
      boolean compatible=false;
      String key;
      int i;
      int compatibleContacts = 0;


      contacts= anIndividual.getContacts();
      //  System.out.println("Contacts size:"+contacts.size());
      for(i=0;i<contacts.size();i++)
	  {
	      key =" "+((Contact)contacts.elementAt(i)).getLeftResidue()+"-"+((Contact)contacts.elementAt(i)).getRightResidue()+" ";
	      if(features.containsKey(key))
		  {
		      //		      if( ((Double)features.get(key)).doubleValue() <membersNumber*crowdingFeatureFactor)
		      //		      System.out.println("Value:"+((Double)features.get(key)).doubleValue() +", CFF:"+crowdingFeatureFactor);
		      if( ((Double)features.get(key)).doubleValue() <crowdingFeatureFactor)
			  { /* the feature contact is not overcrowded */
			      compatibleContacts++;
			      //			      System.out.println("Compatible:"+((Double)features.get(key)).doubleValue() +"<"+crowdingFeatureFactor);
			  }
		      else
			  {
			      //			      System.out.println("No compatible");
			  }
		  }
	      else
		  { /* the contact is not in the population */
		      compatibleContacts++;
		      //		      System.out.println("Compatible: original contact");
		  }
	  }

      //      System.out.println(compatibleContacts+" >"+contacts.size()*0.20+" | crowdingFactor="+crowdingFeatureFactor);

      if((compatibleContacts>(contacts.size()*0.20)) )
	  {
	      compatible = true;
	  }
  
    return compatible;
  }


 public void deleteIndividual(Individual anIndividual)
   {

     Individual tmp;
     double     fitnessTmp;
     boolean    OK;

     OK= true;
     try
       {
	 members.remove(anIndividual);
       }
     catch(ItemNotFound e)
       {
	  System.out.println("ERROR - exception "+e+" in deleteIndividual() in Population.java");
	  OK = false;
       }

     if(OK)
       {
	 membersNumber--;
	 sumFitness-=anIndividual.getFitness();

	 for(members.first();members.isInList();members.advance())
	   {
	     tmp = (Individual) members.retrieve();
	     fitnessTmp = tmp.getFitness();
	     if(maxFitness<fitnessTmp) 
	       {
		 maxFitness = fitnessTmp;
	       }
	     if(minFitness>fitnessTmp)
	       {
		 minFitness = fitnessTmp;
	       }
	   }
	 updateFeatures((PFStringIndividual)anIndividual,DELETE);
       }
     }


   

 public void extinct()
   {
     /*for(members.first();members.isInList();members.advance())
      {
	try
	  {
	    members.remove(members.retrieve());
	  }
	catch(ItemNotFound e)
	  {
	    System.out.println("ERROR: "+e+" in extinct() in Population.java");
	  }
      }*/
    auxList       = new LinkedList( );
    members       = new LinkedListItr(auxList);
    membersNumber = 0;
    minFitness    = Double.POSITIVE_INFINITY;
    maxFitness    = Double.NEGATIVE_INFINITY;
    sumFitness    = 0.0;
    features      = new Hashtable();
    //    crowdingFeatureFactor = 0.8;

   }


 /** Copies up to upTo members of the receiving Population into dst. Overrides the one in the superclass.
     @param dst is the destiny Population where the members of the receiving Population will be copied.
     @param upTo specify how many members are to be copied starting from the first(). */
 public void copyTo(PFStringsPopulation dst, long upTo)
 {
   long i;
   PFStringIndividual tmpIndi;
   PFStringIndividual fromPop;

   i = 0;
   //   System.out.println("PFStringsPopulation:copyTo");
   
   /* copies at most this.membersNumber individual */
       for(this.members.first();(this.members.isInList()&& (i<upTo));this.members.advance() )
	   {
	     fromPop = (PFStringIndividual)(members.retrieve());
	     tmpIndi= (PFStringIndividual)(fromPop.copyIndividual());
	     dst.addIndividual(tmpIndi);
	     i++;
	   }

      

 } 



public void updateAllPopulationFeatures()
{

 PFStringIndividual tmp;
 //	 System.out.println("PFStringsPopulation:updateAllPopulationFeatures"); 
 for(members.first();members.isInList();members.advance())
     {

	 tmp = (PFStringIndividual)( members.retrieve());
	 this.updateFeatures(tmp,ADD);
	 
     }


}


}




