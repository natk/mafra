package MAFRA.Population_Individual;

import java.util.ArrayList;
import java.awt.TextArea;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;


public class ColorsSetIndividual extends Individual{

 public ColorsSetIndividual()
   {
   }

 public ColorsSetIndividual(ProblemFactory aPF)
   {
     super(aPF);
   }


 public void setChromosome(Object aChr)
   {
     chromosome = ((ArrayList)aChr).clone();
   }


 public Individual copyIndividual()
   {
     ColorsSetIndividual aNewOne;

     ArrayList           aNewC;

     aNewOne = new ColorsSetIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());

     aNewC = new ArrayList();
     //1//aNewC.add((ArrayList)this.getChromosome()) ;
     aNewC.add(this.getChromosome()) ;

     aNewOne.setChromosome(this.getChromosome());
     return (Individual)aNewOne;

   }



 public void display()
   {
    StringBuffer tmp;
    int i;
    ArrayList aBS;
   
    aBS=(ArrayList)(this.getChromosome());
    tmp = new StringBuffer();
    for (i=0;i<size;i++)
      {
	  //JN
	  //tmp = tmp.append(aBS.get(i));
	  /*
	if(aBS.get(i))
	  {
	    tmp = tmp.append(1);
	  }
	else
	  {
	    tmp = tmp.append(0);
	  }
	  */
      }

    //System.out.println("|S="+size+",F="+fitness+"|<"+tmp.toString()+">");
    //JN
    System.out.println("|S="+size+",F="+fitness+"|<"+aBS+">");
   }



 public void display(TextArea myTextArea)
   {
    StringBuffer tmp;
    int i;
    ArrayList aBS;
   
    aBS=(ArrayList)(this.getChromosome());
    
    tmp = new StringBuffer();
    for (i=0;i<size;i++)
      {/*
	if(aBS.get(i))
	  {
	    tmp = tmp.append(i);
	  }
	else
	  {
	    tmp = tmp.append(i);
	  }
       */
      }


     myTextArea.append("|S="+size+",F="+fitness+"|<"+tmp.toString()+">\n");
   }



}
