
package MAFRA.AbstractPlan;


import java.util.Hashtable;
import MAFRA.Util.*;
import MAFRA.Strategy.*;
import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;





/** GA Class is used by the EvolutionaryPlan class to solve a problem.
    A GA instance has a particular Mating, Finalization, Initialization, Restart, Mutation, LocalSearch,
    and Selection strategies. Also it has its own paremeters, visitors and populations to update.
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public class GA implements SetStrategies{

 
 private Hashtable parameters;
 private Hashtable visitors;
 private Hashtable populations;
 // private MatingStrategy         myMatingStrategy;
 private AbstractMatingStrategy         myMatingStrategy;
 private FinalizationStrategy   myFinalizationStrategy;
 private InitializePopStrategy  myInitializePopStrategy;
 private RestartPopStrategy     myRestartPopStrategy;
 private AbstractMutationStrategy       myMutationStrategy;
 private AbstractLocalSearchStrategy    myLocalSearchStrategy;
 private SelectionStrategy      mySelectionStrategy;


 public GA()
   {
     parameters = new Hashtable();
     visitors   = new Hashtable();
     populations= new Hashtable();
   }


 /** This constructor is usefull to "clone" a GA. 
     It uses other GA`s strategies, parameters, visitors and 
     populations. This is usefull to run different experiments with similar settings.
     */
 /* public GA(GA anotherGA)
   {
    this.parameters            = anotherGA.parameters.clone();
    this.visitors              = anotherGA.visitors.clone();
    this.populations           = anotherGA.populations.clone();
    this.myMatingStrategy        = anotherGA.getMatingStrategy().clone();
    this.myFinalizationStrategy  = anotherGA.getFinalizationStrategy().clone();
    this.myInitializePopStrategy = anotherGA.getInitializePopStrategy().clone();
    this.myRestartPopStrategy    = anotherGA.getRestartPopStrategy().clone();
    this.myMutationStrategy      = anotherGA.getMutationStrategy().clone();
    this.myLocalSearchStrategy   = anotherGA.getLocalSearchStrategy().clone();
    this.mySelectionStrategy     = anotherGA.getSelectionStrategy().clone();
   }*/

 /* Set and access methods for the private variables */

 /** Adds the parameter aName to the GA's parameter list with a value of aParameter */
 public void addParameter(String aName,Object  aParameter)
   {
     parameters.put(aName,aParameter);
   }

 /** Adds aVisitor as aName to the GA's visitors list. */
 public void addVisitor(String aName, Visitor aVisitor)
   {
    visitors.put(aName,aVisitor);
   }

 /** Adds aPopulation known as aName to the GA's Population list. */
 public void addPopulation(String aName, Population aPopulation)
   {
    populations.put(aName,aPopulation);
   }


 /** Returns the value of the parameter aName */
 public Object getParameter(String aName)
   {
    return (parameters.get(aName));
   }


 /** Returns the visitor whose name is aName */
 public Visitor getVisitor(String aName)
   {
    return ((Visitor)(visitors.get(aName)));
   }


 /** Returns the population named  aName */
 public Population getPopulation(String aName)
   {
    return ((Population)(populations.get(aName)));
   }


 /* SetStrategies Interface implementation */
 
public void addSelectionStrategy(SelectionStrategy aS)
   {
    mySelectionStrategy = aS;
   } 

 public void addLocalSearchStrategy(AbstractLocalSearchStrategy aS)
   {
    myLocalSearchStrategy = aS;
   } 

 public void addMutationStrategy(AbstractMutationStrategy aS)
   {
    myMutationStrategy = aS;
   } 

 public void addRestartPopStrategy(RestartPopStrategy aS)
   {
    myRestartPopStrategy = aS;
   } 

 public void addInitializeStrategy(InitializePopStrategy aS)
   {
    myInitializePopStrategy = aS;
   } 

 public void addFinalizationStrategy(FinalizationStrategy aS)
   {
    myFinalizationStrategy = aS;
   } 

 public void addMatingStrategy(AbstractMatingStrategy aS)
   {
    myMatingStrategy = aS;
   } 

 public SelectionStrategy getSelectionStrategy()
   {
     return(mySelectionStrategy);
   }

 public AbstractLocalSearchStrategy getLocalSearchStrategy()
   {
     return(myLocalSearchStrategy);
   }

 public AbstractMutationStrategy getMutationStrategy()
   {
     return(myMutationStrategy);
   }

 public RestartPopStrategy getRestartPopStrategy()
   {
     return(myRestartPopStrategy);
   }

 public InitializePopStrategy getInitializePopStrategy()
   {
     return(myInitializePopStrategy);
   }
 public FinalizationStrategy getFinalizationStrategy()
   {
     return(myFinalizationStrategy);
   }

 public AbstractMatingStrategy getMatingStrategy()
   {
     return(myMatingStrategy);
   }

}
