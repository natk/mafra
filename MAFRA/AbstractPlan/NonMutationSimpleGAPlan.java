

package MAFRA.AbstractPlan;

/** This class implements a simple GA with ONLY  reproduction and selection stages (hence the name "NonMutation"). 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */


public class NonMutationSimpleGAPlan extends SimpleGAPlan{

  public NonMutationSimpleGAPlan(GA aGA)
    {
      super(aGA);
    }

  /** In this class we overwrite the DoMutation from SimpleGAPlan in such a way to do nothing.*/
  public void DoMutation()
    {}


}
		      
