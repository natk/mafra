
package MAFRA.AbstractPlan;

import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;


/** This class implements a simple memetic plan with a reproduction, mutation, local search and selection stages.
Because it inherits from SimpleGAPlan it only needs to redefine DoLocalSearch() method
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class SimpleMemeticPlan extends SimpleGAPlan{


  public SimpleMemeticPlan(GA aGa)
    {
      super(aGa);
    }



  public void DoLocalSearch()
    {
      Population myPop;

      (myGA.getLocalSearchStrategy()).execute();
      myPop = myGA.getPopulation("parentsPopulation"); // M2711-4 
      myPop.acceptVisitor((FitnessVisitor)(myGA.getVisitor("fitnessVisitor"))); //M2711-4
      
    };








}
		      
