

package MAFRA.AbstractPlan;

import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** AbstractPlan is just a membership class that is the superclass of all the alternative
plans

 @version Memetic Algorithms Framework - V 1.0 - Setember 1999
 @author  Natalio Krasnogor
 */

public abstract class AbstractPlan{
}
