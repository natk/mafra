
package MAFRA.AbstractPlan;

import MAFRA.Strategy.*;
import MAFRA.Visitor.*;
import MAFRA.Population_Individual.*;



/** This class implements a simple GA with a reproduction, mutation and selection stage
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class SimpleGAPlan extends Plan{


  public SimpleGAPlan(GA aGa)
    {
      super(aGa);
    }

  public void DoMutate()
    {
     (myGA.getMutationStrategy()).execute();
    };



  public void DoReproduce()
    {
      (myGA.getMatingStrategy()).execute();
    };




  public void DoUpdatePopulation()
    {
      Population myP;
      Population myOf;
      FitnessVisitor aFS;

    	// System.out.println("Updating Stage");
      aFS = (FitnessVisitor)(myGA.getVisitor("fitnessVisitor"));
    	myOf = myGA.getPopulation("offspringsPopulation");
    	myOf.acceptVisitor(aFS);
      // System.out.println("Best Offspring:"+myOf.getMaxFitness()+" Worst Offspring:"+myOf.getMinFitness());
    	myP  = myGA.getPopulation("parentsPopulation");
    	myP.acceptVisitor(aFS);
    	// System.out.println("Best Parent"+myP.getMaxFitness()+" Worst Parent:"+myP.getMinFitness());
     	(myGA.getSelectionStrategy()).execute();
    	myOf.extinct();
    	//	myP.copyTo(myOf); this is not necesary (M2711-1)

    }

    public boolean DoCheckTermination()
      {
      	long maxGenerations;

      	maxGenerations = ((Long)(myGA.getParameter("maxGenerationsNumber"))).longValue();
      	return(generationsNow>=maxGenerations);
      }






}
