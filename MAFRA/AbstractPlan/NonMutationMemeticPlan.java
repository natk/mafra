
package MAFRA.AbstractPlan;


/** This class implements a simple GA with a reproduction, local search and selection stage 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class NonMutationMemeticPlan extends SimpleMemeticPlan{


  public NonMutationMemeticPlan(GA aGa)
    {
      super(aGa);
    }


  /** In this class we overwrite the DoMutation from SimpleMemeticPlan in such a way to do nothing.*/
  public void DoMutation()
    {}








}
		      
