
package MAFRA.AbstractPlan;

import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;


/** This class implements a self adapting static simple memetic plan with a reproduction, mutation, local search and selection stages.
Because it inherits from SelfAdaptingStaticSimpleGAPlan it only needs to redefine DoLocalSearch() method
 @version Memetic Algorithms Framework - V 1.0 - August 2000
 @author  Natalio Krasnogor
 */

public class SelfAdaptingStaticSimpleMemeticPlan extends SelfAdaptingStaticSimpleGAPlan{


  public SelfAdaptingStaticSimpleMemeticPlan(GA aGa)
    {
      super(aGa);
    }



  public void DoLocalSearch()
    {
      Population myPop;

      (myGA.getLocalSearchStrategy()).execute();
    //  myPop = myGA.getPopulation("parentsPopulation"); // M2711-4 
    //  myPop.acceptVisitor((FitnessVisitor)(myGA.getVisitor("fitnessVisitor"))); //M2711-4
      
    };








}
		      
