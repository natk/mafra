
package MAFRA.AbstractPlan;

import MAFRA.Strategy.*;
import MAFRA.Visitor.*;
import MAFRA.Population_Individual.*;



/** This class implements a self adapting static simple GA with a reproduction, mutation and selection stage.
The self adaptation is done by means of memes that encodes the mutation, crossover and probabilities to
be used in the respective stages. 
 @version Memetic Algorithms Framework - V 1.2 - February 2000
 @author  Natalio Krasnogor
 */

public class SelfAdaptingStaticSimpleGAPlan extends SimpleGAPlan{


  public SelfAdaptingStaticSimpleGAPlan(GA aGa)
    {
      super(aGa);
    }



  public void DoUpdatePopulation()
    {
   
      Population myP;
      Population myOf;
      FitnessVisitor aFS;
      MemeExpressionVisitor aMEV;

        // System.out.println("Updating Stage");
        aFS  = (FitnessVisitor)(myGA.getVisitor("fitnessVisitor"));
	aMEV = (MemeExpressionVisitor)(myGA.getVisitor("memeExpressionVisitor"));
	myOf = myGA.getPopulation("offspringsPopulation");
	myOf.acceptVisitor(aFS);
	//System.out.println();
	//System.out.println("Best Offspring:"+myOf.getMaxFitness()+" Worst Offspring:"+myOf.getMinFitness());
	myP  = myGA.getPopulation("parentsPopulation");
	myP.acceptVisitor(aFS);
	//System.out.println("Best Parent:"+myP.getMaxFitness()+" Worst Parent:"+myP.getMinFitness());
 	(myGA.getSelectionStrategy()).execute();
	myP.acceptVisitor(aMEV);
	myOf.extinct();
	
       


    }






}
		      
