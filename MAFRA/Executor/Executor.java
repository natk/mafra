
package MAFRA.Executor;

/** Interface for all the other strategies executors and operators that collaborate with Strategies to
implement the Strategy pattern inherit.

  @version Memetic Algorithms Framework - V 1.0 - August 1999
  @author  Natalio Krasnogor
   */

public interface Executor{

  public abstract  void execute();

}
