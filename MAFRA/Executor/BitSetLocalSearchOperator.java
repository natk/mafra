

package MAFRA.Executor;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Factory.*;




/** This class is the one that _Actually_ implements the local search operator.
It implements the Operator interface which in turns extends executor interface.
This class is USER DEFINED.

@version Memetic Algorithms Framework - V 1.0 - August 1999
@author  Natalio Krasnogor
*/

public class BitSetLocalSearchOperator implements Operator{

  private BitSetBoltzmannLocalSearcher myLocalSearch;
  private Individual                 myIndividual;
  private double                     temperature;
  private double                     myDistances[][];
  private FitnessVisitor             myFitnessVisitor;
  private double                     popSize=0.0;

  /** Creates a LocalSearch Operator using a specific LocalSearch Factory */
 public BitSetLocalSearchOperator(LocalSearchFactory aFactory)
   {
    myLocalSearch   =(BitSetBoltzmannLocalSearcher)( aFactory.createBoltzmannLocalSearcher());
    myIndividual = null;
   }


 /** The individual passed to the LocalSearchOperator via the hashtable
     is passed by reference.*/
 public void setArguments(Hashtable args)
   {
    myIndividual = ((Individual)args.get("Individual"));
    temperature  = ((Double)args.get("Temperature")).doubleValue();
    myDistances = ((double[][])args.get("Distances"));
    myFitnessVisitor =((FitnessVisitor)args.get("FitnessVisitor"));
    popSize =((Double)args.get("PopSize")).doubleValue();
   }

 
 /** This method returns null because the arguments are passed by reference 
     and nothing new is created. */
 public Hashtable getResults()
   {
     return null;
   }



 public void execute()
   {
     if (myIndividual!= null)
       {
       	 // myIndividual = myLocalSearch.individualLocalSearch(myIndividual,temperature,popSize,myFitnessVisitor);
       	 myIndividual = myLocalSearch.individualLocalSearch(myIndividual,temperature,myFitnessVisitor);
       }
   }
    

}
