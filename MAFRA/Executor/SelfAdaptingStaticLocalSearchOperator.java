



package MAFRA.Executor;

import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;


/** This class is the one that _Actually_ implements the LocalSearch operator for a
self adapting static LocalSearch strategy. Before calling execute both and individual and
a LocalSearch operator should be passed to it.
It implements the Operator interface which in turns extends executor interface.
This class is USER DEFINED.

@version Memetic Algorithms Framework - V 1.0 - August 1999
@author  Natalio Krasnogor
*/
public class SelfAdaptingStaticLocalSearchOperator implements Operator{

  private AbstractLocalSearcher myLocalSearch;
  private Individual            myIndividual;
  private FitnessVisitor        myFitnessVisitor;
  private Double			    myTemperature;
  private Double                myK;
  private Double                myErrorThreshold;

  /** Creates a LocalSearch Operator using a specifi LocalSearch Factory */
 public SelfAdaptingStaticLocalSearchOperator()
   {
    myLocalSearch   = null;
    myIndividual = null;
   }


 /** The individual passed to the LocalSearchOperator via the hashtable
     is passed by reference.*/
 public void setArguments(Hashtable args)
   {
    myIndividual     = (Individual)(args.get("Individual"));
    myLocalSearch    = ((AbstractLocalSearcher)args.get("LocalSearchOperator"));
    myFitnessVisitor = ((FitnessVisitor)args.get("FitnessVisitor"));
    myTemperature    = ((Double)args.get("Temperature"));
    myK              = ((Double)args.get("K"));
    myErrorThreshold = ((Double)args.get("ErrorThreshold"));
    
   }

 
 /** This method returns null because the arguments are passed by reference 
     and nothing new is created. */
 public Hashtable getResults()
   {
     return null;
   }



 public void execute()
   {
     if ((myIndividual!= null)&&(myFitnessVisitor!=null))
       {
        ((TourGeneralHillClimberLocalSearcher)myLocalSearch).setTemperature((myTemperature.doubleValue()));
        ((TourGeneralHillClimberLocalSearcher)myLocalSearch).setK((myK.doubleValue()));
        ((TourGeneralHillClimberLocalSearcher)myLocalSearch).setErrorThreshold((myErrorThreshold.doubleValue()));
        
	     myIndividual =(Individual)( ((TourGeneralHillClimberLocalSearcher)myLocalSearch).individualLocalSearch((Individual)myIndividual));
       }
     else
       {
        System.out.println("ERROR - Either individual or fitnessVisitor is null in SelfAdaptingStaticLocalSearcherOperator's execute() method");
     	System.exit(0);
       }
   }
    

}
