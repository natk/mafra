



package MAFRA.Strategy;

import java.util.Hashtable;

import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;
import MAFRA.Executor.Operator;
import MAFRA.Executor.SelectionMethod;
import MAFRA.Executor.PMXCrossOverOperator;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class is the superclass (abstract) of all the mating strategies.
 @version Memetic Algorithms Framework - V 1.2 - November 1999
 @author  Natalio Krasnogor
 */
public abstract class AbstractMatingStrategy extends Strategy implements Operator{

  protected double     proX;

  public AbstractMatingStrategy()
    {
      super();
    }



  /** This method returns the probability that having choosen two parents they will 
     participate(crossover) or not */
  public double getProbabilityCrossOver()
    {
      return proX;
    }



}
