package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;


/** This class implements the mutation procedures of the populations.
It receives to arguments which are the population to be mutated and 
the probability to mutated a given individual in the population.
The population is passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will mutated the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of mutating an individual.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class PFMutationStrategy extends AbstractMutationStrategy implements Operator{

  private Population myPopulation;
  private double     proMutPerIndLink;
  private int        numberOfHelpers;

  public PFMutationStrategy(MutationFactory aMutationFactory, PFProblem aP)
    {
     super();
     setExecutor(new PFMutationOperator(aMutationFactory,aP));
    }

  /** This are the methods from the Operator Interface. The Hashtable receives to associations,
      one with key="Population" and value the population to be mutated and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to mutate a given individual of the population.*/
  public void setArguments(Hashtable args)
    {
      myPopulation     = (Population)(args.get("Population"));
      proMutPerInd     = ((Double)args.get("ProbabilityPerIndividual")).doubleValue();
      proMutPerIndLink = ((Double)args.get("ProbabilityPerIndividualLink")).doubleValue();
      numberOfHelpers  = ((Integer)args.get("NumberOfHelpers")).intValue();
    }
 


  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     Individual tmp;
     double     rnd;
     Hashtable  args;
     Population tmpPop;

     //     tmpPop = new Population();
     args   = new Hashtable();
  //   System.out.println("MUTATION STAGE");
     for(myPopulation.first();myPopulation.isInList();myPopulation.advance())
       {
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	 tmp = (Individual) myPopulation.retrieve();
	
	 /* Now we mutate the link to the helper*/
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
         if(rnd<proMutPerIndLink)
	   {
	 
            ((SelfAdaptingStaticPFStringIndividual)tmp).setHelperId((int)(MAFRA_Random.nextUniformLong(0,numberOfHelpers)));
	   }
	
	
         if (rnd<proMutPerInd)
	//  System.out.println("Indi:"+tmp.getFitness());
	   { /* we decided to mutate the individual */
	     args.put("Individual",tmp);
	     ((PFMutationOperator)executor).setArguments(args);
             executor.execute();
	      
	   }
	   
       }

    }


}




