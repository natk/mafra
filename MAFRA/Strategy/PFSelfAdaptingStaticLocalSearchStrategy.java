package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Visitor.*;


/** This class implements the local search procedures of the populations.
This is a static self adapting strategy which means that the local search operators
and parameters are fixed (static) but the individuals in the population self adapt
to learn to use the ones that profit them the most.
It receives at creation time a table containing the abstract local searchers with the probabilites.
Later, succesive calls to execute() methods will local search the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of local searching an individual.

 @version Memetic Algorithms Framework - V 1.0 - January 2000
 @author  Natalio Krasnogor
 */
public class PFSelfAdaptingStaticLocalSearchStrategy extends AbstractLocalSearchStrategy implements Operator{

  private Population myPopulation;
  private AbstractLocalSearcherProbConExpTable myHelpers;
  private double temperature;
  private double previousTemperature;
  private double K;
  private double errorThreshold;
  private boolean elitism;
 
 
  


  public PFSelfAdaptingStaticLocalSearchStrategy(AbstractLocalSearcherProbConExpTable helpers)
    {
      super(new PFSelfAdaptingStaticLocalSearchOperator());
      myHelpers  = helpers;
      previousTemperature = 0.0;
      temperature         = 0.0;
      elitism             = true;
    }

  /** This are the methods from the Operator Interface. The Hashtable receives to associations,
      one with key="Population" and value the population to be local searched and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to local search a given individual of the population.*/
  public void setArguments(Hashtable args)
    {
    
     double sL; /* length of the shortest link in the instance */
     double lL; /* length of the longest  link in the instance  */
     double n;  /* number of cities */
     double s;  /* population size  */
     double nC; /* number of links that are exchanged in one iteration of the local
                   searcher */
    
     
      myPopulation = (Population)(args.get("Population"));
      elitism      = ((Boolean)(args.get("Elitism"))).booleanValue();
      myProblemFactory = ((ProblemFactory)args.get("ProblemFactory"));
      if(myProblemFactory==null)
      {
       System.out.println("ERROR - problemFactory is null in SelfAdaptingStaticLocalSearchStrategy's setArguments");
       System.exit(0);
      }
      myFitnessVisitor = new FitnessVisitor(myProblemFactory);
      
    /*  sL = ((TSPProblem)myProblemFactory).getShortestLink();
      lL = ((TSPProblem)myProblemFactory).getLongestLink();
      n  = ((TSPProblem)myProblemFactory).getSize();
      s  = myPopulation.getMembersNumber();
      nC= 2;
      
       We set K in such a way that it will have a value in acord with the theory 
      errorThreshold = (Math.log(lL/sL)/n) - ( 2*Math.sqrt((lL/sL)-1)/(n*Math.sqrt(s)) ) +  ( 2*Math.log(lL/sL)*Math.sqrt((lL/sL)-1) / (n*n*Math.sqrt(s)) );
      K = -Math.log(errorThreshold)/(n*nC*(sL-lL)*(sL-lL));*/
      K=5;
     
    }
  


  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     SelfAdaptingStaticPFStringIndividual tmp;
     double     rnd;
     Hashtable  args;
     Population tmpPop;
     double     proLocalSearchInd;
     double      delta;
     int        helperId;
   

     //     tmpPop = new Population();
     args = new Hashtable();
     
     /* we compute the temperature for the local searchers */
    // delta      = Math.abs(myPopulation.getMaxFitness() - myPopulation.getAvgFitness());
     delta      = Math.abs(myPopulation.getMaxFitness() - myPopulation.getMinFitness());
    
     System.out.println();
     System.out.println("+F:"+myPopulation.getMaxFitness()+", -F:"+myPopulation.getMinFitness()+" <<>> delta="+delta);
     if (delta >0)
     {
	  temperature = (1.0/delta);
	  previousTemperature = temperature;
	//  System.out.println(".temperature="+temperature);
     }
     else
     {
      temperature         = previousTemperature*2;
      previousTemperature = temperature;
      if(temperature>=Double.MAX_VALUE)
      {
       temperature         = Double.MAX_VALUE;
       previousTemperature = Double.MAX_VALUE;
      }
      System.out.println("*2temperature="+temperature);
     }
     
//     temperature = Double.POSITIVE_INFINITY;
     myPopulation.first();
     tmp = (SelfAdaptingStaticPFStringIndividual)myPopulation.retrieve();
     ((SelfAdaptingStaticPFStringIndividual)tmp).setTemperature(temperature);
     if(elitism)
     {
//      System.out.println(".");
      myPopulation.advance();
     }
    /* System.out.println("##########################################");
     System.out.println("##########################################");
     System.out.println(" previousTemperature="+previousTemperature+" temperature="+temperature); */
     for(;myPopulation.isInList();myPopulation.advance())
     {
	 	rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
		tmp = (SelfAdaptingStaticPFStringIndividual)( myPopulation.retrieve());
	 	helperId =((SelfAdaptingStaticPFStringIndividual)tmp).getHelperId();
	 	proLocalSearchInd= myHelpers.getLocalSearcherHelperProbability(helperId);
		((SelfAdaptingStaticPFStringIndividual)tmp).setTemperature(temperature);

        if (rnd<proLocalSearchInd)
	   	{ /* we decided to local search the individual */
	     args.put("LocalSearchOperator",myHelpers.getLocalSearcherHelper(helperId));
	     args.put("Individual",tmp);
	     args.put("FitnessVisitor",myFitnessVisitor);
	     args.put("Temperature",new Double(temperature));
	     args.put("K",new Double(K));
	     args.put("ErrorThreshold",new Double(errorThreshold));
	     ((PFSelfAdaptingStaticLocalSearchOperator)executor).setArguments(args);
		 executor.execute();
	   	}
     }
   /*  System.out.println("##########################################");
     System.out.println("##########################################");*/
    
}


}

