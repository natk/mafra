package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Visitor.*;


/** This class implements the local search procedures of the populations.
This is a static self adapting strategy which means that the local search operators
and parameters are fixed (static) but the individuals in the population self adapt
to learn to use the ones that profit them the most.
It receives at creation time a table containing the abstract local searchers with the probabilites.
Later, succesive calls to execute() methods will local search the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of local searching an individual.

 @version Memetic Algorithms Framework - V 1.0 - January 2000
 @author  Natalio Krasnogor
 */
public class PFSelfAdaptingStaticLocalSearchStrategyRel extends AbstractLocalSearchStrategy implements Operator{

  private Population myPopulation;
  private Population auxPopulation;
  private AbstractLocalSearcherProbConExpTable myHelpers;
  private double temperature;
  private double previousTemperature;
  private double K;
  private double errorThreshold;
  private boolean elitism;
  private int historyLength;
  private double history[][];
  

  private SortingVisitor mySortingVisitor;
  
  
 
 
  


  public PFSelfAdaptingStaticLocalSearchStrategyRel(AbstractLocalSearcherProbConExpTable helpers)
    {
      super(new PFSelfAdaptingStaticLocalSearchOperatorRel());
      myHelpers  = helpers;
      previousTemperature = 0.0;
      temperature         = 0.0;
      elitism             = true;

      mySortingVisitor    = new SortingVisitor();
    
    }

  /** This are the methods from the Operator Interface. The Hashtable receives to associations,
      one with key="Population" and value the population to be local searched and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to local search a given individual of the population.*/
  public void setArguments(Hashtable args)
    {
    
     int f;
    
     
      myPopulation = (Population)(args.get("Population"));
      auxPopulation = (Population)(args.get("AuxPopulation"));
      elitism      = ((Boolean)(args.get("Elitism"))).booleanValue();
      myProblemFactory = ((ProblemFactory)args.get("ProblemFactory"));
      historyLength       = ((Integer)(args.get("HistoryLength"))).intValue();
      history             = new double[historyLength][2];
      for (f=0;f<historyLength;f++)
      {
       history[f][0]=0.0;
       history[f][1]=0.0;
      }
      if(myProblemFactory==null)
      {
       System.out.println("ERROR - problemFactory is null in SelfAdaptingStaticLocalSearchStrategy's setArguments");
       System.exit(0);
      }
      myFitnessVisitor = new FitnessVisitor(myProblemFactory);
      //       K=0.09375; /* 0.0  --  0.125 */ 
      K=0.050; /* mejor entre 0.01 y 0,1 */
     
     

     
    }
  


  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     SelfAdaptingStaticPFStringIndividual tmp;
     double     rnd;
     Hashtable  args;
     Population tmpPop;
     double     proLocalSearchInd;
     double      delta;
     int        helperId;
     int f;


     args = new Hashtable();
     
     /* we compute the temperature for the local searchers */
     /* we shift the history list 
      for(f=historyLength-1;f>0;f--)
      {
        history[f][0]=history[f-1][0];
        history[f][1]=history[f-1][1];
      }
      history[0][0] = myPopulation.getMaxFitness();
      history[0][1] = myPopulation.getMinFitness();     
      delta = 0.0;
      for(f=0;f<historyLength;f++)
      {
       if(Math.abs(history[f][1])>0.0000000001)
       {
        delta = delta + Math.abs((Math.abs(history[f][0])/Math.abs(history[f][1]))-1.0);
       }
      }
      if (delta >0.0000000001)
      {
       temperature = (historyLength/delta);
       previousTemperature = temperature;
       System.out.println("1-temperature="+temperature);
      }
      else
      {
       temperature         = previousTemperature*2;
       previousTemperature = temperature;
       if(temperature>=Double.MAX_VALUE)
       {
        temperature         = Double.MAX_VALUE;
        previousTemperature = Double.MAX_VALUE;
       }
       System.out.println("2-temperature="+temperature);
      }
     */
     
     
     myPopulation.acceptVisitor(myFitnessVisitor);
     delta = Math.abs(auxPopulation.getMaxFitness()-(1.0*auxPopulation.getAvgFitness()));
     temperature=1.0/delta;
     //     System.out.println("DELTA="+delta+" TEMP="+temperature);
     if(temperature>=Double.MAX_VALUE)
     {
      temperature         = Double.MAX_VALUE;
     }
     //     System.out.println("MF="+auxPopulation.getMaxFitness()+",AF="+auxPopulation.getAvgFitness()+",T="+temperature);
     if(elitism)
     {
      myPopulation.acceptVisitor(mySortingVisitor);
     }
     myPopulation.first();
     tmp = (SelfAdaptingStaticPFStringIndividual)myPopulation.retrieve();
     ((SelfAdaptingStaticPFStringIndividual)tmp).setTemperature(temperature);
     if(elitism)
     {
      myPopulation.advance();
     }
   /*  System.out.println("##########################################");
     System.out.println("##########################################");
     System.out.println(" previousTemperature="+previousTemperature+" temperature="+temperature);*/
     int negFit=0;
     for(;myPopulation.isInList();myPopulation.advance())
     {
	        rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
		tmp = (SelfAdaptingStaticPFStringIndividual)( myPopulation.retrieve());
		if (tmp.getFitness()<0) negFit=negFit+1;
	 	helperId =((SelfAdaptingStaticPFStringIndividual)tmp).getHelperId();
	 	proLocalSearchInd= myHelpers.getLocalSearcherHelperProbability(helperId);
		((SelfAdaptingStaticPFStringIndividual)tmp).setTemperature(temperature);

        if (rnd<proLocalSearchInd)
	   	{ /* we decided to local search the individual */
	     args.put("LocalSearchOperator",myHelpers.getLocalSearcherHelper(helperId));
	     args.put("Individual",tmp);
	     args.put("FitnessVisitor",myFitnessVisitor);
	     args.put("Temperature",new Double(temperature));
	     args.put("K",new Double(K));
	     args.put("ErrorThreshold",new Double(errorThreshold));
	     ((PFSelfAdaptingStaticLocalSearchOperatorRel)executor).setArguments(args);
		 executor.execute();
	   	}
     }
     /*  System.out.println("NEGFIT="+negFit);
     System.out.println("##########################################");
     System.out.println("##########################################");*/
    
}


}

