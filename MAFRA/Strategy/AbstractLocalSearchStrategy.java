package MAFRA.Strategy;


import MAFRA.Population_Individual.*;
import MAFRA.Executor.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class is the superclass of all the local search strategies.

 @version Memetic Algorithms Framework - V 1.2 - November 1999
 @author  Natalio Krasnogor
 */
public class AbstractLocalSearchStrategy extends Strategy{


  protected Population     myPopulation;
  protected double         proLocSearPerInd;
  protected ProblemFactory myProblemFactory;
  protected FitnessVisitor myFitnessVisitor;


public AbstractLocalSearchStrategy(Executor anExecutor)
  {
    super(anExecutor);
  }


  /** This method returns the probability of applying local search to a given individual.*/
  public double getProbabilityLocalSearch()
    {
      return proLocSearPerInd;
    }

}
