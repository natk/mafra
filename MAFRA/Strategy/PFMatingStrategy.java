

package MAFRA.Strategy;

import java.util.Hashtable;

import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;
import MAFRA.Executor.Operator;
import MAFRA.Executor.SelectionMethod;
import MAFRA.Executor.PFCrossOverOperator;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class implements the mating procedures of the populations.
It receives three arguments which are the population to be used as the
originators (parentsPopulation), the population where the new generation will be stored
(offspringsPopulation) and the probability to crossover two given individual in the population
which were selected to mate.

Both populations are passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will crossover the population
with the associated probability and return the offsprings in the appropriate
population.
Subsequent calls to setArguments can be used to change the population reference or
the probability of crossover.

The constructor receives a CrossoverOperator factory and a Selection method.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class PFMatingStrategy extends AbstractMatingStrategy implements Operator{

    private static int converged=0;

    private PFStringsPopulation parentsPopulation;
    private PFStringsPopulation offspringsPopulation;
    private SelectionMethod mySelectionMethod;
    private long lambda;
    private long matingPoolSize;
    private long gamma;
    private FitnessVisitor myFitnessVisitor;
    private ProblemFactory myProblemFactory;

  public PFMatingStrategy(CrossOverFactory aCrossoverFactory,SelectionMethod aSM, ProblemFactory myProblem)
    {
     super();
     setExecutor(new PFCrossOverOperator(aCrossoverFactory,myProblem));
     mySelectionMethod = aSM;
     myFitnessVisitor = new FitnessVisitor(myProblem);
     myProblemFactory=myProblem;
    }

  /** These are the methods from the Operator Interface. The Hashtable receives three associations,
      one with key="parentsPopulation" and value the population to be used as a source
      of parents, the other with key="offspringsPopulation" and value the population where
      the newly generated individuals will be stored,
      key="matingProbability" with a double value between 0 and 1, which is the pobability
      to mate a given set of individual of the population and lambda which is the
      number of ofssprings to generate. Also the mating pool size that the Selection method
      will produce should be provided.*/
  public void setArguments(Hashtable args)
    {
      offspringsPopulation = (PFStringsPopulation)(args.get("offspringsPopulation"));
      parentsPopulation    = (PFStringsPopulation)(args.get("parentsPopulation"));
      proX                 = ((Double)args.get("matingProbability")).doubleValue();
      lambda               = ((Long)args.get("lambda")).longValue();
      matingPoolSize       = ((Long)args.get("matingPoolSize")).longValue();
      gamma                = ((Long)args.get("gamma")).longValue();
      mySelectionMethod.setArguments(args);
    }



  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
      PFStringIndividual newOffspring;
      Individual parent1;
      Individual parent2;
      Individual RAMMatingPool[];
      Population matingPool;
      Hashtable args;
      Hashtable args2;
      long matingPoolSize2;
      int i;
      double rnd;
      long rp1;
      long rp2;
      Population tmpPopu;
      int numberOfTimesTried;
      SortingVisitor aSortingVisitor= new SortingVisitor();
      int tmpChrom[];
      int tried=0;
      boolean flag=false;




      /* We prepare the offspring population to receive the newborns */
      offspringsPopulation.extinct();


      /* We construct the mating pool */
      args = new Hashtable();
      args.put("parentsPopulation",parentsPopulation);
      args.put("matingPoolSize",new Long(matingPoolSize));
      mySelectionMethod.execute();
      args = mySelectionMethod.getResults();
      matingPool=(Population)(args.get("matingPool"));


      /* The mating season has arrived! */
      args2   = new Hashtable();
      matingPoolSize2 = (long)matingPool.getMembersNumber();
      RAMMatingPool = new Individual[(int)matingPoolSize2];
      i = 0;
      for(matingPool.first();matingPool.isInList();matingPool.advance())
    	{
    	  RAMMatingPool[i]=(Individual)(matingPool.retrieve());
    	  i++;
    	}
      i=0;

      tmpPopu = new Population();
      numberOfTimesTried = 0;

       while((i<gamma)&&(tried<gamma))
      	{
      	  rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
      	  do
      	    {
      	      rp1=MAFRA_Random.nextUniformLong(0,matingPoolSize2);
      	      rp2=MAFRA_Random.nextUniformLong(0,matingPoolSize2);


      	    } while (rp1==rp2);
      	  parent1 = (Individual) (RAMMatingPool[(int)rp1]);
      	  parent2 = (Individual) (RAMMatingPool[(int)rp2]);

      	  if (rnd<proX)
      	    { /* we decided to mate the individual */

      	      args2.put("parent1",parent1);
      	      args2.put("parent2",parent2);
      	      ((PFCrossOverOperator)executor).setArguments(args2);
      	      executor.execute();

      	      args = ((PFCrossOverOperator)executor).getResults();
      	      newOffspring =(PFStringIndividual)( ((PFStringIndividual)(args.get("offspring1"))).copyIndividual());
      	    }
      	  else
      	    {
      	      /* we arbitrarly choose to copy parent2 */
      	      newOffspring =(PFStringIndividual)(((PFStringIndividual)parent2).copyIndividual());
      	    }

      	  /* uncomment to have a population with possible duplicates */
      	  //offspringsPopulation.addIndividual(newOffspring);
      	  //i++;
      	  /* uncomment to have no duplicated individuals  */
      	  newOffspring.acceptVisitor(myFitnessVisitor);
      	  if(((PFStringsPopulation)offspringsPopulation).addIndividualIfCompatible((PFStringIndividual)newOffspring))
      	      {
          		  i++;
          		  tried=0;

      	      }
      	  else
      	      {
      		        tried++;
      	      }


      	}
       if(tried>=gamma)
  	   {/* we complete the population without concerns about its overcrowding*/
  	       flag=true;
  	       while(i<gamma)
  		   {
  		       /* we arbitrary create a random individual to complete gamma BUT we increment the number of converged populations*/
  		       newOffspring =new PFStringIndividual(myProblemFactory);
  		       newOffspring.acceptVisitor(myFitnessVisitor);
  		       ((PFStringsPopulation)offspringsPopulation).addIndividual((PFStringIndividual)newOffspring);

  		       i++;

  		   }
  	   }
       if(flag)
  	   {
  	       converged++;
  	       System.out.println("converged="+converged);

  	       if(converged>20)
  		   {
  		       System.out.println("Population Converged in contact map space");
  		       System.out.println("Finalizing run");
  		       System.exit(0);
  		   }
  	   }
         else
  	   {
  	       converged=0;
  	   }





    }






}












	  //if(tried>gamma)
	  //     {   /* we arbitrary create a random individual to complete gamma BUT we increment the number of converged populations*/
	  //  newOffspring =new SelfAdaptingStaticPFStringIndividual(myProblemFactory);
	  //  helperId = (int)(MAFRA_Random.nextUniformLong(0,numberOfHelpers));
	  //  ((SelfAdaptingStaticPFStringIndividual)newOffspring).setHelperId(helperId);
	  //  newOffspring.acceptVisitor(myFitnessVisitor);
	  //  ((PFStringsPopulation)offspringsPopulation).addIndividual((SelfAdaptingStaticPFStringIndividual)newOffspring);

	  //      i++;
	  //  flag=true;
	  //   }

//	}
       //  System.out.println("Converged="+converged);
       /*  if(flag)
	   {
	       converged++;

	       if(converged>20)
		   {
		       System.out.println("Population Converged in contact map space");
		       System.out.println("Finalizing run");
		       System.exit(0);
		   }
	   }
       else
	   {
	       converged=0;
	   }
       */
