



package MAFRA.Strategy;

import java.util.Hashtable;

import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;
import MAFRA.Executor.Operator;
import MAFRA.Executor.SelectionMethod;
import MAFRA.Executor.DPXCrossOverOperator;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class implements the mating procedures of the populations.
It receives three arguments which are the population to be used as the
originators (parentsPopulation), the population where the new generation will be stored
(offspringsPopulation) and the probability to crossover two given individual in the population
which were selected to mate.

Both populations are passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will crossover the population
with the associated probability and return the offsprings in the appropriate
population.
Subsequent calls to setArguments can be used to change the population reference or
the probability of crossover.

The constructor receives a CrossoverOperator factory and a Selection method.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class DPXMatingStrategy extends AbstractMatingStrategy implements Operator{

  private Population parentsPopulation;
  private Population offspringsPopulation;
  private SelectionMethod mySelectionMethod;
  private long lambda;
  private long matingPoolSize;
  private long gamma;

  public DPXMatingStrategy(CrossOverFactory aCrossoverFactory,SelectionMethod aSM, ProblemFactory myProblem)
    {
     super();
     setExecutor(new DPXCrossOverOperator(aCrossoverFactory,myProblem));
     mySelectionMethod = aSM;
    }

  /** This are the methods from the Operator Interface. The Hashtable receives three associations,
      one with key="parentsPopulation" and value the population to be used as a source
      of parents, the other with key="offspringsPopulation" and value the population where
      the newly generated individuals will be stored, 
      key="matingProbability" with a double value between 0 and 1, which is the pobability
      to mate a given set of individual of the population and lambda which is the
      number of ofssprings to generate. Also the mating pool size that the Selection method
      will produce should be provided.*/
  public void setArguments(Hashtable args)
    {
      offspringsPopulation = (Population)(args.get("offspringsPopulation"));
      parentsPopulation    = (Population)(args.get("parentsPopulation"));
      proX                 = ((Double)args.get("matingProbability")).doubleValue();
      lambda               = ((Long)args.get("lambda")).longValue();
      matingPoolSize       = ((Long)args.get("matingPoolSize")).longValue();
      gamma                = ((Long)args.get("gamma")).longValue();
      mySelectionMethod.setArguments(args);
    }
  


  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
      SelfAdaptingStaticTourIndividual newOffspring;
      Individual parent1;
      Individual parent2;
      Individual RAMMatingPool[];
      Population matingPool;
      Hashtable args;
      Hashtable args2;
      long matingPoolSize2;
      int i;
      double rnd;
      long rp1;
      long rp2;
      Population tmpPopu;
      int numberOfTimesTried;
      SortingVisitor aSortingVisitor= new SortingVisitor();
      int helperId;
      int tmpChrom[];
    


      /* We prepare the offspring population to receive the newborns */
      offspringsPopulation.extinct();


      /* We construct the mating pool */
      args = new Hashtable();
      args.put("parentsPopulation",parentsPopulation);
      args.put("matingPoolSize",new Long(matingPoolSize));
      mySelectionMethod.execute();
      args = mySelectionMethod.getResults();
      matingPool=(Population)(args.get("matingPool"));

      
      /* The mating season has arrived! */
      args2   = new Hashtable();
      matingPoolSize2 = (long)matingPool.getMembersNumber();
      RAMMatingPool = new Individual[(int)matingPoolSize2];
      i = 0;
      for(matingPool.first();matingPool.isInList();matingPool.advance())
	{
	  RAMMatingPool[i]=(Individual)(matingPool.retrieve());
	  i++;
	}
      i=0;

      tmpPopu = new Population();
      numberOfTimesTried = 0;
      
       while(i<gamma)
	{
	  rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	  do
	    {
	      rp1=MAFRA_Random.nextUniformLong(0,matingPoolSize2);
	      rp2=MAFRA_Random.nextUniformLong(0,matingPoolSize2);
	   

	    } while (rp1==rp2);
	  parent1 = (Individual) (RAMMatingPool[(int)rp1]);
	  parent2 = (Individual) (RAMMatingPool[(int)rp2]);

	  if (rnd<proX)
	    { /* we decided to mate the individual */
	  
	      args2.put("parent1",parent1);
	      args2.put("parent2",parent2);
	      ((DPXCrossOverOperator)executor).setArguments(args2);
	      executor.execute();
	     
	      args = ((DPXCrossOverOperator)executor).getResults();
	      newOffspring =(SelfAdaptingStaticTourIndividual)( ((SelfAdaptingStaticTourIndividual)(args.get("offspring1"))).copyIndividual());
	      /* We decide which helper to  inherit */
	      if(((SelfAdaptingStaticTourIndividual)parent1).getHelperId()==((SelfAdaptingStaticTourIndividual)parent2).getHelperId())
	      {
	       helperId = ((SelfAdaptingStaticTourIndividual)parent1).getHelperId();
	      }
	      else
	      {
	       if(parent1.compares(parent2)==1)
		{// parent1  has smaller fitness than parent2
		  helperId =( (SelfAdaptingStaticTourIndividual)parent2).getHelperId();
		}
	       else
		{// either parent1 has bigger fitness or same fitness
		  helperId = ((SelfAdaptingStaticTourIndividual)parent1).getHelperId();
		}
	      }
	    }
	  else
	    { /*we don't mate the individuals but  choose to copy the best parent 
	      if(parent1.compares(parent2)==1)
		{// parent1  has smaller fitness than parent2
		 newOffspring =(SelfAdaptingStaticTourIndividual)(((SelfAdaptingStaticTourIndividual)parent2).copyIndividual());

		}
	       else
		{// either parent1 has bigger fitness or same fitness
		 newOffspring =(SelfAdaptingStaticTourIndividual)(((SelfAdaptingStaticTourIndividual)parent1).copyIndividual());
		
		}*/
	      /* we arbitrarly choose to copy parent2 */
	      newOffspring =(SelfAdaptingStaticTourIndividual)(((SelfAdaptingStaticTourIndividual)parent2).copyIndividual());
	      tmpChrom     = permEncoding2EdgeEncoding((int[])(newOffspring.getChromosome()),((int)newOffspring.getSize()));
	      newOffspring.setChromosome( edgeEncoding2PermEncoding(tmpChrom,((int)newOffspring.getSize())));
	      helperId = ((SelfAdaptingStaticTourIndividual)parent1).getHelperId();
	    
	    }
	  ((SelfAdaptingStaticTourIndividual)newOffspring).setHelperId(helperId);
      	 
	    offspringsPopulation.addIndividual(newOffspring);
	    i++;
	 
	  }
	     
	 }
       
      
	    
	
 



 private int[] edgeEncoding2PermEncoding(int c[],int size)
 {
  int i;
  int c2[];
  int s;
  int e;
  
  c2 = new int[size];
  
  s=0;
  e=c[s];
  c2[0]=s;
  for(i=1;i<size;i++)
  {
   c2[i]=e;
   s=e;
   e=c[s];
  }
  return c2;
 }

 private int[] permEncoding2EdgeEncoding(int aTour[], int size)
 {/* podria estar mal por la ultima signacion a aTour[0]*/;
  int i;
  int finalTour[];
  
  finalTour = new int[size];
  
  for(i=0;i<(size-1);i++)
  {
   finalTour[aTour[i]]=aTour[i+1];
  }
  finalTour[aTour[(int)size-1]]=aTour[0];
  return finalTour;    
 }




}
