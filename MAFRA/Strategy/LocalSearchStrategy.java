






package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Executor.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class implements the Local Search procedures of the populations.
It receives two arguments which are the population and 
the probability to apply local search to a given individual in the population.
The population is passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will apply local search to the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of applying local search to an individual.

 @version Memetic Algorithms Framework - V 1.0 - September 1999
 @author  Natalio Krasnogor
 */
public class LocalSearchStrategy extends AbstractLocalSearchStrategy implements Operator{

  protected double         previousTemperature;
  protected double         temperature;

  public LocalSearchStrategy(LocalSearchFactory aLocalSearchFactory)
    {
     super(new BitSetLocalSearchOperator(aLocalSearchFactory));
     previousTemperature = 0.0;
     temperature         = 2.0;
    }

  /** This are the methods from the Operator Interface. The Hashtable receives three associations,
      one with key="Population" and value the population to be localy searched and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to apply local search to a given individual of the population. The third one is the problem factory,
      that the local search strategy MUST know in order to operate.*/
  public void setArguments(Hashtable args)
    {
      myPopulation = (Population)(args.get("Population"));
      proLocSearPerInd = ((Double)args.get("ProbabilityPerIndividual")).doubleValue();
      myProblemFactory = ((ProblemFactory)args.get("ProblemFactory"));
      myFitnessVisitor = new FitnessVisitor(myProblemFactory);
    }
  

  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     Individual tmp;
     double     rnd;
     Hashtable  args;
     Population tmpPop;
     //     double     temperature;
     double     delta;


     //     tmpPop = new Population();
     args       = new Hashtable();
     // this is for standard annealing
     //    delta      = Math.abs(myPopulation.getMaxFitness() - myPopulation.getMinFitness());
     /*     delta      = Math.abs(myPopulation.getMaxFitness() - myPopulation.getAvgFitness());
     if (delta >0)
       {
	 temperature = (1.0/delta);
	 previousTemperature = temperature;
       }
     else
       {
	 temperature = previousTemperature*2;
	 previousTemperature = temperature;
       }*/
     //this is for lineal annealing
      temperature = temperature * 0.9995; 
     // this is for non-adaptive, t=0;
     //temperature = 0.0;
     //     System.out.println("Max="+myPopulation.getMaxFitness()+",min="+myPopulation.getMinFitness()+" :---> Temp="+temperature);

     myPopulation.first();
     // this if for elitism when uncommented 
     myPopulation.advance();
     for(;myPopulation.isInList();myPopulation.advance())
       {
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	 tmp = (Individual) myPopulation.retrieve();
// 	 tmp.setTemperature(temperature);
         if (rnd<proLocSearPerInd)
	   { /* we decided to apply local search to the individual */
	     args.put("Individual",tmp);
	     args.put("Temperature",new Double(temperature));
	     args.put("PopSize",myPopulation.getMembersNumber());
	   //  args.put("Distances",((TSPProblem)myProblemFactory).getDistances());
	     args.put("FitnessVisitor",myFitnessVisitor);
	     ((Operator)executor).setArguments(args);
             executor.execute();
	   }
	   
       }

    }


}
