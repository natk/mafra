

package MAFRA.Strategy;

import MAFRA.Executor.Executor;


/** The Strategy class implements a Strategy pattern (together with all the other NameStrategy classes
and NameExecutor classes)
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class Strategy implements Executor{



  protected Executor executor;

  public Strategy()
    {
    }

  public Strategy(Executor anExecutor)
    {
      executor = anExecutor;
    }
  
  public void setExecutor(Executor anExecutor)
    {
      executor = anExecutor;
    }
  
  public Executor getExecutor()
    {
      return executor;
    }

  public void execute()
    {
      executor.execute();
    }
		    


}
