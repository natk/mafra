



package MAFRA.Strategy;

import java.util.Hashtable;

import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;
import MAFRA.Executor.Operator;
import MAFRA.Executor.SelectionMethod;
import MAFRA.Executor.PMXCrossOverOperator;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class implements the mating procedures of the populations.
It receives three arguments which are the population to be used as the
originators (parentsPopulation), the population where the new generation will be stored
(offspringsPopulation) and the probability to crossover two given individual in the population
which were selected to mate.

Both populations are passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will crossover the population
with the associated probability and return the offsprings in the appropriate
population.
Subsequent calls to setArguments can be used to change the population reference or
the probability of crossover.

The constructor receives a CrossoverOperator factory and a Selection method.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class PMXMatingStrategy extends AbstractMatingStrategy implements Operator{

  private Population parentsPopulation;
  private Population offspringsPopulation;
  private SelectionMethod mySelectionMethod;
  private long lambda;
  private long matingPoolSize;

  public PMXMatingStrategy(CrossOverFactory aCrossoverFactory,SelectionMethod aSM,ProblemFactory aProblem)
    {
     super();
     setExecutor(new PMXCrossOverOperator(aCrossoverFactory,aProblem));
     mySelectionMethod = aSM;
    }

  /** This are the methods from the Operator Interface. The Hashtable receives three associations,
      one with key="parentsPopulation" and value the population to be used as a source
      of parents, the other with key="offspringsPopulation" and value the population where
      the newly generated individuals will be stored, 
      key="matingProbability" with a double value between 0 and 1, which is the pobability
      to mate a given set of individual of the population and lambda which is the
      number of ofssprings to generate. Also the mating pool size that the Selection method
      will produce should be provided.*/
  public void setArguments(Hashtable args)
    {
      offspringsPopulation = (Population)(args.get("offspringsPopulation"));
      parentsPopulation    = (Population)(args.get("parentsPopulation"));
      proX                 = ((Double)args.get("matingProbability")).doubleValue();
      lambda               = ((Long)args.get("lambda")).longValue();
      matingPoolSize       = ((Long)args.get("matingPoolSize")).longValue();
      mySelectionMethod.setArguments(args);
    }
  


  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
      Individual newOffspring;
      Individual parent1;
      Individual parent2;
      Individual RAMMatingPool[];
      Population matingPool;
      Hashtable args;
      Hashtable args2;
      long matingPoolSize2;
      int i;
      double rnd;
      long rp1;
      long rp2;
      Population tmpPopu;
      int numberOfTimesTried;


      /* We prepare the offspring population to receive the newborns */
      offspringsPopulation.extinct();


      /* We construct the mating pool */
      args = new Hashtable();
      args.put("parentsPopulation",parentsPopulation);
      args.put("matingPoolSize",new Long(matingPoolSize));
      mySelectionMethod.execute();
      args = mySelectionMethod.getResults();
      matingPool=(Population)(args.get("matingPool"));

      
      /* The mating season has arrived! */
      args2   = new Hashtable();
      matingPoolSize2 = (long)matingPool.getMembersNumber();
      RAMMatingPool = new Individual[(int)matingPoolSize2];
      i = 0;
      for(matingPool.first();matingPool.isInList();matingPool.advance())
	{
	  RAMMatingPool[i]=(Individual)(matingPool.retrieve());
	  i++;
	}
      i=0;

      tmpPopu = new Population();
      numberOfTimesTried = 0;
      while(i<lambda)
	{
	  rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	  do
	    {
	      rp1=MAFRA_Random.nextUniformLong(0,matingPoolSize2);
	      rp2=MAFRA_Random.nextUniformLong(0,matingPoolSize2);
	   

	    } while (rp1==rp2);
	  parent1 = (Individual) (RAMMatingPool[(int)rp1]);
	  parent2 = (Individual) (RAMMatingPool[(int)rp2]);

	  if (rnd<proX)
	    { /* we decided to mate the individual */
	  
	      args2.put("parent1",parent1);
	      args2.put("parent2",parent2);
	      ((PMXCrossOverOperator)executor).setArguments(args2);
	      executor.execute();
	     
	      args = ((PMXCrossOverOperator)executor).getResults();
	      newOffspring = ((TourIndividual)(args.get("offspring1"))).copyIndividual();
	      //  TSPAWTDisplayVisitor.myTextArea.append("heme aqui\n"); 
	      //      System.out.println(newOffspring.equals(parent1) && newOffspring.equals(parent2));
	    }
	  else
	    { /*we don't mate the individuals but arbitrarly choose to copy parent1 */
	      newOffspring =parent1.copyIndividual();
	    }
	
	      offspringsPopulation.addIndividual(newOffspring);
	      i++;
	     
	   
	}

    }


}
