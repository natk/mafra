package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;


/** This class implements the mutation procedures of the populations.
This is a static self adapting strategy which means that the mutation operators
and parameters are fixed (static) but the individuals in the population self adapt
to learn to use the ones that profit them the most.
It receives at creation time a table containing the abstract mutations with the probabilites.
Later, succesive calls to execute() methods will mutated the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of mutating an individual.

 @version Memetic Algorithms Framework - V 1.0 - January 2000
 @author  Natalio Krasnogor
 */
public class SelfAdaptingStaticMutationStrategy extends AbstractMutationStrategy implements Operator{

  private Population myPopulation;
  private AbstractMutCrossProbConExpTable myHelpers;
 
 
  


  public SelfAdaptingStaticMutationStrategy(AbstractMutCrossProbConExpTable helpers)
    {
      super(new SelfAdaptingStaticMutationOperator());
      myHelpers  = helpers;
    }

  /** This are the methods from the Operator Interface. The Hashtable receives to associations,
      one with key="Population" and value the population to be mutated and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to mutate a given individual of the population.*/
  public void setArguments(Hashtable args)
    {
      myPopulation = (Population)(args.get("Population"));
      proMutPerInd = ((Double)args.get("ProbabilityPerIndLink")).doubleValue();
    }
  


  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     Individual tmp;
     double     rnd;
     Hashtable  args;
     Population tmpPop;
     double     proMutInd;
     int        helperId;


     //     tmpPop = new Population();
     args = new Hashtable();
     for(myPopulation.first();myPopulation.isInList();myPopulation.advance())
       {
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	 //	 System.out.println("obteniendo tmp de la poblacions");
	 tmp = (Individual)( myPopulation.retrieve());
	 
	  /* Now we mutate the link */
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
         if(rnd<proMutPerInd)
	   {
            ((SelfAdaptingStaticBitSetIndividual)tmp).setHelperId((int)(MAFRA_Random.nextUniformLong(0,myHelpers.getNumberOfHelpers()-1)));
	   }
	
	 
	 //System.out.println("getHelperId");
	 	 helperId =((SelfAdaptingStaticBitSetIndividual)tmp).getHelperId();
	 // helperId =((Individual)tmp).getHelperId();
	  proMutInd= myHelpers.getMutationHelperProbability(helperId);
         if (rnd<proMutInd)
	   { /* we decided to mutate the individual */
	     args.put("MutationOperator",myHelpers.getMutationHelper(helperId));
	     //  System.out.println("putting tmp");
	     args.put("Individual",tmp);
	     ((SelfAdaptingStaticMutationOperator)executor).setArguments(args);

             executor.execute();
	   }
	
	   
       }

    }


}
