

package MAFRA.Mutation;

/** This is a membership class which is superclass of all mutations under a BitSet encoding.
    @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */
public abstract class AbstractBitSetMutation extends AbstractMutation{



}
