

package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public class StringTwoPointMutation extends AbstractStringMutation{


 public Object individualMutation(Object parent1)
 
    {
      String p1;
      String c1;
      char temp[];
      long   rndPoint;
      long   rndMut; 
      
       

      p1       = (String)parent1;
      temp     = p1.toCharArray();
      rndPoint = MAFRA_Random.nextUniformLong(0,p1.length()-1);
      rndMut   = MAFRA_Random.nextUniformLong(0,getAlphabetSize());
      temp[rndPoint]= getSimbol(rndMut);

      rndPoint = MAFRA_Random.nextUniformLong(0,p1.length()-1);
      rndMut   = MAFRA_Random.nextUniformLong(0,getAlphabetSize());
      temp[rndPoint]= getSimbol(rndMut);
      
      c1 = copyValueOf(temp);
      return c1;
      
    }

}
