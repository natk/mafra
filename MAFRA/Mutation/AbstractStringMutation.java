

package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


/** This is a membership class which is superclass of all mutations under a string encoding.
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public abstract class AbstractStringMutation extends AbstractMutation implements StringData{



  /* StringData fields */
  private char alphabet[];
  private long alphabetSize;
  private boolean variableLength; /* variableLength=1 if the strings to be represented will be of variable
                                 length, 0 otherwise */
  private long stringLength;

  /* Implementation of the StringData methods */
  public void setAlphabetSize(long size)
    {
      alphabetSize = size;
    }

  public void setVariableLength(boolean variableL)
    {
      variableLength = variableL;
    }

  public void setStringLength(long lengthS)
    {
      stringLength = lengthS;
    }


  public void setSimbols(char simbols[])
    {
      int i;
     
      if(alphabetSize<=0)
      {
       System.out.println("ERROR - trying to set an alphabet but the size of the alphabet is not specified in AbstractStringMutation");
       System.exit(0);
      }
      alphabet = new char[(int)alphabetSize];     
      for(i=0;i<alphabetSize;i++)
	{
	  alphabet[i] = simbols[i];
	}
    }

  public long getAlphabetSize()
    {
      return alphabetSize;
    }





  public char []getAlphabet()
    {
	return  alphabet;
    }




  public boolean getVariableLength()
    {
      return variableLength;
    }

  public long getStringLength()
    {
      return stringLength;
    }

  public char getSimbol(long number)
    {
      return alphabet[(int)number];
    }

}




















