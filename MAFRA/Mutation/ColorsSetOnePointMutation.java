package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import java.util.ArrayList;
import java.util.Random;


/**This class work with mutations functions.
 @author  Jorge Habib Namour
 */

public class ColorsSetOnePointMutation extends AbstractBitSetMutation{


 public Individual individualMutation(Individual parent1)
 
    {
      ArrayList c1;
      char temp[];
      long   rndPoint;
      long   rndMut; 
      

      c1       = (ArrayList)parent1.getChromosome();
      
      
      rndPoint = MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()-1));
      Random ran = new Random();
      c1.set((int)rndPoint, ran.nextInt(MAFRA.Test.GCPTest.amountOfColors));
      
      return parent1;
      
    }

}
