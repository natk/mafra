
package MAFRA.AbstractEvolutionaryPlan;

import java.util.Hashtable;
import MAFRA.Strategy.*;
import MAFRA.Visitor.*;
import MAFRA.AbstractPlan.*;



/** CoEvolutionaryPlan class is a class that allows the implementation 
of co-evolutionary simulations. It uses several EvolutionaryPlan instances
as the subjects of co-evolution. It doesn't have many features in common with the individual
evolutionary processes but is grouped with EvolutionaryPlan  under the AbstractEvolutionaryPlan class
for clarity of concepts. Actually it shares  some implementation features with
the GA class.


The DoXXXX() methods  implement the  Pattern Template Method.
Subclasses override some or all DoXXXX() methods to provide with the appropriate behaviour
for the run() methods. (Eventually also run can be overrided) 

 @version Memetic Algorithms Framework - V 1.0 - Setember 1999
 @author  Natalio Krasnogor
 */
public class CoEvolutionaryPlan extends AbstractEvolutionaryPlan{

 private Hashtable evolutionaryPlans;
 private Hashtable parameters;
 private Hashtable visitors;

 private FinalizationStrategy   myFinalizationStrategy;
 private InitializePopStrategy  myInitializePopStrategy;
 private RestartCoEvolvingPopStrategy     myRestartCoEvolvingPopStrategy;

 private long generationsNow;

 public CoEvolutionaryPlan()
   {
     parameters        = new Hashtable();
     visitors          = new Hashtable();
     evolutionaryPlans = new Hashtable();
     generationsNow = 0;
   }

 /* Set and access methods for the private variables */

 /** Adds the parameter aName to the CoEvolutionaryPlan parameter list with a value of aParameter */
 public void addParameter(String aName,Object  aParameter)
   {
     parameters.put(aName,aParameter);
   }

 /** Adds aVisitor as aName to the CoEvolutionaryPlan's visitors list. */
 public void addVisitor(String aName, Visitor aVisitor)
   {
    visitors.put(aName,aVisitor);
   }

 /** Adds an EvolutionaryPlan to the CoEvolutionaryPlan's EvolutionaryPlan list.
   The name of the plan will be "evolutionaryPlan"+(number_of_plans_coevolving+1) 
   and will be returned for future references.*/
 public String addEvolutionaryPlan(EvolutionaryPlan anEvolutionaryPlan)
   {
     String aName;

     aName = new String("evolutionaryPlan"+(evolutionaryPlans.size()+1));
     evolutionaryPlans.put(aName,anEvolutionaryPlan);
     return aName;
   }


 /** Set the number of generations of co-evolutionary steps */
 public void setGenerationsNow(long aGNumber)
 {
   generationsNow = aGNumber;
 }

 /** Returns the value of the parameter aName */
 public Object getParameter(String aName)
   {
    return (parameters.get(aName));
   }


 /** Returns the visitor whose name is aName */
 public Visitor getVisitor(String aName)
   {
    return ((Visitor)(visitors.get(aName)));
   }

 /** Returns the evolutionary plan whose name is aName */
 public EvolutionaryPlan getEvolutionaryPlan(String aName)
   {
     return((EvolutionaryPlan)(evolutionaryPlans.get(aName)));
   }


 public long getGenerationsNow()
   {
     return generationsNow;
   }

 /* SetStrategies Interface implementation */

 public void addRestartCoEvolvingPopStrategy(RestartCoEvolvingPopStrategy aS)
   {
    myRestartCoEvolvingPopStrategy = aS;
   } 

 public void addInitializeStrategy(InitializePopStrategy aS)
   {
    myInitializePopStrategy = aS;
   } 

 public void addFinalizationStrategy(FinalizationStrategy aS)
   {
    myFinalizationStrategy = aS;
   } 

 public RestartCoEvolvingPopStrategy getRestartCoEvolvingPopStrategy()
   {
     return(myRestartCoEvolvingPopStrategy);
   }

 public InitializePopStrategy getInitializePopStrategy()
   {
     return(myInitializePopStrategy);
   }
 public FinalizationStrategy getFinalizationStrategy()
   {
     return(myFinalizationStrategy);
   }





 public void DoRestartCoEvolution()
   {
     myRestartCoEvolvingPopStrategy.execute();
   }; 

 public void DoInitCoEvolution()
   {
    myRestartCoEvolvingPopStrategy .setArguments(evolutionaryPlans);
   };

 public void DoFinalizeCoEvolution()
   {};

 public boolean DoCheckTerminationCoEvolution()
   {
     long maxGenerations;

     maxGenerations = ((Long)(parameters.get("maxGenerationsNumber"))).longValue();
     return(generationsNow>=maxGenerations);



   };

 /** Executes the CoEvolutionary plan.*/

 public void run()
   {
     int numberOfPlans;
     int j;
     EvolutionaryPlan anEvPlan;
     String name;
     
    
     numberOfPlans = evolutionaryPlans.size();
     DoInitCoEvolution();
     
     while(!DoCheckTerminationCoEvolution())
       {
	 for(j=0;j<numberOfPlans;j++)
	   {
	     name = new String("evolutionaryPlan"+(j+1));
	     anEvPlan = this.getEvolutionaryPlan(name);
	     System.out.println("********************* ********************* Evolving "+name+" ********************* ********************* ");
	     
	     anEvPlan.run();
	   }
	 generationsNow ++;
	 DoRestartCoEvolution();

       }
      
     DoFinalizeCoEvolution();
    

   }




}
