
package MAFRA.AbstractEvolutionaryPlan;

import MAFRA.Util.*;
import MAFRA.AbstractPlan.*;
import MAFRA.Factory.*;



/** EvolutionaryPlan class is the class that holds the three main parts of any
evolutionary system: a GA, a Problem to solve and a Plan to follow with the ga to solve
the problem.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class EvolutionaryPlan extends AbstractEvolutionaryPlan{

 private GA           myGA;
 private ProblemFactory      myProblem;
 private Plan         myPlan;
 private Statistician myStatistician;
 private Display      myDisplay;


 public  EvolutionaryPlan(GA aGA, ProblemFactory aProblem, Plan aPlan, Statistician aStat, Display aDisp)
   {
     setGA(aGA);
     setProblem(aProblem);
     setPlan(aPlan);
     setStatistician(aStat);
     setDisplay(aDisp);


   }

 public void run()
   {
    if(myPlan.getGA()==null)
      {
	       myPlan.setGA(myGA);
      }
    myPlan.run();
   }

 public void setDisplay(Display aDisp)
   {
     myDisplay = aDisp;
   }

 public Display getDisplay()
   {
     return myDisplay;
   }

 public void setPlan(Plan aPlan)
   {
     myPlan = aPlan;
   }

 public Plan getPlan()
   {
     return myPlan;
   }

 public void setStatistician(Statistician myStat)
   {
     myStatistician = myStat;
   }

 public Statistician getStatistician()
   {
     return myStatistician;
   }

 public void setProblem(ProblemFactory aProblem)
   {
     myProblem = aProblem;
   }

 public ProblemFactory getProblem()
   {
     return myProblem;
   }


 public void setGA(GA aGA)
   {
     myGA = aGA;
   }

 public GA getGA()
   {
     return myGA;
   }


}
