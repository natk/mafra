

package MAFRA.CrossOver;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;


/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourOnePointCrossOver extends AbstractTourCrossOver{
  /* This class assumes that the parents are all of the same size */

 public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      TourIndividual p1;
      TourIndividual p2;
      TourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      boolean alreadyInTour[];
      int    rndPoint;
      int size;
      int i;
      int pos;
       
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (TourIndividual)parent1;
      c1 = (int[])(p1.getChromosome());
      p2 = (TourIndividual)parent2;
      c2 = (int[])(p2.getChromosome());
      size =(int) p1.getSize();
      alreadyInTour = new boolean[size];
      for (i=0;i<size;i++)
	{
	  alreadyInTour[i] = false;
	 
	}
      rndPoint = (int) MAFRA_Random.nextUniformLong(0,size-1);

      c3= new int[size];
      /* We pass to offspring the first i links from parent1 */
      for(i=0;i<rndPoint;i++)
	{
	  alreadyInTour[c1[i]]=true;
	  c3[i] = c1[i];
	}

      /* We complete with parent2 links */
      pos = 0;
      while((i<size)&&(pos<size))
	{
	  if(!alreadyInTour[c2[pos]])
	     {
	       c3[i]=c2[pos];
	       alreadyInTour[c2[pos]] = true;
	       i++;
	     }
	  pos++;
	}
    
      offs = new TourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(c3);
   

      return (Object)offs;
      
    }

}
