package MAFRA.CrossOver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import MAFRA.Util.*;
import MAFRA.Population_Individual.*;
import java.util.Random;

/** This class make the cross of the parents and generate a child.
 This class assumes that both parents are of the same length.
    @author  Jorge Habib Namour
    */

public class ColorsSetOnePointCrossOver extends AbstractBitSetCrossOver{
  

    public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      ArrayList p1;
      ArrayList p2;
      ArrayList childChromosome;
      ColorsSetIndividual child;
      long   rndPoint;
      int    i;
      int    size;

      p1 = (ArrayList)(((Individual)parent1).getChromosome());
      p2 = (ArrayList)(((Individual)parent2).getChromosome());
      size =(int)( ((Individual)parent1).getSize());
      childChromosome = new ArrayList();
      Random ran = new Random();
      rndPoint = ran.nextInt((int)size);

      for (i = 0; i <= rndPoint; i++) {
	  childChromosome.add(i, p1.get(i));
      }
      for (; i < size; i++) {
	  childChromosome.add(i, p2.get(i));
      }
 
      child = new ColorsSetIndividual();
      child.setChromosome(childChromosome);

      child.setFitness(updateFitness(childChromosome));

      if(updateFitness(childChromosome) == 0)
	  {
              System.out.println("-----------------");
              System.out.println("-----------------");
	      System.out.println(">> Solution found! Chromosome solution: "+childChromosome);
	      long amountOfColors = childChromosome.stream().distinct().count();
	      int[] ret = new int[childChromosome.size()];
              Iterator<Integer> iterator = childChromosome.iterator();
	      for (int j=0; j < ret.length; j++)
	      {
		  ret[j] = iterator.next().intValue();
	      }
	      Drawer drawer = new Drawer(MAFRA.Test.GCPTest.aGraph, ret, (int)amountOfColors);
	      
	      try {
		  TimeUnit.SECONDS.sleep(10);
                  System.exit(0);
	      } catch (Exception e) {System.err.format("IOException: %s%n", e);}	      
	      }

	  child.setSize(((Individual)parent1).getSize());
      return (Object)child;
      
    }
      
      private int updateFitness(ArrayList chromosome)
      {
	  int f = 0;
	  System.out.println("Chromosome: "+chromosome);

	  for (int v = 0; v < MAFRA.Test.GCPTest.aGraph.V(); v++) {
	      for (int w : MAFRA.Test.GCPTest.aGraph.adj(v)) {
		  if (chromosome.get(v) == chromosome.get(w))
		      {
  		        f++;
		      }
	      }
	  }
	  f = f / 2;
	  System.out.println("Fitness: "+f);
	  return f;
      }
    

}
