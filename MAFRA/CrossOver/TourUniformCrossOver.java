

package MAFRA.CrossOver;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;


/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourUniformCrossOver extends AbstractTourCrossOver{
  /* This class assumes that the parents are all of the same size */

 public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      TourIndividual p1;
      TourIndividual p2;
      TourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      boolean alreadyInTour[];
      double    rndPoint;
      int size;
      int i;
      int indexC1;
      int indexC2;
       
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (TourIndividual)parent1;
      c1 = (int[])(p1.getChromosome());
      p2 = (TourIndividual)parent2;
      c2 = (int[])(p2.getChromosome());
      size =(int) p1.getSize();

      c3= new int[size];
      alreadyInTour = new boolean[size];
      indexC1 = 0;
      indexC2 = 0;
      for(i=0;i<size;i++)
	{
	  alreadyInTour[i] = false;
	}

      i = 0;
      while(i<size)
	{
	  rndPoint =  MAFRA_Random.nextUniformDouble(0,1);
	 
	  if(rndPoint<=0.5)
	    {
	      if(!alreadyInTour[c1[indexC1]])
		{
		  c3[i]                  = c1[indexC1];
		  alreadyInTour[c1[indexC1]] = true;
		  i++;
		}
	      indexC1++;
	    }
	  else
	    {
	      if (!alreadyInTour[c2[indexC2]])
		{
		  c3[i] = c2[indexC2];
		  alreadyInTour[c2[indexC2]] = true;
		  i++;
		}
	      indexC2++;
	    }

	}
      

      offs = new TourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(c3);
   

      return (Object)offs;
      
      }

}
