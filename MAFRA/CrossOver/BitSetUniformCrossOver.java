




package MAFRA.CrossOver;

import java.util.BitSet;
import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */
public class BitSetUniformCrossOver extends AbstractBitSetCrossOver{
  /* this class assumes that both parents are of the same length */

 public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      BitSet p1;
      BitSet p2;
      BitSet childChromosome;
      BitSetIndividual child;
      double   rndParent;
      int    i;
      int    size;
       

      p1 = (BitSet)(((Individual)parent1).getChromosome());
      p2 = (BitSet)(((Individual)parent2).getChromosome());
      size =(int)( ((Individual)parent1).getSize());
      childChromosome = new BitSet();

      for (i=0;i<size;i++)
	{
	  rndParent = MAFRA_Random.nextUniformDouble(0.0,1.0);
	  if(rndParent<0.5)
	    {
	      if(p1.get(i))
		{
		  childChromosome.set(i);
		}
	      else
		{
		  childChromosome.clear(i);
		}
	    }
	  else
	    {
	      if(p2.get(i))
		{
		  childChromosome.set(i);
		}
	      else
		{
		  childChromosome.clear(i);
		}
	    }

	  
	}

      child = new BitSetIndividual();
      child.setChromosome(childChromosome);
      child.setFitness(0.0);
      child.setSize(((Individual)parent1).getSize());
      return (Object)child;

      
    }

}
