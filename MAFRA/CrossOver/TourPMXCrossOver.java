

package MAFRA.CrossOver;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;


/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourPMXCrossOver extends AbstractTourCrossOver{
  /* This class assumes that the parents are all of the same size */

 public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      TourIndividual p1;
      TourIndividual p2;
      TourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      boolean alreadyInTour[];
      long    rndPoint1;
      long    rndPoint2;
      long    tmp;
      int size;
      int i,j;
      int rndCity;
      boolean notFoundYet;

       
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (TourIndividual)parent1;
      c1 = (int[])(p1.getChromosome());
      p2 = (TourIndividual)parent2;
      c2 = (int[])(p2.getChromosome());
      size =(int) p1.getSize();
      rndPoint1 = MAFRA_Random.nextUniformLong(0,size-1);
      rndPoint2 = MAFRA_Random.nextUniformLong(0,size-2);
      if(rndPoint1>rndPoint2)
	{
	  tmp       = rndPoint1;
	  rndPoint1 = rndPoint2;
	  rndPoint2 = tmp;
	}
      c3= new int[size];
      alreadyInTour = new boolean[size];
      for(i=0;i<size;i++)
	{
	  alreadyInTour[i] = false;
	}
      for (i=0;i<size;i++)
	{
	  c3[i]=c1[i];
	}
      for (i=(int)rndPoint1;i<=(int)rndPoint2;i++)
	{
	  c3[i] = c2[i];
	  alreadyInTour[c3[i]] = true;
	}
      for(i=0;i<(int)rndPoint1;i++)
	{
	  if(inSubTour(c3[i],c3,rndPoint1,rndPoint2))
	    {
	      c3[i]=-1;
	    }
	  else
	    {
	      alreadyInTour[c3[i]]=true;
	    }
	}
      for(i=(int)rndPoint2+1;(int)i<size;i++)
	{
	  if(inSubTour(c3[i],c3,rndPoint1,rndPoint2))
	    {
	      c3[i]=-1;
	    }
	  else
	    {
	      alreadyInTour[c3[i]]=true;
	    }
	}
      for(i=0;i<size;i++)
	{
	  if(c3[i]<0)
	    {
	     notFoundYet = true;
	     for (j=0;(j<size)&&notFoundYet;j++)
	       {
		 if (!alreadyInTour[j])
		   {
	      		 c3[i]=j;
			 alreadyInTour[j]=true;
			 notFoundYet = false;
		   }
	       }
	    }
	}

      offs = new TourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(c3);
   

      return (Object)offs;
      
      }

private boolean inSubTour(int city,int cities[],long startPoint, long endPoint)
{
 int i;
 boolean result=false;

 for (i=(int)startPoint;(i<=(int)endPoint)&&(!result);i++)
   {
    result = result || (city==cities[i]);
   }
 
 return result;

}


}
