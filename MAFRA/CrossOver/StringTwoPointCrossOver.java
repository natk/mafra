

package MAFRA.CrossOver;


import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public class StringTwoPointCrossOver extends AbstractStringCrossOver{
  /* This class assumes that the parents are all of the same size */

 public Object twoParentsCrossover(Object parent1,Object parent2)

    {
      String p1;
      String p2;
      String c1;
      int    rndPoint1;
      int    rndPoint2;
      int    rndPoint3;
      int    rndPoint4;
      int    end;
      int    tmp;
      int    L;
      StringIndividual offs;


      p1        = (String)(((Individual)parent1).getChromosome());
      p2        = (String)(((Individual)parent2).getChromosome());
      L         = p1.length();
 //     System.out.println("Entre Xs: p1="+p1+" , p2="+p2);
      do
      {
       rndPoint1 = (int)( MAFRA_Random.nextUniformLong(0,L) );
       rndPoint2 = (int)( MAFRA_Random.nextUniformLong(0,L) );

       if (rndPoint1>rndPoint2)
       {
      	tmp       = rndPoint1;
      	rndPoint1 = rndPoint2;
      	rndPoint2 = tmp;
       }
      // System.out.println("L="+L+", rndPoint1="+rndPoint1+", rndPoint2="+rndPoint2);
      } while( (L-rndPoint2+rndPoint1<5)||(rndPoint2==rndPoint1));
     // System.out.println("Sali del do 1");

      if((L-rndPoint2)>=rndPoint1)
      { /* we have more space to the right of point rndPoint2 */
       do
       {
	        rndPoint3 = (int)( MAFRA_Random.nextUniformLong(rndPoint2+1,L) );
          rndPoint4 = (int)( MAFRA_Random.nextUniformLong(rndPoint2+1,L) );
       } while (rndPoint3==rndPoint4);
      }
      else
      { /* we have more space to the left of point rndPoint1  */
       do
       {
        rndPoint3 = (int)( MAFRA_Random.nextUniformLong(0,rndPoint1-1) );
        rndPoint4 = (int)( MAFRA_Random.nextUniformLong(0,rndPoint1-1) );
       } while (rndPoint3==rndPoint4);
      }
      if (rndPoint3>rndPoint4)
      {
       tmp       = rndPoint3;
       rndPoint3 = rndPoint4;
       rndPoint4 = tmp;
      }

   //   System.out.println("r1="+rndPoint1+", r2="+rndPoint2+", r3="+rndPoint3+", r4="+rndPoint4);


      if(rndPoint4<rndPoint1)
  	  {
  	   c1 = p1.substring(0,rndPoint3)+p2.substring(rndPoint3,rndPoint4)+p1.substring(rndPoint4,rndPoint1)+
  	        p2.substring(rndPoint1,rndPoint2)+p1.substring(rndPoint2);
  	  }
  	  else
  	  {
  	   c1 = p1.substring(0,rndPoint1)+p2.substring(rndPoint1,rndPoint2)+p1.substring(rndPoint2,rndPoint3)+
  	        p2.substring(rndPoint3,rndPoint4)+p1.substring(rndPoint4);
  	  }

      offs = new StringIndividual();
      offs.setFitness(Double.NEGATIVE_INFINITY);
      offs.setSize(L+1);
      offs.setChromosome(c1);

    //  System.out.println("Sali Xs="+c1);
    //  System.out.println();
      return (Object)offs;


     }



}
