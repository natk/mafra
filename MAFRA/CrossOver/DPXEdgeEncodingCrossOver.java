

package MAFRA.CrossOver;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;
import MAFRA.Factory.*;


/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    M3012-1
    */
public class DPXEdgeEncodingCrossOver extends AbstractTourCrossOver{
  /* This class assumes that the parents are all of the same size */


  public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      TourIndividual p1;
      TourIndividual p2;
      TourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      int permTour[];
      boolean in[];
      boolean out[];
      long    rndPoint1;
      long    rndPoint2;
      long    tmp;
      int size;
      int i,j;
      int rndCity;
      boolean OK;
      int startCity;
      int endCity;
 
      int rndNumber;
      double tot1;
      double tot2;  
      double distances[][];
      
     // System.out.println("Construyendo hijo");
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (TourIndividual)parent1;
      c1 = (int[])(p1.getChromosome());
      p2 = (TourIndividual)parent2;
      c2 = (int[])(p2.getChromosome());
      size =(int) p1.getSize();
   

      c3= new int[size];
      in  = new boolean[size];
      out = new boolean[size];
      permTour      = new int[size];
      for(i=0;i<size;i++)
	{
	  in[i] = false;
	  out[i] = false;
	  permTour[i]      = i;
	  c3[i]            = -1;
	}
      for (i=0;i<2*size;i++)
      {
       twoSwap(permTour,size);
      }

     
     for (i=0;i<size;i++)
     {
      startCity = i;
      endCity   = c1[startCity];
      if( (c2[startCity]==endCity) || (c2[endCity]==startCity) )
      {
       /* the link startCity<--->endCity is in both parents */
       /* this preserves feasibility */
       c3[startCity]  = endCity;
       in[endCity]    = true;
       out[startCity] = true;
          //   System.out.println(" edge comun"+startCity+"->"+endCity);
      }
     }
     
     for (i=0;i<size;i++)
     {
      if(c3[i] == -1)
      {
       /* this preserves feasibility */
       completeDP(c3,c1,c2,i,permTour,in,out,size);
      }
     }
     
     if(!readyTour(c3,size)) /* that is, the tour in incomplete */
     {
      complete(c3,permTour,in,out,size);
     }
 
   
    //  System.out.println("****"+i+"****");
      offs = new TourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(c3);
   

      return (Object)offs;
      
      }
      
    
void completeDP(int c3[],int c1[],int c2[],int i, int perm[],boolean in[], boolean out[], int size)
{
 int j;
 int city;
 boolean completed=false;
 
 j=0;
 do
 {
  city = perm[j];
  if( (c1[i]!=city) && (c1[city]!=i) && (c2[i]!=city) && (c2[city]!=i) )
  {
  
   if (feasible(c3,i,city,in,out,size) )
   {
    c3[i]     = city; /* the link i<--->city is not in any of the parents */
    out[i]    = true;
    in[city]  = true;
    completed = true;
   }
  
  }
  
  j++;
  
  
 } while((j<size)&&(!completed));
 
}  

   
    
void complete(int c3[],int perm[],boolean in[],boolean out[], int size)
{
 int j=0;
 int i;
 int city;
 boolean completed=false;
 
 
 for (i=0;i<size;i++)
 {
  if(c3[i]==-1)
  {
   j=0;
   completed = false;
   do
   {
    city = perm[j];
    if (feasible(c3,i,city,in,out,size))
    {
     c3[i]     = city;
     out[i]    = true;
     in[city]  = true;
     completed = true;
    }
   
    j++;
   } while((j<size)&&(!completed));  
   if(j>size) System.out.println("ERROR completing child in DPXEdgeEncodingCrossOver.java"); 
  }
 
 }
 
 

 
} 


boolean feasible(int t[],int start, int end,boolean in[],boolean out[],int size)
{
 boolean res=true;
 
 if(end==start)
 {/* self-loop */
  res = false;
 }
 else
 {
  if(in[end])
  {/* somebody else is pointing to the same city */
   res = false;
  }
  else
  {
   if (out[end])
   {
    res = !checkSubCicle(t,start,end,size);
   }
   
  }
 }
 
 return res;
}

boolean checkSubCicle(int t[],int start, int end, int size)
{
 int count=1;
 int tmpS=end;
 
 do
 {
  end= t[tmpS];
  count++;
  tmpS=end;
 }while((tmpS!=start)&&(tmpS!=-1));
 
 return((tmpS==start)&&(count<size));
 
 
 
}



boolean readyTour(int c[],int size)
{
 int start;
 int end;
 int prev;
 int count =0;
 int tour[];
 int i;
 
 tour      = new int[size];
 for(i=0;i<size;i++)
 {
  tour[i] = c[i];
 }
 
 
 start = 0;
 end   =tour[start];
 while(end>=0)
 {
  prev  = start;
  start = end;
  end   = tour[start];
  tour[prev] = -2;
  count++;
  
 }
 
 return(count==size);
 
}



private void twoSwap(int c[],long size)
{
  int city1;
  int city2;
  int tmpCity;
  
  city1 = (int)MAFRA_Random.nextUniformLong(0,size);
  city2 = (int)MAFRA_Random.nextUniformLong(0,size);
  
  tmpCity  = c[city1];
  c[city1] = c[city2];
  c[city2] = tmpCity;
  
}



}
