

package MAFRA.CrossOver;
/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

import MAFRA.Util.MAFRA_Random;

public class StringOnePointCrossOver extends AbstractStringCrossOver{
  /* This class assumes that the parents are all of the same size */

 public Object twoParentsCrossover(Object parent1,Object parent2)
 
    {
      String p1;
      String p2;
      String c1;
      long    rndPoint;
       

      p1 = (String)parent1;
      p2 = (String)parent2;
      rndPoint = MAFRA_Random.nextUniformLong(0,p1.length()-1);
      c1 = p1.substring(0,(int)rndPoint)+p2.substring((int)(rndPoint+1),(int)(p2.length()-1));
      return c1;
      
    }

}
