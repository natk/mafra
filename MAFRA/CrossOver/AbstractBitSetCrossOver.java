
package MAFRA.CrossOver;
import MAFRA.Population_Individual.*;

/** This is a membership class which is superclass of all crossovers under a BitSet encoding.
    @version Memetic Algorithms Framework - V 1.0 -August 1999
    @author  Natalio Krasnogor
    */

public abstract class AbstractBitSetCrossOver extends AbstractCrossOver{

}
