

package MAFRA.CrossOver;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;
import MAFRA.Factory.*;


/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    M3012-1
    */
public class DPXTourEncodingCrossOver extends AbstractTourCrossOver{
  /* This class assumes that the parents are all of the same size */


  public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      SelfAdaptingStaticTourIndividual p1;
      SelfAdaptingStaticTourIndividual p2;
      SelfAdaptingStaticTourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      int permTour[];
      boolean in[];
      boolean out[];
      long    rndPoint1;
      long    rndPoint2;
      long    tmp;
      int size;
      int i,j;
      int rndCity;
      boolean OK;
      int startCity;
      int endCity;
 
      int rndNumber;
      double tot1;
      double tot2;  
      double distances[][];
      
     // System.out.println("Construyendo hijo");
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (SelfAdaptingStaticTourIndividual)parent1;
      size =(int) p1.getSize();
      c1 = permEncoding2EdgeEncoding((int[])(p1.getChromosome()),size);
      p2 = (SelfAdaptingStaticTourIndividual)parent2;
      c2 = permEncoding2EdgeEncoding((int[])(p2.getChromosome()),size);

   
      
      c3= new int[size];
      in  = new boolean[size];
      out = new boolean[size];
      permTour      = new int[size];
      for(i=0;i<size;i++)
	{
	  in[i] = false;
	  out[i] = false;
	  permTour[i]      = i;
	  c3[i]            = -1;
	}
      for (i=0;i<2*size;i++)
      {
       twoSwap(permTour,size);
      }

     //System.out.println("NEW OFFSPRING");
     for (i=0;i<size;i++)
     {
      startCity = i;
      endCity   = c1[startCity];
      if( (c2[startCity]==endCity) || (c2[endCity]==startCity) )
      {
       /* the link startCity<--->endCity is in both parents */
       /* this preserves feasibility */
       c3[startCity]  = endCity;
       in[endCity]    = true;
       out[startCity] = true;
     //  System.out.println(" edge comun"+startCity+"->"+endCity);
      }
     }
     
     for (i=0;i<size;i++)
     {
      if(c3[i] == -1)
      {
       /* this preserves feasibility */
       /* we try first to complete with nearest neighbours information & preserving distances */
       completeDP(c3,c1,c2,i,permTour,in,out,size);
      // completeNNDP(c3,c1,c2,i,in,out,size);
      }
     }
     
     for (i=0;i<size;i++)
     {
      if(c3[i] == -1)
      {
       /* this preserves feasibility */
       /* we try next to complete preserving just distances */
       completeDP(c3,c1,c2,i,permTour,in,out,size);
       //completeNNDP(c3,c1,c2,i,in,out,size);
      }
     }
     
     if(!readyTour(c3,size)) /* that is, the tour is incomplete */
     { /* forget about preserving distances... */
      complete(c3,permTour,in,out,size);
      //completeNN(c3,in,out,size);
     }
 
   
    //  System.out.println("****"+i+"****");
      offs = new SelfAdaptingStaticTourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(edgeEncoding2PermEncoding(c3,size));
   

      return (Object)offs;
      
      }
      
    
void completeDP(int c3[],int c1[],int c2[],int i, int perm[],boolean in[], boolean out[], int size)
{
 int j;
 int city;
 boolean completed=false;
 
 j=0;
 do
 {
  city = perm[j];
  if( (c1[i]!=city) && (c1[city]!=i) && (c2[i]!=city) && (c2[city]!=i) )
  {
  
   if (feasible(c3,i,city,in,out,size) )
   {
    c3[i]     = city; /* the link i<--->city is not in any of the parents and is feasible*/
    out[i]    = true;
    in[city]  = true;
    completed = true;
   }
  
  }
  
  j++;
  
  
 } while((j<size)&&(!completed));
 
} 



void completeNNDP(int c3[],int c1[],int c2[],int i,boolean in[], boolean out[], int size)
{
 int city;
 boolean completed=false;
  
// System.out.println("***Analizing completion***");
 TSPProblem.NearestNeighbours[i].first();
 do
 {
  city = ( (Edge)( TSPProblem.NearestNeighbours[i].retrieve())).getDestiny();
//  System.out.print(( (Edge)( TSPProblem.NearestNeighbours[i].retrieve())).getLength()+",");
  if( (c1[i]!=city) && (c1[city]!=i) && (c2[i]!=city) && (c2[city]!=i) )
  {
  
   if (feasible(c3,i,city,in,out,size) )
   {
    c3[i]     = city; /* the link i<--->city is not in any of the parents */
    out[i]    = true;
    in[city]  = true;
    completed = true;
  //  System.out.println("");
  //  System.out.println("Elegida:"+(( (Edge)( TSPProblem.NearestNeighbours[i].retrieve())).getLength()));
   }
  
  }
  TSPProblem.NearestNeighbours[i].advance();
 } while((TSPProblem.NearestNeighbours[i].isInList()) && (!completed));
 
}  




   
    
void complete(int c3[],int perm[],boolean in[],boolean out[], int size)
{
 int j=0;
 int i;
 int city;
 boolean completed=false;
 
 
 for (i=0;i<size;i++)
 {
  if(c3[i]==-1)
  {
   j=0;
   completed = false;
   do
   {
    city = perm[j];
    if (feasible(c3,i,city,in,out,size))
    {
     c3[i]     = city;
     out[i]    = true;
     in[city]  = true;
     completed = true;
    }
   
    j++;
   } while((j<size)&&(!completed));  
   if(j>size) System.out.println("ERROR completing child in DPXEdgeEncodingCrossOver.java"); 
  }
 
 }
 
 

 
}

 

void completeNN(int c3[],boolean in[],boolean out[], int size)
{
 int city;
 boolean completed=false;
 int i;
 int j;
  

 for (i=0;i<size;i++)
 {
  if(c3[i]==-1)
  {
   TSPProblem.NearestNeighbours[i].first();
   completed = false;
   j=0;
   do
   {
    city = ( (Edge)( TSPProblem.NearestNeighbours[i].retrieve())).getDestiny();
    if (feasible(c3,i,city,in,out,size) )
    {
     c3[i]     = city;
     out[i]    = true;
     in[city]  = true;
     completed = true;
    }
    TSPProblem.NearestNeighbours[i].advance();
    j++;
   } while((TSPProblem.NearestNeighbours[i].isInList()) && (!completed));
   if(j>size) System.out.println("ERROR completing child in DPXEdgeEncodingCrossOver.java"); 
  }
 }
}     
    
    
    
    
    
boolean feasible(int t[],int start, int end,boolean in[],boolean out[],int size)
{
 boolean res=true;
 
 if(end==start)
 {/* self-loop */
  res = false;
 }
 else
 {
  if(in[end])
  {/* somebody else is pointing to the same city */
   res = false;
  }
  else
  {
   if (out[end])
   {
    res = !checkSubCicle(t,start,end,size);
   }
   
  }
 }
 
 return res;
}

boolean checkSubCicle(int t[],int start, int end, int size)
{
 int count=1;
 int tmpS=end;
 
 do
 {
  end= t[tmpS];
  count++;
  tmpS=end;
 }while((tmpS!=start)&&(tmpS!=-1));
 
 return((tmpS==start)&&(count<size));
 
 
 
}


/*
boolean readyTour(int c[],int size)
{
 int start;
 int end;
 int prev;
 int count =0;
 int tour[];
 int i;
 
 tour      = new int[size];
 for(i=0;i<size;i++)
 {
  tour[i] = c[i];
 }
 
 
 start = 0;
 end   =tour[start];
 while(end>=0)
 {
  prev  = start;
  start = end;
  end   = tour[start];
  tour[prev] = -2;
  count++;
  
 }
 
 return(count==size);
 
}*/


boolean readyTour(int c[],int size)
{
 int i;
 boolean found=false;;
 
 
 i=0;
 do
 {
  found = c[i]==-1;
 
  i++;
 }while((!found)&&(i<size));
  
 return(!found);
 
}



private void twoSwap(int c[],long size)
{
  int city1;
  int city2;
  int tmpCity;
  
  city1 = (int)MAFRA_Random.nextUniformLong(0,size);
  city2 = (int)MAFRA_Random.nextUniformLong(0,size);
  
  tmpCity  = c[city1];
  c[city1] = c[city2];
  c[city2] = tmpCity;
  
}





 private int[] edgeEncoding2PermEncoding(int c[],int size)
 {
  int i;
  int c2[];
  int s;
  int e;
  
  c2 = new int[size];
  
  s=0;
  e=c[s];
  c2[0]=s;
  for(i=1;i<size;i++)
  {
   c2[i]=e;
   s=e;
   e=c[s];
  }
  return c2;
 }


 private int[] permEncoding2EdgeEncoding(int aTour[], int size)
 {
  int i;
  int finalTour[];
  
  finalTour = new int[size];
  
  for(i=0;i<(size-1);i++)
  {
   finalTour[aTour[i]]=aTour[i+1];
  }
  finalTour[aTour[(int)size-1]]=aTour[0];
  return finalTour;    
 }



}
