

package MAFRA.CrossOver;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;
import MAFRA.Factory.*;


/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    M3012-1
    */
public class GreedyEdgeEncodingCrossOver extends AbstractTourCrossOver{
  /* This class assumes that the parents are all of the same size */

 private ProblemFactory myProblem;
 
 public GreedyEdgeEncodingCrossOver (ProblemFactory aProblem)
 {
  myProblem = aProblem;
 }
 
 
 
    private boolean feasible(int aTour[],int startC, int endC,int size, boolean alreadyInTour[],int initialCity, int alreadyIn)
      {
       int f;
       boolean result = true;
       int originalCity;
       int count;
       
       result = ((initialCity!=endC)||(alreadyIn>=(size-1))) && (!alreadyInTour[endC]);
       return result;
 }
 
  public Object twoParentsCrossOver(Object parent1,Object parent2)
 
    {
      TourIndividual p1;
      TourIndividual p2;
      TourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      int permTour[];
      boolean alreadyInTour[];
      int size;
      int i,j;
      boolean OK;
      int startCity;
      int endCity1;
      int endCity2;  
      int endCity;
      int initialCity;
      int alreadyIn;
      double distances[][];
      
   
      distances = ((TSPProblemEdgeEncoding)myProblem).getDistances(); 
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (TourIndividual)parent1;
      c1 = (int[])(p1.getChromosome());
      p2 = (TourIndividual)parent2;
      c2 = (int[])(p2.getChromosome());
      size =(int) p1.getSize();
   

      c3= new int[size];
      permTour      = new int[size];
      alreadyInTour = new boolean[size];
      for(i=0;i<size;i++)
	{
	  permTour[i]      = i;
	  c3[i]            = -1;
	  alreadyInTour[i]    = false;
	}
      for (i=0;i<size;i++)
      {
       twoSwap(permTour,size);
      }

      
    
    // System.out.println();
    //System.out.println("New Tour:");
    //  System.out.println("Parent1="+p1.getFitness()+",Parent2="+p2.getFitness());
    
      i=0;
      startCity =permTour[0];  
      initialCity = startCity;
      alreadyIn = 1;
      endCity   = -1;
      while (i<size)
      {
       if ( (int)MAFRA_Random.nextUniformLong(0,2)==1)
       {
        endCity1  = c1[startCity];
        endCity2  = c2[startCity];
       }
       else
       {
        endCity1  = c2[startCity];
       	endCity2  = c1[startCity];
       }
       /* Next city to include in tour */
      if ((distances[startCity][endCity1]<=distances[startCity][endCity2]) && feasible(c3,startCity,endCity1,size,alreadyInTour,initialCity,i))
       {
        endCity = endCity1;
       }
       else
       if (feasible(c3,startCity,endCity2,size,alreadyInTour,initialCity,i))
       {
        endCity = endCity2;
       }
       else
       {
        for(j=0;j<size;j++)
	{
	 if(feasible(c3,startCity,permTour[j],size,alreadyInTour,initialCity,i)) 
	 {
	  endCity = permTour[j];
          j=size+1;
	 }
	}
       }
       //System.out.println("agregando a "+startCity+":"+endCity);
       
       c3[startCity]           = endCity;
       i++;
       startCity               = endCity;
       alreadyInTour[endCity]  = true;
      }
      //System.out.println("****"+i+"****");
      offs = new TourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(c3);
      /*System.out.println();
      for (j=0;j<size;j++)
       System.out.print(j);
      System.out.println();
      for (j=0;j<size;j++)
       System.out.print(c3[j]);
      System.out.println(); */
      return (Object)offs;
      
 }
      
 
 
 
/* Asimetric */ 
 public Object twoParentsCrossOver2(Object parent1,Object parent2)
 
    {
      TourIndividual p1;
      TourIndividual p2;
      TourIndividual offs;
      int c1[];
      int c2[];
      int c3[];
      int permTour[];
      boolean alreadyInTour[];
      long    rndPoint1;
      long    rndPoint2;
      long    tmp;
      int size;
      int i,j;
      int rndCity;
      boolean OK;
      int startCity;
      int endCity1;
      int endCity2;  
        double tot1;
      double tot2;  
      double distances[][];
      
      distances = ((TSPProblemEdgeEncoding)myProblem).getDistances(); 
      //      TSPAWTDisplayVisitor.myTextArea.append("AQUI TOY adentro del crossover !\n");
      p1 = (TourIndividual)parent1;
      c1 = (int[])(p1.getChromosome());
      p2 = (TourIndividual)parent2;
      c2 = (int[])(p2.getChromosome());
      size =(int) p1.getSize();
   

      c3= new int[size];
      alreadyInTour = new boolean[size];
      permTour      = new int[size];
      for(i=0;i<size;i++)
	{
	  alreadyInTour[i] = false;
	  permTour[i]      = i;
	  c3[i]            = -1;
	}
      for (i=0;i<size;i++)
      {
       twoSwap(permTour,size);
      }

      
      i=0;
      startCity =permTour[0];
    System.out.println();
    System.out.println("New Tour:");
    //  System.out.println("Parent1="+p1.getFitness()+",Parent2="+p2.getFitness());
    
      
      while (i<size)
      {
       endCity1  = c1[startCity];
       endCity2  = c2[startCity];
     
//       System.out.print(distances[startCity][endCity1]+"<>"+distances[startCity][endCity2]);
    
       if(( (distances[startCity][endCity1]<=distances[startCity][endCity2]) )  && (!alreadyInTour[endCity1]) && (!prematureCicle(c3,startCity,endCity1,size)))
       {
         c3[startCity]           = endCity1;
	 alreadyInTour[endCity1] = true;
	 i++;
	 startCity               = endCity1;
	 System.out.print("1,");
        }
       else
       if(( (distances[startCity][endCity2]<=distances[startCity][endCity1])  ) &&(!alreadyInTour[endCity2]) && (!prematureCicle(c3,startCity,endCity2,size)))        
       {
        c3[startCity]           = endCity2;
	alreadyInTour[endCity2] = true;
	i++;
	startCity               = endCity2;
	System.out.print("2,");
       }
       else
       {
        OK = false;
	j  = 0;
        do
	{
	 if( (!alreadyInTour[permTour[j]]) && (!prematureCicle(c3,startCity,permTour[j],size)))
	 {
	  c3[startCity]              = permTour[j];
	  alreadyInTour[permTour[j]] = true;
	  OK                         = true;
	  startCity                  = permTour[j];
	 }
	 else j++;
	
	}while(!OK);
	System.out.print("R,");
	i++;
       }
       
      }
      
   
      System.out.println("****"+i+"****");
      offs = new TourIndividual();
      offs.setFitness(Double.POSITIVE_INFINITY);
      offs.setSize(size);
      offs.setChromosome(c3);
   

      return (Object)offs;
      
      }
      
 
private boolean prematureCicle(int aTour[],int startCity,int nextCity,int size)
{
 int originalCity;
 int count;

 return false;
/* count=0;
 originalCity = startCity;
 startCity    = nextCity;
 while((aTour[nextCity]!=-1) && (aTour[nextCity]!=originalCity))
 {
  nextCity =aTour[nextCity];
  count = count +1;
 }
 
 
 return((count<size)&&(aTour[nextCity]==originalCity));
 */
}

 
  

private boolean inSubTour(int city,int cities[],long startPoint, long endPoint)
{
 int i;
 boolean result=false;

 for (i=(int)startPoint;(i<(int)endPoint)&&(!result);i++)
   {
    result = result || (city==cities[i]);
   }
 
 return result;

}




private void twoSwap(int c[],long size)
{
  int city1;
  int city2;
  int tmpCity;
  
  city1 = (int)MAFRA_Random.nextUniformLong(0,size);
  city2 = (int)MAFRA_Random.nextUniformLong(0,size);
  
  tmpCity  = c[city1];
  c[city1] = c[city2];
  c[city2] = tmpCity;
  
}



}
