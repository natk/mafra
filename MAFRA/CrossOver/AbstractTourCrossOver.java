
package MAFRA.CrossOver;

import MAFRA.Util.*;



/** This is a membership class which is superclass of all crossovers under a tour encoding (int[]).
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public abstract class AbstractTourCrossOver extends AbstractCrossOver{



}
