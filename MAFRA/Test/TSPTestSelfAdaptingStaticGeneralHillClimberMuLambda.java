package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;






/** This class is an example of the application to the TSP problem of an evolutionary approach. 
This implements a Memetic Algorithm with:
[1] Tournament Selection for constructing the mating pool.
[2] (Mu+Lambda) selection strategy.
[3] A local search which is an adaptive general hill climber. Each individual
    selects the kind of hill climber he wants to use.
[4] DPX crossover and 2change mutation.
 @version Memetic Algorithms Framework - V 1.2 - August 2000
 @author  Natalio Krasnogor
*/
public class TSPTestSelfAdaptingStaticGeneralHillClimberMuLambda extends Test{

  public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SelfAdaptingStaticSimpleMemeticPlan      myPlan;

     /* CrossOver Stage Variables */
     DPXMatingStrategy                        myMatingStrategy;
     TourEncodingXFactory                  myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                         myMutationStrategy;
     TourEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     SelfAdaptingStaticLocalSearchStrategy                   myLocalSearchStrategy;
     TourEncodingLSFactory                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     TSPProblem                         myProblem;
     SelfAdaptingStaticIndividualTourFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     TSPAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;
     MemeExpressionVisitor                 aMemeExpressionVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog                       aLog;
     MemesEvolutionaryActivityLog                                            memesEvolutionaryActivityLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     long gamma;
     boolean displayAny;
     AbstractLocalSearcherProbConExpTable       myLocalSearchProbConExpTable;
     int  numberOfHelpers=24;
     
     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu     = (Integer.parseInt(argv[3]));
     lambda = (Integer.parseInt(argv[4]));
     gamma = (Integer.parseInt(argv[5]));
     displayAny = (Boolean.valueOf(argv[6])).booleanValue(); //M2612-1

     myProblem       =(TSPProblem)( TSPProblem.readInstance(argv[0]));
     aDisplayVisitor = new TSPAWTDisplayVisitor(200,200,myProblem.getMinX(),myProblem.getMaxX(),myProblem.getMinY(),myProblem.getMaxY(),false,displayAny,myProblem.getPositions());
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new SelfAdaptingStaticIndividualTourFactory(myProblem,numberOfHelpers);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);



     /* Mutation Stage Initalization */
     myMutationFactory   = new TourEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.05));
     /* LEARNING PROBABILITY */
     args.put("ProbabilityPerIndividualLink", new Double(0.125));
     args.put("NumberOfHelpers", new Integer(numberOfHelpers));
     

     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new TourEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new DPXMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.3));
     args.put("lambda",new Long(mu));
     args.put("matingPoolSize", new Long(mu));
     args.put("gamma",new Long(gamma)); /* intermediate population */
     myMatingStrategy.setArguments(args); 

    
  


     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new TourEncodingLSFactory();              
         /* Meme table contruction stage */
	
         myLocalSearchProbConExpTable   = new AbstractLocalSearcherProbConExpTable(numberOfHelpers);
	 /* myLocalSearchFactory.createGeneralHillClimberLocalSearcher(sinOpt,blindFirstBest,numIt,move)*/

   /* TWO-EXCHANGE */
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,0,1,1,myProblem),0.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,0,2,1,myProblem),0.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,0,3,1,myProblem),0.0);	

   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,1,1,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,2,1,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,3,1,myProblem),1.0);	

   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,1,1,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,2,1,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,3,1,myProblem),1.0);	


   /* THREE-EXCHANGE */
 
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,1,2,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,2,2,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,3,2,myProblem),1.0);	

   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,1,2,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,2,2,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,3,2,myProblem),1.0);	


   /* FOUR-EXCHANGE */
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,0,1,3,myProblem),0.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,0,2,3,myProblem),0.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,0,3,3,myProblem),0.0);	

   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,1,3,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,2,3,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,1,3,3,myProblem),1.0);	

   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,1,3,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,2,3,myProblem),1.0);	
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,2,3,3,myProblem),1.0);	


	
	
	
     myLocalSearchStrategy = new SelfAdaptingStaticLocalSearchStrategy( myLocalSearchProbConExpTable);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args); 
     
     

    
     aMemeExpressionVisitor = new MemeExpressionVisitor(myLocalSearchProbConExpTable);





     

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addVisitor("memeExpressionVisitor",aMemeExpressionVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(2010));



     myPlan = new SelfAdaptingStaticSimpleMemeticPlan(myGa);

     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-SAS-GeneralHillClimber-"+argv[1]+"-LOG.dat",1,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
   
     memesEvolutionaryActivityLog = new MemesEvolutionaryActivityLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-SAS-GeneralHillClimber-"+argv[1]+"-MEEA.dat",1," #Gen AvgFit ExVaMeme1 ExVaMeme2 ... ExVaMemeN : EvAcMeme1 EvAcMeme2 ... EvAcMemeN",myLocalSearchProbConExpTable);
     myPlan.setLog("MemesEvolutionaryActivityLog",memesEvolutionaryActivityLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);


   }
}
