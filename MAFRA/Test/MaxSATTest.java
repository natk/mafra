package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;





/** This class is an example of the application to the SAT problem of an evolutionary
approach.

 @version Memetic Algorithms Framework - V 1.0 - September 1999
 @author  Natalio Krasnogor
 */
public class MaxSATTest extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {

     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                    myGa;
     SimpleMemeticPlan                myPlan;

     /* CrossOver Stage Variables */
     MatingStrategy                        myMatingStrategy;
     BitSetEncodingXFactory                myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                      myMutationStrategy;
     BitSetEncodingMFactory                myMutationFactory;


     /* LocalSearch Stage Variables */
     LocalSearchStrategy                   myLocalSearchStrategy;
     BitSetEncodingLSFactory               myLocalSearchFactory;


     /* Selection Stage Variables */
     SelectionStrategy                     mySelectionStrategy;
     MuPlusLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     MaxSATProblem                         myProblem;
     IndividualBitSetFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;
     BitSetIndividual                      anIndividual;
     BitSet                                aChromosome;

     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     DisplayVisitor                        aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;


     /* Gral Initialization */
     MAFRA_Random.initialize(117351317);
     aSortingVisitor = new SortingVisitor();
     aDisplayVisitor = new DisplayVisitor();

     // A collection of SAT instances is available at https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html
     // Passes an instance file location (in cnf format) from the command line.
     // The cnf format is defined here: https://www.cs.ubc.ca/~hoos/SATLIB/Benchmarks/SAT/satformat.ps
     myProblem       =(MaxSATProblem)( MaxSATProblem.readInstance(argv[0]));
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new IndividualBitSetFactory(myProblem);
     myPopulation        = new Population(100,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,100);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);




     /* Mutation Stage Initalization */
     myMutationFactory   = new BitSetEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.5));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new BitSetEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new MatingStrategy(myCrossOverFactory, myMatingSelectionMethod, myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.650));
     args.put("lambda",new Long(100));
     args.put("matingPoolSize", new Long(5));
     myMatingStrategy.setArguments(args);

     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new BitSetEncodingLSFactory();
     myLocalSearchStrategy = new LocalSearchStrategy(myLocalSearchFactory);
     args = new Hashtable();
     args.put("Population",myPopulation);
     args.put("ProbabilityPerIndividual", new Double(0.1));
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args);

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuPlusLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,100,100);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);

     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy);
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy);
     myGa.addLocalSearchStrategy(myLocalSearchStrategy);
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(300));


     myPlan = new SimpleMemeticPlan(myGa);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();





   }
}
