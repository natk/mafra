package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class is an example of the application to the TSP problem of an evolutionary approach.
This implements a Genetic Algorithm with:
[1] Tournament Selection for constructing the mating pool.
[2] (Mu+Lambda) selection strategy.
[3] PMX crossover and 2change mutation.
 @version Memetic Algorithms Framework - V 1.2 - November 1999
 @author  Natalio Krasnogor
*/
public class TSPTestElitistGAMuPlusLambda extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {
     // I added the mu and lambda stuff M2811-4


     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa;
     SimpleMemeticPlan                        myPlan;

     /* CrossOver Stage Variables */
     PMXMatingStrategy                        myMatingStrategy;
     TourEncodingXFactory                  myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     TourMutationStrategy                         myMutationStrategy;
     TourEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     ElitistLocalSearchStrategy                   myLocalSearchStrategy;
     TourEncodingLSFactory                 myLocalSearchFactory;


     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuPlusLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     TSPProblem                         myProblem;
     IndividualTourFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     TSPAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog                       aLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     boolean displayAny;

     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu     = (Integer.parseInt(argv[3]));
     lambda = (Integer.parseInt(argv[4]));

     displayAny = (Boolean.valueOf(argv[5])).booleanValue(); //M2612-1

     // instances of the simmetric travelling salesman problem can be downloaded from here: http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsplib.html
     myProblem       =(TSPProblem)( TSPProblem.readInstance(argv[0]));
     aDisplayVisitor = new TSPAWTDisplayVisitor(200,200,myProblem.getMinX(),myProblem.getMaxX(),myProblem.getMinY(),myProblem.getMaxY(),true,displayAny,myProblem.getPositions());
     aFitnessVisitor = new FitnessVisitor(myProblem);

     /* Populations Initialization*/
     myFactory           = new IndividualTourFactory(myProblem);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aFitnessVisitor);
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aFitnessVisitor);
     myOffsprings.acceptVisitor(aDisplayVisitor);

     /* Mutation Stage Initalization OK */
     myMutationFactory   = new TourEncodingMFactory();
     myMutationStrategy  = new TourMutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(.25));
     myMutationStrategy.setArguments(args);

     /* CrossOver Stage Initialization OK */
     myCrossOverFactory      = new TourEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new PMXMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.75));
     args.put("lambda",new Long(mu));
     args.put("matingPoolSize", new Long(mu));
     myMatingStrategy.setArguments(args);

     /* Local Search Stage Initialization OK */
     myLocalSearchFactory  = new TourEncodingLSFactory();
     myLocalSearchStrategy = new ElitistLocalSearchStrategy(myLocalSearchFactory);
     args = new Hashtable();
     args.put("Population",myPopulation);
     args.put("ProbabilityPerIndividual", new Double(0.25));
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args);

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuPlusLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);

     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy);
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy);
     myGa.addLocalSearchStrategy(myLocalSearchStrategy);
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(1000));


     // This plan tries to maximise fitnesss
     myPlan = new SimpleMemeticPlan(myGa);
     System.out.println("");
     System.out.println("LOG FILE:"+argv[0]+"-Log-"+mu+"+"+lambda+"-Elitist-GA"+argv[1]+".dat");
     System.out.println("");
     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-Log-"+mu+"+"+lambda+"-Elitist-GA"+argv[1]+".dat",1,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);

   }
}
