package MAFRA.Test;




import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;


/** This class is an example of the application to the Counting Ones  problem of an
evolutionary approach.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class OneMaxTest extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {

     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                    myGa;
     SimpleGAPlan               myPlan;

     /* CrossOver Stage Variables */
     MatingStrategy                        myMatingStrategy;
     BitSetEncodingXFactory                myCrossOverFactory;
     TournamentSelectionMethod            myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                      myMutationStrategy;
     BitSetEncodingMFactory                myMutationFactory;

     /* Selection Stage Variables */
     SelectionStrategy                     mySelectionStrategy;
     MuPlusLambdaSelectionStrategyExecutor myMPLSExecutor;

     /* Problem and Individual Variables */
     OneMaxProblem                         myProblem;
     IndividualBitSetFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;
     BitSetIndividual                      anIndividual;
     BitSet                                aChromosome;

     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     DisplayVisitor                        aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;


     /* Gral Initialization */
     MAFRA_Random.initialize(27131411);
     aSortingVisitor = new SortingVisitor();
     aDisplayVisitor = new DisplayVisitor();
     myProblem       = new OneMaxProblem(100);
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new IndividualBitSetFactory(myProblem);
     myPopulation        = new Population(20,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,20);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);

     
     /* Mutation Stage Initalization */
     myMutationFactory   = new BitSetEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.7));
     myMutationStrategy.setArguments(args);
     
     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new BitSetEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new MatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.8));
     args.put("lambda",new Long(20));
     args.put("matingPoolSize", new Long(20));
     myMatingStrategy.setArguments(args);


     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuPlusLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,20,20);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);

     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy);
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy);
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(100));

     myPlan = new SimpleGAPlan(myGa);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();
     System.out.println("We start again:");
     myPlan.setGenerationsNow(3);
     theEvolutionaryPlan.run();




   }
}
