package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;






/** This class is an example of the application to the PF problem of an evolutionary approach. 
This implements a Memetic Algorithm with:
[1] Tournament Selection for constructing the mating pool, size=2.
[2] (Mu,Lambda) selection strategy
[3] One Meme as  Local Search.
[4] 2-Point String Crossover, and one-point mutation in the relatice encoding.
[5] No Heuristic Initialization .
 @version Memetic Algorithms Framework - V 1.2 - december 2000
 @author  Natalio Krasnogor
*/
public class MPFRel extends Test{

  public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SelfAdaptingStaticSimpleMemeticPlan      myPlan;
   
     /* CrossOver Stage Variables */
     PFMatingStrategy                        myMatingStrategy;
     StringEncodingXFactory                  myCrossOverFactory;
     SelectionMethod                         myMatingSelectionMethod;
   

     /* Mutation Stage Variables */
     PFMutationStrategyRel                         myMutationStrategy;
     StringEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     PFSelfAdaptingStaticLocalSearchStrategyRel                   myLocalSearchStrategy;
     PFStringEncodingLSFactory                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     PFProblemRel                         myProblem;
     SelfAdaptingStaticIndividualPFStringFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                       aFitnessVisitor;
     PFAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                       aSortingVisitor;
     MemeExpressionVisitor                aMemeExpressionVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog               aLog;
     MemesEvolutionaryActivityLog         memesEvolutionaryActivityLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     long gamma;
     boolean displayAny;
     boolean displayAll;
     AbstractLocalSearcherProbConExpTable       myLocalSearchProbConExpTable;
    int  numberOfHelpers=1; /*!"!"*/

     
     String move;        /* this will hold a rule based move */
     int blindFirstBest; /* this is 0 if the search is blind, 1 if it is first improvement and 2 if it is best improvement*/
     int numIt;          /* number of iterations */
     boolean hcBhc;      /* hcBhc=false then hillclimber, hcBhc=true then boltzmann hill climber */
     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu         = (Integer.parseInt(argv[3]));
     lambda     = (Integer.parseInt(argv[4]));
     gamma      = (Integer.parseInt(argv[5]));
     displayAny = (Boolean.valueOf(argv[6])).booleanValue();
     displayAll = (Boolean.valueOf(argv[7])).booleanValue();
     move       = argv[8];                            /* a move of the form "string-->string" */
     blindFirstBest  = (Integer.parseInt(argv[9]));   /* values: 0=blind      ,1= first     ,2=best       */
     numIt      = (Integer.parseInt(argv[10]));
     hcBhc      = (Boolean.valueOf(argv[11])).booleanValue();

     myProblem       =(PFProblemRel)(new  PFProblemRel(argv[0]));
     aDisplayVisitor = new PFAWTDisplayVisitor(displayAll,displayAny,argv[0]);
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new SelfAdaptingStaticIndividualPFStringFactory(myProblem,numberOfHelpers);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);



     /* Mutation Stage Initalization */
     myMutationFactory   = new StringEncodingMFactory(true);
     myMutationFactory.setAlphabetSize(5);
     myMutationFactory.setSimbols(PFProblemRel.TRI2DREALPHABET);
     myMutationStrategy  = new PFMutationStrategyRel(myMutationFactory,myProblem);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(argv[12]));
     /* LEARNING PROBABILITY */
     args.put("ProbabilityPerIndividualLink", new Double(1.0));
     args.put("NumberOfHelpers", new Integer(numberOfHelpers));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new StringEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod(); 
  //   myMatingSelectionMethod = new SUSSelectionMethod(); 
     ((TournamentSelectionMethod)myMatingSelectionMethod).setSize(2);
     myMatingStrategy         = new PFMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(argv[13]));
  //   args.put("lambda",new Long(mu)); M15-02-2001
  //   args.put("matingPoolSize", new Long(mu)); M15-02-2001
      args.put("lambda",new Long(lambda));
      args.put("matingPoolSize", new Long(lambda));
      args.put("gamma",new Long(gamma)); /* intermediate population */
      myMatingStrategy.setArguments(args); 

    
  


     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new PFStringEncodingLSFactory();              
     /* Meme table contruction stage */
     myLocalSearchProbConExpTable   = new AbstractLocalSearcherProbConExpTable(numberOfHelpers);
     /* ############################################################################################################# 
        ############################################################################################################# 
        P      U       T              A            M       E        M        E            H       E       R    E
	############################################################################################################## 
        ##############################################################################################################
     */
     

     /* Meme */
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,move,myProblem),1.0);	


  


       
     myLocalSearchStrategy = new PFSelfAdaptingStaticLocalSearchStrategyRel( myLocalSearchProbConExpTable);
     args = new Hashtable();
     args.put("ProblemFactory",myProblem);
     args.put("Elitism",Boolean.valueOf(argv[15]));
     args.put("HistoryLength",new Integer(10));
     // uncomment the line below for a (mu+lambda) strategy  
     //args.put("Population",myPopulation);  
     // uncomment the line below for a (mu,lambda) strategy  
      args.put("Population",myOffsprings);
    
     myLocalSearchStrategy.setArguments(args); 
     
     

    
     aMemeExpressionVisitor = new MemeExpressionVisitor(myLocalSearchProbConExpTable);





     

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addVisitor("memeExpressionVisitor",aMemeExpressionVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(argv[14]));
     



    myPlan = new SelfAdaptingStaticSimpleMemeticPlan(myGa);
   

     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-Meme-"+numIt+"-"+hcBhc+"-"+move.substring(0,move.indexOf("-->"))+"_"+move.substring(move.indexOf("-->")+3,move.length()-1)+"-"+argv[1]+"-LOG.dat",1,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
   
     memesEvolutionaryActivityLog = new MemesEvolutionaryActivityLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-Meme-"+numIt+"-"+hcBhc+"-"+move.substring(0,move.indexOf("-->"))+"_"+move.substring(move.indexOf("-->")+3,move.length()-1)+"-"+argv[1]+"-MEEA.dat",1," #Gen AvgFit ExVaMeme1 ExVaMeme2 ... ExVaMemeN : EvAcMeme1 EvAcMeme2 ... EvAcMemeN",myLocalSearchProbConExpTable);
     myPlan.setLog("MemesEvolutionaryActivityLog",memesEvolutionaryActivityLog);
     myPlan.resetElitism();
    // myPlan.setElitePopulationSize(1);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);


   }
}



/*
###################################################################################################################
###################################################################################################################
###################################################################################################################
              M             E            M           E             S
###################################################################################################################
###################################################################################################################
###################################################################################################################


   
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sS"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->Ss"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->Nn"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sS"),myProblem),1.0);	     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->Ns"),myProblem),1.0);	     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sN"),myProblem),1.0);	     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nS"),myProblem),1.0);	     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->Sn"),myProblem),1.0);	          
     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WN"),myProblem),1.0);	          
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->NW"),myProblem),1.0);	          
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nE"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->En"),myProblem),1.0);	               
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->Ws"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sW"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ES"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SE"),myProblem),1.0);
     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SnN"),myProblem),1.0);	          
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SsN"),myProblem),1.0);	          
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sNn"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sSn"),myProblem),1.0);	               
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nEs"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nWs"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->NES"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->NWS"),myProblem),1.0);
     
 
     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEESE"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sEsWs"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SSSWS"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sSsWs"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nSnES"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnEsE"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnSEE"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESSWs"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEsWW"),myProblem),1.0); 
     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->|"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->*"),myProblem),1.0);	

     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->!1"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->!2"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->!3"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->!4"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->!5"),myProblem),1.0);	

     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->%NS"),myProblem),1.0);	     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->%ns"),myProblem),1.0);	     
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->%WE"),myProblem),1.0);	
  
   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("_-->_"),myProblem),1.0);	
   



 myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EE-->Es"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("Es-->EE"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EEss-->EsEs"),myProblem),1.0);	
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EsEs-->EEss"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EnEsE-->EsEnE"),myProblem),1.0);
     myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EsEnE-->EnEsE"),myProblem),1.0); 
     
     
*/


  
   
  
 /* Reflects Rules */ 
//.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,hcBhc,blindFirstBest,numIt,new String("*-->%NS"),myProblem),1.0);	     
//.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,hcBhc,blindFirstBest,numIt,new String("*-->%ns"),myProblem),1.0);	     
//.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,hcBhc,blindFirstBest,numIt,new String("*-->%WE"),myProblem),1.0);	

    /* Estirar */
    //.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->NNNNN"),myProblem),1.0);
    //.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nnnnn"),myProblem),1.0);
    //.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SSSSS"),myProblem),1.0);
    //.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sssss"),myProblem),1.0);
    //.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEEEE"),myProblem),1.0);
    //.  myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWWWW"),myProblem),1.0);


  /* A */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("NNN-->NEE"),myProblem),0.2);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("NEE-->NNN"),myProblem),0.2);
    /* B */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EEE-->ESn"),myProblem),0.2);
     /* C */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("ESn-->EEE"),myProblem),0.2);
     /* D */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("ESEEE-->EEESE"),myProblem),0.2);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EEESE-->ESEEE"),myProblem),0.2);
     /* E */ 
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("nWnEn-->nEnWn"),myProblem),0.2);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("nEnWn-->nWnEn"),myProblem),0.2);
     /* F */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("sWsSs-->sSsWs"),myProblem),0.2);
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("sSsWs-->sWsSs"),myProblem),0.2);
     /* G */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("SssES-->SSSWS"),myProblem),0.2);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("SSSWS-->SssES"),myProblem),0.2);
     /* H */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("nnESS-->nSnES"),myProblem),0.2);
     /* I */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("ENESE-->EnEsE"),myProblem),0.2);	
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EnEsE-->ENESE"),myProblem),0.2);	
     /* J */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EEnSE-->EnSEE"),myProblem),0.2);	
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EnSEE-->EEnSE"),myProblem),0.2);	
    /* K */
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EEsss-->ESSWs"),myProblem),0.2);	
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("ESSWs-->EEsss"),myProblem),0.2);
   /* L */
       //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EnSSE-->EEsEE"),myProblem),0.2);
   /* M */
       //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("EEsEE-->EnSSE"),myProblem),0.2);  
       
       
       

    /* A' */
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->NEE"),myProblem),1.0);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->NNN"),myProblem),1.0);
     /* B' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESn"),myProblem),1.0);
     /* C' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEE"),myProblem),1.0);
     /* D' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEESE"),myProblem),1.0);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESEEE"),myProblem),1.0);
     /* E' */ 
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nEnWn"),myProblem),1.0);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nWnEn"),myProblem),1.0);
     /* F' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sSsWs"),myProblem),1.0);
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->sWsSs"),myProblem),1.0);
     /* G' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SSSWS"),myProblem),1.0);
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->SssES"),myProblem),1.0);
     /* H' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->nSnES"),myProblem),1.0);
     /* I' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnEsE"),myProblem),1.0);	
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ENESE"),myProblem),1.0);	
     /* J' */
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnSEE"),myProblem),1.0);	
         //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEnSE"),myProblem),1.0);	
    /* K' */
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESSWs"),myProblem),1.0);	
        //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEsss"),myProblem),1.0);
   /* L' */
       //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEsEE"),myProblem),1.0);
   /* M' */
       //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnSSE"),myProblem),1.0);  
       
       
       
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEEEEnWWWWW"),myProblem),1.0);  
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEEEENWWWWW"),myProblem),1.0);  
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEEEEsWWWWW"),myProblem),1.0);
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEEEESWWWWW"),myProblem),1.0); 
           
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWWWWnEEEEE"),myProblem),1.0);  
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWWWWNEEEEE"),myProblem),1.0);  
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWWWWsEEEEE"),myProblem),1.0);
    //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWWWWSEEEEE"),myProblem),1.0);        
    
    
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEnNWW"),myProblem),1.0);        
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EENnWW"),myProblem),1.0);
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EEsSWW"),myProblem),1.0);        
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EESsWW"),myProblem),1.0);     
   

 //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWnNEE"),myProblem),1.0);        
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWNnEE"),myProblem),1.0);
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWsSEE"),myProblem),1.0);        
   //. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WWSsEE"),myProblem),1.0);   
   


//. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnnSSE"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnSnSE"),myProblem),1.0);
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESSnnE"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESnSnE"),myProblem),1.0);    
   
//. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnnSSE"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->EnSnSE"),myProblem),1.0);
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESSnnE"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->ESnSnE"),myProblem),1.0); 
   

//. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WNNssW"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WNsNsW"),myProblem),1.0);
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WssNNW"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WsNsNw"),myProblem),1.0);    
   
//. myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WNNssW"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WNsNsW"),myProblem),1.0);
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WssNNW"),myProblem),1.0);        
//.   myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcherRel(false,hcBhc,blindFirstBest,numIt,new String("*-->WsNsNw"),myProblem),1.0);   
