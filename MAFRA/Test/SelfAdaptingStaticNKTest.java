package MAFRA.Test;




import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;


/** This class is an example of the application to the Counting Ones  problem of an
evolutionary approach with self adapting mutation and mutation probabilities. 

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class SelfAdaptingStaticNKTest extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                    myGa; 
     SelfAdaptingStaticSimpleGAPlan               myPlan;

     /* CrossOver Stage Variables */
     SelfAdaptingStaticMatingStrategy                        myMatingStrategy;
     BitSetEncodingXFactory                myCrossOverFactory;
     TournamentSelectionMethod            myMatingSelectionMethod;

     /* Mutation Stage Variables */
     SelfAdaptingStaticMutationStrategy                      myMutationStrategy;
     BitSetEncodingMFactory                myMutationFactory;

     /* Selection Stage Variables */
     SelectionStrategy                     mySelectionStrategy;
     MuLambdaSelectionStrategyExecutor myMPLSExecutor;

     /* Problem and Individual Variables */
     NKProblem                         myProblem;
     SelfAdaptingStaticIndividualBitSetFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;
     SelfAdaptingStaticBitSetIndividual                      anIndividual;
     BitSet                                aChromosome;                               

     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     DisplayVisitor                        aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;
     MemeExpressionVisitor                 aMemeExpressionVisitor;



     /* Log Variables */
     GenFitEvsFitMaxMutAvgMutMaxCrossAvgCrossAvgFitLog                       aLog;
     MemesEvolutionaryActivityLog                                            memesEvolutionaryActivityLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu,lambda,numGen;
     boolean displayAny;
     AbstractMutCrossProbConExpTable myMutCrossProbConExpTable;
    


     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(27131411);
   
     mu     = (Integer.parseInt(argv[2]));
     lambda = (Integer.parseInt(argv[3]));
     numGen = (Integer.parseInt(argv[4]));
     displayAny = (Boolean.valueOf(argv[5])).booleanValue(); 
     aSortingVisitor = new SortingVisitor();
     aDisplayVisitor = new DisplayVisitor();
     /* Random number seed for the landscape */
     MAFRA_Random.initialize( (Integer.parseInt(argv[7])) );
     myProblem       = new NKProblem((Integer.parseInt(argv[0])),(Integer.parseInt(argv[1])),false);
     /* Random number seed for the experiment */
     MAFRA_Random.initialize( (Integer.parseInt(argv[8])) );
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new SelfAdaptingStaticIndividualBitSetFactory(myProblem,29);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,mu);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);


     /* Meme table contruction stage */
     myMutationFactory   = new BitSetEncodingMFactory();
     myCrossOverFactory      = new BitSetEncodingXFactory();
     myMutCrossProbConExpTable   = new AbstractMutCrossProbConExpTable(29);

 
       
  //  myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0000),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
  //  myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0001),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
  //  myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0002),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
  //  myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0003),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
 //   myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0004),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
   
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0005),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0010),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0015),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0020),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0025),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0030),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0035),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0040),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0045),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
    myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.0050),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.010),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.015),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.020),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.025),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.030),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.035),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.040),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.045),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.050),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.055),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.060),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.065),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.070),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.075),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.080),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.085),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.090),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.095),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
     myMutCrossProbConExpTable.addMeme(myMutationFactory.createOnePointMutation(0.10),1.0,myCrossOverFactory.createSelfAdaptingStaticOnePointCrossOver(),0.0);
   




     aMemeExpressionVisitor = new MemeExpressionVisitor(myMutCrossProbConExpTable);

     /* Mutation Stage Initalization */
     myMutationStrategy  = new SelfAdaptingStaticMutationStrategy(myMutCrossProbConExpTable);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndLink",new Double(argv[9]));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new SelfAdaptingStaticMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem,myMutCrossProbConExpTable);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.0));
     args.put("lambda",new Long(lambda));
     args.put("matingPoolSize", new Long(mu));
     myMatingStrategy.setArguments(args);


     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy);
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy);
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
//     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addVisitor("memeExpressionVisitor",aMemeExpressionVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(numGen));



     myPlan = new SelfAdaptingStaticSimpleGAPlan(myGa);
     aLog = new   GenFitEvsFitMaxMutAvgMutMaxCrossAvgCrossAvgFitLog(myPlan,argv[0]+"-"+argv[1]+"-Log-"+argv[6]+"-LR-"+argv[9]+".dat",10,myProblem," #Gen #FitEvs Fit MaxMut AvgMut MaxCross AvgCross AvgFit",myMutCrossProbConExpTable);
     myPlan.setLog("GenFitEvsFitMaxMutAvgMutMaxCrossAvgCrossAvgFitLog",aLog);
     
  memesEvolutionaryActivityLog = new MemesEvolutionaryActivityLog(myPlan,argv[0]+"-"+argv[1]+"-MemesExpressionLog-"+argv[6]+"-LR-"+argv[9]+".dat",10," #Gen AvgFit EvAcMeme1 EvAcMeme2 ... EvAcMemeN",myMutCrossProbConExpTable);
  myPlan.setLog("MemesEvolutionaryActivityLog",memesEvolutionaryActivityLog);

	  
	
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();
      



    


   }
}
