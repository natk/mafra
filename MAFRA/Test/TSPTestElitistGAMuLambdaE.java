package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;





/** This class is an example of the application to the TSP problem of an evolutionary
approach. 
[1] Tournament Selection for constructing the mating pool.
[2] Random initial population
[3] GEECrossOver
[4] (Mu,Lambda) selection strategy
[5] PX=0.8, PM=0.05
[6] edge encoding
 @version Memetic Algorithms Framework - V 1.0 - September 1999
 @author  Natalio Krasnogor
 */
public class TSPTestElitistGAMuLambdaE extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {
  // I added the mu and lambda reading from argv (M212-7)
 //  I added the displayAny reading from argv (M2612-1)
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SimpleMemeticPlan                             myPlan;

     /* CrossOver Stage Variables */
     GEEMatingStrategy                        myMatingStrategy;
     TourEncodingXFactory                  myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                         myMutationStrategy;
     TourEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     ElitistLocalSearchStrategy                   myLocalSearchStrategy;
     TourEncodingLSFactory                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     TSPProblemEdgeEncoding        myProblem;
     IndividualTourFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     TSPAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog                       aLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     boolean displayAny ; //M2612-1
     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu     = (Integer.parseInt(argv[3]));
     lambda = (Integer.parseInt(argv[4]));
     displayAny = (Boolean.valueOf(argv[5])).booleanValue(); //M2612-1


     myProblem       =(TSPProblemEdgeEncoding)( TSPProblemEdgeEncoding.readInstance(argv[0]));
     aDisplayVisitor = new TSPAWTDisplayVisitor(200,200,myProblem.getMinX(),myProblem.getMaxX(),myProblem.getMinY(),myProblem.getMaxY(),false,displayAny,myProblem.getPositions());
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new IndividualTourFactory(myProblem);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);




     /* Mutation Stage Initalization */
     myMutationFactory   = new TourEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.05));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new TourEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new GEEMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.8));
     args.put("lambda",new Long(mu));
     args.put("matingPoolSize", new Long(mu));
     myMatingStrategy.setArguments(args); 

     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new TourEncodingLSFactory(true);              
     myLocalSearchStrategy = new ElitistLocalSearchStrategy(myLocalSearchFactory);
     args = new Hashtable();
     args.put("Population",myPopulation);
     args.put("ProbabilityPerIndividual", new Double(0.0));
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args); 

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     /* We run 3 times the number of Generations of the Memetic Versions */
     myGa.addParameter("maxGenerationsNumber",new Long(6000));



     myPlan = new SimpleMemeticPlan(myGa);

     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-Log-"+mu+"-"+lambda+"-Elitist-GA-Edge"+argv[1]+".dat",1,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);
    


   }
}
