package MAFRA.Test;




import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;



/** This class is an example of the application to the SAT problem of a co-evolutionary
approach. 

 @version Memetic Algorithms Framework - V 1.0 - September 1999
 @author  Natalio Krasnogor
 */

public class CoEvolvingTest extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {


     
     /* General Evolutionary Plan Variables */
     CoEvolutionaryPlan myCoEvolutionaryPlan;
     RestartCoEvolvingPopStrategy myRestartCoEvolvingPopStrategy;

     EvolutionaryPlan                         theEvolutionaryPlan1;
     EvolutionaryPlan                         theEvolutionaryPlan2;

     GA                                    myGa1; 
     GA                                    myGa2;
     SimpleGAPlan               myPlan1;
     SimpleGAPlan               myPlan2;

     /* CrossOver Stage Variables */
     MatingStrategy                        myMatingStrategy1;
     MatingStrategy                        myMatingStrategy2;
     BitSetEncodingXFactory                myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod1;
     TournamentSelectionMethod             myMatingSelectionMethod2;

     /* Mutation Stage Variables */
     MutationStrategy                      myMutationStrategy1;
     MutationStrategy                      myMutationStrategy2;
     BitSetEncodingMFactory                myMutationFactory;

     /* Selection Stage Variables */
     SelectionStrategy                     mySelectionStrategy1;
     SelectionStrategy                     mySelectionStrategy2;
     MuLambdaSelectionStrategyExecutor myMPLSExecutor1;
     MuLambdaSelectionStrategyExecutor myMPLSExecutor2;

     /* Problem and Individual Variables */
     MaxSATProblem                         myProblem;
     IndividualBitSetFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation1;
     Population                            myOffsprings1;
     Population                            myPopulation2;
     Population                            myOffsprings2;
     BitSetIndividual                      anIndividual;
     BitSet                                aChromosome;                               

     /* Visitors Variables */
     CoEvolutionaryFitnessVisitor          aFitnessVisitor1;
     CoEvolutionaryFitnessVisitor          aFitnessVisitor2;
     MaxSATAWTDisplayVisitor                        aDisplayVisitor1;
     MaxSATAWTDisplayVisitor                        aDisplayVisitor2;
     SortingVisitor                        aSortingVisitor;

     /* Gral Variables */
     Hashtable                             args;
     long                                  i,j,gene;

     
     /* Gral Initialization */
     //  MAFRA_Random.initialize();
     MAFRA_Random.initialize(27131411);
     aSortingVisitor = new SortingVisitor();
     myProblem       =(MaxSATProblem)( MaxSATProblem.readInstance("/home/staffg/natk/PROJECTS/INSTANCES/SAT/UniformRandom-3-SAT/uf50-01.cnf"));
     aDisplayVisitor1 = new MaxSATAWTDisplayVisitor(myProblem);
     aDisplayVisitor2 = new MaxSATAWTDisplayVisitor(myProblem);
     aFitnessVisitor1 = new CoEvolutionaryFitnessVisitor(myProblem);
     aFitnessVisitor2 = new CoEvolutionaryFitnessVisitor(myProblem);



      /* Population1 Initialization*/
     myFactory           = new IndividualBitSetFactory(myProblem);
     myPopulation1        = new Population(10,myFactory);
     myPopulation1.setName("Parents");
     myPopulation1.acceptVisitor(aDisplayVisitor1);
     myOffsprings1 = new Population();
     myPopulation1.copyTo(myOffsprings1,10);
     myOffsprings1.setName("Offsprings");
     myOffsprings1.acceptVisitor(aDisplayVisitor1);
      /* Population2 Initialization*/
     myPopulation2        = new Population(10,myFactory);
     myPopulation2.setName("Parents");
     myPopulation2.acceptVisitor(aDisplayVisitor2);
     myOffsprings2 = new Population();
     myPopulation2.copyTo(myOffsprings2,10);
     myOffsprings2.setName("Offsprings");
     myOffsprings2.acceptVisitor(aDisplayVisitor2);




     /* Mutation Stage1 Initalization */
     myMutationFactory   = new BitSetEncodingMFactory();
     myMutationStrategy1  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings1);
     args.put("ProbabilityPerIndividual", new Double(0.3));
     myMutationStrategy1.setArguments(args);
     /* Mutation Stage2 Initalization */
     myMutationStrategy2  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings2);
     args.put("ProbabilityPerIndividual", new Double(0.3));
     myMutationStrategy2.setArguments(args);




     /* CrossOver Stage1 Initialization */
     myCrossOverFactory      = new BitSetEncodingXFactory();
     myMatingSelectionMethod1 = new TournamentSelectionMethod();
     myMatingStrategy1         = new MatingStrategy(myCrossOverFactory,myMatingSelectionMethod1);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings1);
     args.put("parentsPopulation",myPopulation1);
     args.put("matingProbability",new Double(0.8));
     args.put("lambda",new Long(10));
     args.put("matingPoolSize", new Long(10));
     myMatingStrategy1.setArguments(args);
     /* CrossOver Stage2 Initialization */
     myMatingSelectionMethod2 = new TournamentSelectionMethod();
     myMatingStrategy2         = new MatingStrategy(myCrossOverFactory,myMatingSelectionMethod2);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings2);
     args.put("parentsPopulation",myPopulation2);
     args.put("matingProbability",new Double(0.8));
     args.put("lambda",new Long(10));
     args.put("matingPoolSize", new Long(10));
     myMatingStrategy2.setArguments(args);





     /* Selection Stage 1 Initialization */
     myMPLSExecutor1      = new MuLambdaSelectionStrategyExecutor(myPopulation1,myOffsprings1,10,10);
     mySelectionStrategy1 = new SelectionStrategy(myMPLSExecutor1);
     /* Selection Stage 2 Initialization */
     myMPLSExecutor2      = new MuLambdaSelectionStrategyExecutor(myPopulation2,myOffsprings2,10,10);
     mySelectionStrategy2 = new SelectionStrategy(myMPLSExecutor2);




	
     /* GA 1 setting */
     myGa1 = new GA();
     myGa1.addMatingStrategy(myMatingStrategy1);
     myGa1.addMutationStrategy(myMutationStrategy1);
     myGa1.addSelectionStrategy(mySelectionStrategy1);
     myGa1.addVisitor("sortingVisitor",aSortingVisitor);
     myGa1.addVisitor("displayVisitor",aDisplayVisitor1);
     myGa1.addVisitor("fitnessVisitor",aFitnessVisitor1);
     myGa1.addPopulation("parentsPopulation",myPopulation1);
     myGa1.addPopulation("offspringsPopulation",myOffsprings1);
     myGa1.addParameter("maxGenerationsNumber",new Long(1));
     /* GA 2 setting */
     myGa2 = new GA();
     myGa2.addMatingStrategy(myMatingStrategy2);
     myGa2.addMutationStrategy(myMutationStrategy2);
     myGa2.addSelectionStrategy(mySelectionStrategy2);
     myGa2.addVisitor("sortingVisitor",aSortingVisitor);
     myGa2.addVisitor("displayVisitor",aDisplayVisitor2);
     myGa2.addVisitor("fitnessVisitor",aFitnessVisitor2);
     myGa2.addPopulation("parentsPopulation",myPopulation2);
     myGa2.addPopulation("offspringsPopulation",myOffsprings2);
     myGa2.addParameter("maxGenerationsNumber",new Long(1));


     /* Plan 1 setting */
     myPlan1 = new SimpleGAPlan(myGa1);
     myPlan1.keepPrevious();
     theEvolutionaryPlan1 = new EvolutionaryPlan(myGa1,myProblem,myPlan1,null,null);
     /* Plan 2 setting */
     myPlan2 = new SimpleGAPlan(myGa2);
     myPlan2.keepPrevious();
     theEvolutionaryPlan2 = new EvolutionaryPlan(myGa2,myProblem,myPlan2,null,null);
     aFitnessVisitor1.setCoEvolvedPlan(myPlan2);
     aFitnessVisitor2.setCoEvolvedPlan(myPlan1);


     myCoEvolutionaryPlan = new CoEvolutionaryPlan();
     myRestartCoEvolvingPopStrategy = new RestartCoEvolvingPopStrategy();
     myCoEvolutionaryPlan.addRestartCoEvolvingPopStrategy(myRestartCoEvolvingPopStrategy);
     myCoEvolutionaryPlan.addEvolutionaryPlan(theEvolutionaryPlan1);
     myCoEvolutionaryPlan.addEvolutionaryPlan(theEvolutionaryPlan2);
     myCoEvolutionaryPlan.addParameter("maxGenerationsNumber",new Long(250));
     myCoEvolutionaryPlan.run();

    aDisplayVisitor1.close();
    aDisplayVisitor2.close();
    System.exit(0);

   }
}
