package MAFRA.Test;



import DataStructures.*;
import java.io.IOException;
import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;



/** This class is an example of the application to the GCP (Graph Coloring Problem) of an
evolutionary approach.
 @author  Jorge Habib Namour
 @mail jnamour@herrera.unt.edu.ar
*/
public class GCPTest extends Test{

    public static Graph aGraph;
    public static int amountOfColors;

 public static void  main(String []argv) throws CloneNotSupportedException
   {

     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                    myGa;
     SimpleGAPlan               myPlan;

     /* CrossOver Stage Variables */
     MatingStrategy                        myMatingStrategy;
     ColorsSetEncodingXFactory                myCrossOverFactory;
     TournamentSelectionMethod            myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                      myMutationStrategy;
     ColorsSetEncodingMFactory                myMutationFactory;

     /* Selection Stage Variables */
     SelectionStrategy                     mySelectionStrategy;
     MuPlusLambdaSelectionStrategyExecutor myMPLSExecutor;

     /* Problem and Individual Variables */
     GCPProblem                         myProblem;
     IndividualColorsFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;
     BitSetIndividual                      anIndividual;
     BitSet                                aChromosome;

     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     DisplayVisitor                        aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     String filename, colorsStr;
     int colors;


     /* Gral Initialization */
     filename = argv[0];
     colorsStr = argv[1];
     amountOfColors = (Integer.parseInt(colorsStr));
     //colors = (Integer.parseInt(colorsStr));
     GCPTest.aGraph = null;     
     try {
       GCPTest.aGraph = new Graph(filename);
     } catch(IOException ex) {
       System.out.println("Cannot read graph file: " + filename);
       System.exit(1);
     } catch(IllegalArgumentException ex) {
       System.out.println("Invalid graph file: " + filename);
       System.exit(1);
     }

     MAFRA_Random.initialize(27131411);
     aSortingVisitor = new SortingVisitor();
     aDisplayVisitor = new DisplayVisitor();
     myProblem       = new GCPProblem(GCPTest.aGraph.V(), colorsStr);
     aFitnessVisitor = new FitnessVisitor(myProblem);

     /* Populations Initialization*/
     myFactory           = new IndividualColorsFactory(myProblem);
     myPopulation        = new Population(20,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,20);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);
     /* Mutation Stage Initalization */
     myMutationFactory   = new ColorsSetEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.7));
     myMutationStrategy.setArguments(args);
     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new ColorsSetEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new MatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.8));
     args.put("lambda",new Long(20));
     args.put("matingPoolSize", new Long(20));
     myMatingStrategy.setArguments(args);
     
     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuPlusLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,20,20);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
     
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy);
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy);
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(100));
    
     myPlan = new SimpleGAPlan(myGa);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();
     System.out.println("-----------------");
     System.out.println("-----------------");
     System.out.println(">> Solution not found!");
     
   }
}
