

package MAFRA.Factory;
import MAFRA.CrossOver.*;

/** CrossoverFactory is an abstract class meant to be the superclass of concrete crossover factories.
    A CrossoverOperator instance will receive a CrossoverFactory as an argument for its
    constructor. At runtime the _actual_ parameter will be any of CrossoverFactory subclasses
    which are not abstract and hence implements a set of different crossovers for different
    encodings. The AbstractCrossOver class is used just as a memebership class (superclass) of
    the objects (concrete crossovers for a concrete encoding) returned by createXXXX operations.
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public abstract class CrossOverFactory extends Factory{

 public  abstract AbstractCrossOver createOnePointCrossOver();

 public  abstract AbstractCrossOver createTwoPointCrossOver();

 public  abstract AbstractCrossOver createThreePointCrossOver();

 public  abstract AbstractCrossOver createFourPointCrossOver();

 public  abstract AbstractCrossOver createUniformCrossOver();

 public  abstract AbstractCrossOver createBehaviourCrossOver();

}


