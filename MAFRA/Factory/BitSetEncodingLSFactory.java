

package MAFRA.Factory;

import MAFRA.LocalSearcher.*;
/** @version Memetic Algorithms Framework - V 1.0 - September 1999
    @author  Natalio Krasnogor
    */
public class BitSetEncodingLSFactory extends LocalSearchFactory{


 public  AbstractLocalSearcher createBoltzmannLocalSearcher()
   {
    BitSetBoltzmannLocalSearcher ls;
    ls = new BitSetBoltzmannLocalSearcher();
    return (ls);
   };


  public AbstractLocalSearcher createAnnealingLocalSearcher()
    {
	/* not implemented */
	return (null);
    };

}
