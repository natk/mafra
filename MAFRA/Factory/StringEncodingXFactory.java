

package MAFRA.Factory;

import MAFRA.CrossOver.*;


/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public class StringEncodingXFactory extends CrossOverFactory{


 public  AbstractCrossOver createOnePointCrossOver()
   {
   /* StringOnePointCrossOver oPX;
    oPX = new StringOnePointCrossOver();
    return (oPX);*/
    return null;
   };

 public  AbstractCrossOver createTwoPointCrossOver()
   {
    StringTwoPointCrossOver tPX;
    tPX = new StringTwoPointCrossOver();
    return(tPX);
   };

 public  AbstractCrossOver createThreePointCrossOver()
   {
/*    StringThreePointCrossover thPX;
    thPX = new StrinThreePointCrossOver();
    return(thPX);*/
    return null;
   };

 public  AbstractCrossOver createFourPointCrossOver()
   {
/*    StringFourPointCrossOver fPX;
    fPX = new StringFourPointCrossOver();
    return(fPX);*/
    return null;
   };

 public  AbstractCrossOver createUniformCrossOver()
   {
/*    StringUniformCrossOver uX;
    uX = new StringUniformCrossOver();
    return(uX);*/
    return null;
   };

 public AbstractCrossOver createBehaviourCrossOver()
 {
  return null;
 }

}
