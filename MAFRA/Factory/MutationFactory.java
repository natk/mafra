

package MAFRA.Factory;

import MAFRA.Mutation.*;

/** MutationFactory is an abstract class meant to be the superclass of concrete mutation factories.
    A MutationOperator instance will receive a MutationFactory as an argument for its
    constructor. At runtime the _actual_ parameter will be any of MutationFactory subclasses
    which are not abstract and hence implements a set of different mutations for different
    encodings. The AbstractMutation class is used just as a memebership class (superclass) of
    the objects (concrete mutations for a concrete encoding) returned by createXXXX operations.
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public abstract class MutationFactory extends Factory{

 public  abstract AbstractMutation createOnePointMutation();
   
 public  abstract AbstractMutation createTwoPointMutation();

 public  abstract AbstractMutation createThreePointMutation();

 public  abstract AbstractMutation createFourPointMutation();




}


