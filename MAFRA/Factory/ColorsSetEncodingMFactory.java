package MAFRA.Factory;
import MAFRA.Mutation.*;


/**This class work with mutations functions.
I'm using only the one point mutation in the color set.
 @author  Jorge Habib Namour
 */
public class ColorsSetEncodingMFactory extends MutationFactory{

    //This function select the point of the mutation of an individual
 public  AbstractMutation createOnePointMutation()
   {
    ColorsSetOnePointMutation oPM;
    oPM = new ColorsSetOnePointMutation();
    return (oPM);
   };

   public  AbstractMutation createTwoPointMutation()
   {
    BitSetTwoPointMutation tPM;
    tPM = new BitSetTwoPointMutation();
    return(tPM);
   };

 public  AbstractMutation createThreePointMutation()
   {
    BitSetThreePointMutation thPM;
    thPM = new BitSetThreePointMutation();
    return(thPM);
   };

 public  AbstractMutation createFourPointMutation()
   {
    BitSetFourPointMutation fPM;
    fPM = new BitSetFourPointMutation();
    return(fPM);
   };


}
