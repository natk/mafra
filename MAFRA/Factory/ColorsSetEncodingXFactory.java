
package MAFRA.Factory;

import MAFRA.CrossOver.*;

/**This class work with the cross over functions.
I'm using only the one point cross over in the color set.
    @author  Jorge Habib Namour
    */

public class ColorsSetEncodingXFactory extends CrossOverFactory{


 public  AbstractCrossOver createOnePointCrossOver()
   {
    ColorsSetOnePointCrossOver oPX;
    oPX = new ColorsSetOnePointCrossOver();
    return (oPX);
   };

   public  AbstractCrossOver createTwoPointCrossOver()
   {
    BitSetTwoPointCrossOver tPX;
    tPX = new BitSetTwoPointCrossOver();
    return(tPX);
   };

 public  AbstractCrossOver createThreePointCrossOver()
   {
    BitSetThreePointCrossOver thPX;
    thPX = new BitSetThreePointCrossOver();
    return(thPX);
   };

 public  AbstractCrossOver createFourPointCrossOver()
   {
    BitSetFourPointCrossOver fPX;
    fPX = new BitSetFourPointCrossOver();
    return(fPX);
   };


   public AbstractCrossOver createUniformCrossOver()
     {
       BitSetUniformCrossOver uX;

       uX = new BitSetUniformCrossOver();
       return (uX);
     }


   public AbstractCrossOver createBehaviourCrossOver()
     {
       BitSetBehaviourUX uX;

       uX = new BitSetBehaviourUX();
       return (uX);
     }

}
