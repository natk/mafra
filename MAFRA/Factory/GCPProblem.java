package MAFRA.Factory;


import DataStructures.*;
import java.util.BitSet;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** This class is the base for work with the GCP problem.
 @author  Jorge Habib Namour
*/

public class GCPProblem extends ProblemFactory{
    protected int amountOfColors;

    //the constructor create an object with the amount of nodes and the
    //amount of colors proposed
    public GCPProblem(double aSize, String colors)
   {
       
     super(aSize);
     this.amountOfColors = (Integer.parseInt(colors));
   }

    
  public double fitness(Individual anIndividual)
    {return 1;}

  public boolean feasibilityCheck(Individual anIndividual)
    {
      return true;
    };
    
  //this function generate a base chromosome
  //with colors ordered in increasing order
  public Object newSolution()
    {	
	ArrayList aChromosome = new ArrayList();
	int i;
        Random ran = new Random();
	System.out.println("Cant color: "+this.amountOfColors);
	for (i = 0;i<size;i++) {
	    aChromosome.add(ran.nextInt(this.amountOfColors));
	}
     
	return aChromosome;

    }

}
