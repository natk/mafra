package MAFRA.Factory;

import MAFRA.Population_Individual.*;


/** This factory is used to initialize a population with Individuals whose
Chromosome is a color set.
 @author  Jorge Habib Namour
 */
public class IndividualColorsFactory extends IndividualFactory{

  protected ProblemFactory myProblem;

  public IndividualColorsFactory(ProblemFactory myP)
    {
      myProblem = myP;
    }

  public Individual createNewIndividual()
    {
      ColorsSetIndividual aI;

      aI = new ColorsSetIndividual(myProblem);
      return aI;
    }
}
