
package MAFRA.Factory;

import MAFRA.CrossOver.*;
/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */

public class TourEncodingXFactory extends CrossOverFactory{


 /*  DPX Edge Encoding CrossOver */
 public AbstractCrossOver createDPXCrossOver(boolean edgeEncoding)
 {
  DPXEdgeEncodingCrossOver dpxX;
  DPXTourEncodingCrossOver dpxX2;
  
  if(edgeEncoding)
  {
   dpxX = new DPXEdgeEncodingCrossOver();
   return (dpxX);
  }
  else
  {
   dpxX2 = new DPXTourEncodingCrossOver();
   return (dpxX2);
  }
  
 }


 /*  Greedy Edge Encoding CrossOver */
 public AbstractCrossOver createGEECrossOver(ProblemFactory myProblem)
 {
  GreedyEdgeEncodingCrossOver geeX;
  
  geeX = new GreedyEdgeEncodingCrossOver(myProblem);
  return (geeX);
  
 }

 /* PMX permutation encoding */
 public  AbstractCrossOver createPMXCrossOver()
   {
     TourPMXCrossOver pmxX;

     pmxX = new TourPMXCrossOver();
     return (pmxX);
   }

 public  AbstractCrossOver createOnePointCrossOver()
   {
    TourOnePointCrossOver oPX;
    oPX = new TourOnePointCrossOver();
    return (oPX);
   };

   public  AbstractCrossOver createTwoPointCrossOver()
   {
    TourTwoPointCrossOver tPX;
    tPX = new TourTwoPointCrossOver();
    return(tPX);
   };

 public  AbstractCrossOver createThreePointCrossOver()
   {
    TourThreePointCrossOver thPX;
    thPX = new TourThreePointCrossOver();
    return(thPX);
   };

 public  AbstractCrossOver createFourPointCrossOver()
   {
    TourFourPointCrossOver fPX;
    fPX = new TourFourPointCrossOver();
    return(fPX);
   };


   public AbstractCrossOver createUniformCrossOver()
     {
       TourUniformCrossOver uX;

       uX = new TourUniformCrossOver();
       return (uX);
     }


   public AbstractCrossOver createBehaviourCrossOver()
     {
       TourBehaviourUX uX;

       uX = new TourBehaviourUX();
       return (uX);
     }
   


}
