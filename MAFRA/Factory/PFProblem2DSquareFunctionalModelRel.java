package MAFRA.Factory;




import DataStructures.*;
import Exceptions.*;
import Supporting.*;

import java.util.StringTokenizer;
import java.io.*;
import java.awt.*;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


public class PFProblem2DSquareFunctionalModelRel extends PFProblemFactory implements StringData
{ /*First */
  
public static char SQUARE2DABALPHABET[]={'F','B','R','L'}; 
public static int  SQUARE2DABALPHABETSIZE=4; 

public static char SQUARE2DREALPHABET[]={'F','R','L'}; 
public static int  SQUARE2DREALPHABETSIZE=3; 

public static int  RESIDUETYPE=0;    /* we use this constant to specify the first dimension of  */
                                     /* the foldingSpace matrix                                 */
public static int  RESIDUECHAINID=1; /* and this one to store the position of the residue in the*/
                                     /* chain that is positioned in foldingSpace[RESIDUETYPE][x][y] */
  
private static TextArea TextAreaMsg;
private static Frame myFrame;  
private String instance; 
private char foldingSpace[][][];
private int surfaceSize;
private int  structureLength;

 /* StringData fields */
  private char alphabet[];
  private long alphabetSize;
  private boolean variableLength; /* variableLength=1 if the strings to be represented will be of variable
                                 length, 0 otherwise */
  private long stringLength;

public PFProblem2DSquareFunctionalModelRel(String anInstance)
  {
    super(anInstance.length(),"Protein Structure Prediction");
    instance = new String(checkAlphabet(anInstance));
     surfaceSize = (int)((size * 4) + 4);
     createFoldingSpace();
    alphabet     = SQUARE2DREALPHABET;
    alphabetSize = SQUARE2DREALPHABETSIZE;
    structureLength = (int)(size-2);

    
   }

 
  public double fitness(Individual anIndividual)
    { /* START */
     String positions;
     int posi[];
     int posj[];
     int k;
     int bonds = 0;
     int Nh = 0;
     int penalties = 0;
     int aux = 0;
     int i = (int) (2*size+1);
     int j = (int) (2*size+1);
     int scale = (int)(300*(1-size/12));
     int diam  = 8;
     char heading = 'E';
 
     
     positions =(String)( anIndividual.getChromosome());
     posi = new int[instance.length()];
     posj = new int[instance.length()];
     
     /* we position the very first amino acid */
     posi[aux] = i;
     posj[aux] = j;
     foldingSpace[RESIDUETYPE][i][j] = instance.charAt(0);
     foldingSpace[RESIDUECHAINID][i][j] = 0;
     ((SelfAdaptingStaticPFStringIndividual)anIndividual).drawFirstMonomer(i*11+scale, j*11+scale, foldingSpace[RESIDUETYPE][i][j], diam);
     if (instance.charAt(0) == 'H')
     {
      Nh++;
     }
     
     
   
     /* we position the next amino acid  to the EAST*/
     aux++;
     j+=1;
     posi[aux] = i;
     posj[aux] = j;
     foldingSpace[RESIDUETYPE][i][j] = instance.charAt(1);
     foldingSpace[RESIDUECHAINID][i][j] = 1;
     ((SelfAdaptingStaticPFStringIndividual)anIndividual).drawMonomer(i*11+scale, j*11+scale, foldingSpace[RESIDUETYPE][i][j], diam);
     if (instance.charAt(1) == 'H')
     {
      Nh++;
     }
	
     
     ((PFStringIndividual)anIndividual).removeContacts();

	
	for(k=1; k <= structureLength; k++)
	{
		if (instance.charAt(k+1) == 'H'){
			Nh++;
		}
	 switch(heading)
	 {
	 case 'E': /*heading east */
	  {	
		switch (positions.charAt(k-1)){
			case 'F':	{
						j++;
						heading = 'E';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
							    {
								bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');
							    }
							else
							    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
								{
								    bonds--;
								}
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
							    {
								bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');
							    }
							else
							    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
								{
								    bonds--;
								}
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
							    {
								bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');
							    }
							else
							    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
								{
								    bonds--;
								}


							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
						    }

			
						break;
			}

			case 'L':	{
						i--;
						heading = 'N';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
						    }
						break;
						}
			case 'R':	{
						i++;
						heading = 'S';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
						    }
						break;
			}
		
		}
	     break;
	    }/* end heading E */
	 case 'N': /* heading North */
	    {	
		switch (positions.charAt(k-1)){
			case 'F':	{
					        i--;
						heading = 'N';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
						    }
						break;
						}
			case 'L':	{
					        j--;
						heading = 'W';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							
						}				
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
						    }		
						break;
						}
			case 'R':	{
						j++;
						heading = 'E';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
						    }
						break;
			}

		}
	     break;
	    }/* end  heading n */
	    
	 case 'W': /* heading west */
	    {	
		switch (positions.charAt(k-1)){
			case 'F':	{
						j--;
						heading = 'W';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
						    }
						break;
						}
			case 'L':	{
					        i++;
						heading = 'S';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
						    }
						break;
						}
			case 'R':	{
						i--;
						heading = 'N';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
						    }
						break;
			}
			
		}
	     break;
	    }/* end heading West */
	    
	 case 'S': /* heading south */
	    {	
		switch (positions.charAt(k-1)){
			
			case 'F':	{
					        i++;
						heading = 'S';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							
						}					
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
						    }	
						break;
					       }
			case 'L':	{
						j++;
					        heading = 'E';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j+1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j+1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j+1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i][j+1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
						    }
						break;
					       }
			case 'R':	{
						j--;
					        heading = 'W';
						if (foldingSpace[RESIDUETYPE][i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[RESIDUETYPE][i][j]=='P')||(foldingSpace[RESIDUETYPE][i][j]=='X'))
										{
										 foldingSpace[RESIDUETYPE][i][j] = instance.charAt(k+1);
										 foldingSpace[RESIDUECHAINID][i][j] =(char)( k+1);
										}
						if (foldingSpace[RESIDUETYPE][i][j] == 'H'){
							if(foldingSpace[RESIDUETYPE][i-1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i-1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i-1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i+1][j]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i+1][j],'H');} else
								    if(foldingSpace[RESIDUETYPE][i+1][j]=='P')
									{
									    bonds--;
									}
							if(foldingSpace[RESIDUETYPE][i][j-1]=='H')
								{bonds+=2;
								((PFStringIndividual)anIndividual).setContact(foldingSpace[RESIDUECHAINID][i][j],foldingSpace[RESIDUECHAINID][i][j-1],'H');} else
								    if(foldingSpace[RESIDUETYPE][i][j-1]=='P')
									{
									    bonds--;
									}
							
						}
						else /* the position foldingSpace[RESIDUETYPE][i][j]=='P' */
						    {
							if(foldingSpace[RESIDUETYPE][i][j-1]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i-1][j]!='X')
							    bonds--;
							if(foldingSpace[RESIDUETYPE][i+1][j]!='X')
							    bonds--;
						    }
						break;
			}

		}
	     break;
	    }/* end heading south */

	 }/* del switch(heading) */
	 /* END */
		aux++;
		posi[aux] = i;
		posj[aux] = j;
	
	        ((SelfAdaptingStaticPFStringIndividual)anIndividual).drawMonomer(i*11+scale, j*11+scale, foldingSpace[RESIDUETYPE][i][j], diam);
		
	}
			
	bonds = bonds - (10 * penalties * ((4 * Nh) + 3));
	for(int t = 0; t <= aux; t++)
		foldingSpace[RESIDUETYPE][posi[t]][posj[t]] = 'X';
		
	numberOfFitnessCalls+=1.0;	
	return bonds;     
     
     
    }
  
  public boolean feasibilityCheck(Individual anIndividual)
    {
    return true;
    };

// random fold
    public Object newSolution()
      {
       int i;
       int random;
       char  tmp[];
       int SIZE = structureLength;
       
       tmp = new char[SIZE];
       for (i=0;i<SIZE;i++)
       {
        tmp[i] =getSimbol((int)(MAFRA_Random.nextUniformLong(0,alphabetSize)));
       }
       
       return (new String(tmp));
       


      }


    private void createFoldingSpace(){
		foldingSpace = new char [2][surfaceSize][surfaceSize];
		for (int i = 0; i < surfaceSize; i++){
			for (int j = 0; j < surfaceSize; j++){
				foldingSpace[RESIDUETYPE][i][j] = 'X';
			}
		}
	}



  /* Implementation of the StringData methods */
  public void setAlphabetSize(long size)
    {
      alphabetSize = size;
    }

  public void setVariableLength(boolean variableL)
    {
      variableLength = variableL;
    }

  public void setStringLength(long lengthS)
    {
      stringLength = lengthS;
    }


  public void setSimbols(char simbols[])
    {
      int i;
      for(i=0;i<alphabetSize;i++)
	{
	  alphabet[i] = simbols[i];
	}
    }

  public long getAlphabetSize()
    {
      return alphabetSize;
    }



  public char []getAlphabet()
    {

	return  SQUARE2DREALPHABET;
    }
 


  public boolean getVariableLength()
    {
      return variableLength;
    }

  public long getStringLength()
    {
      return stringLength;
    }

  public char getSimbol(long number)
    {
      return alphabet[(int)number];
    }
  
  /** Generates a random substructure of a given length in the approapriate alphabet */
  public String getSubstructure(long length)
  {
   char substr[];
   int rndSimb;
   int f;
   
   substr = new char[(int)length];
   for (f=0;f<length;f++)
   {
    rndSimb   = ((int)MAFRA_Random.nextUniformLong(0,getAlphabetSize()));
    substr[f]= getSimbol(rndSimb);
   }
  return (new String(substr));
 }
 
 /** Generates a stretched substructure, in this alphabet means a substructure all with F */
 public String getStretchSubstructure(long length)
  {
   char substr[];
   char rndSimb;
   int f;
   
   substr = new char[(int)length];
   rndSimb = 'F';
   for (f=0;f<length;f++)
   {
    substr[f]= rndSimb;
   }
  return (new String(substr));
 }
 
 
 /* Generates a pivoted substructure by mutating one position in the string */
 public String getPivotSubstructure(String aStructure, int where, int rLength, int pivotType)
 {
  int i;
  String newStructure;
  char symbol;
  
  newStructure = aStructure.substring(0,where);
  
  
   symbol=aStructure.charAt(where);
   switch(pivotType)
   {
    case 1:
    {
     switch(symbol)
     {
      case 'F':
      {
       symbol='L';
       break;
      }
    
      case 'R':
      {
       symbol='L';
       break;
      }

      case 'L':
      {
       symbol='R';
       break;
      }
     }
     break;
    }
   
    case 2:
    {
    switch(symbol)
     {
      case 'F':
      {
       symbol='R';
       break;
      }
      case 'L':
      {
       symbol='R';
       break;
      }
     case 'R':
	 {
	     symbol='L';
	     break;
	 }

     }
    }
   }
  // System.out.println("-->"+newStructure+"("+newStructure.length()+")");
   newStructure=newStructure+String.valueOf(symbol);
  
  
  if(newStructure.length()<aStructure.length())
  {
   newStructure= newStructure+aStructure.substring(where+1);
  }
 // System.out.println("oldStructure="+aStructure+"post:"+where+", length:"+rLength+", newStructure="+newStructure);
  return (newStructure);
 }
 
    /** This is implemented, in a first approximation, as a sliding window of size length moving ONE position to the right */
 public String getShiftSubstructure(String aStructure, int where, int length)
 {

  String temp;
  int toWhere;
  char simbol;  

  simbol=aStructure.charAt(where+length);
  temp = aStructure.substring(0,where)+ simbol+aStructure.substring(where,where+length)+aStructure.substring(where+length+1,aStructure.length());

  return(temp );
 
 }
 
 public String getReflectSubstructure(String aStructure, int where, int rLength, int reflectType)
    {/* ignores reflectType */
  int i;
  String newStructure;
  char symbol;
  
   newStructure = aStructure.substring(0,where);
   for(i=where;i<(where+rLength);i++)
   {
    symbol=aStructure.charAt(i); 
    switch(symbol)
    {
     case 'F':{
      symbol = 'F';
      break;
     }
     case 'R':{
      symbol = 'L';
      break;
     }
     case 'L':{
      symbol = 'F';
      break;
     }

    }
    newStructure=newStructure+String.valueOf(symbol); 
   }
   if(newStructure.length()<aStructure.length())
   {
    newStructure= newStructure+aStructure.substring(where+rLength);
   }
  // System.out.println("oldStructure="+aStructure+"post:"+where+", length:"+rLength+", newStructure="+newStructure);
  return (newStructure);
  }

 
 

  /* This is NOT implemented yet for the sq2d!!!!!! */
  /* This is NOT implemented yet for the sq2d!!!!!! */
  /* This is NOT implemented yet for the sq2d!!!!!! */
 /** This method converts a string in the absolute encoding to one in the relative */
public char[] abs2Rel(String absS)
{
 int i;
 char relTemp[];
 int l = absS.length();
 char heading;
 char sym='F';
 
 relTemp = new char[l];
 for(i=1;i<l;i++)
 {
  heading=absS.charAt(i-1);
  switch(heading)
  {
   case 'E':
   {
    switch(absS.charAt(i))
    {
     case 'E':
     {
      sym='F';
      break;
     }
     case 'n':
     {
      sym='n';
      break;
     }
     case 'N':
     {
      sym='N';
      break;
     }
     case 'W':
     {
      sym='F';
      break;
     }
     case 's':
     {
      sym='s';
      break;
     }
     case 'S':
     {
      sym='S';
      break;
     }
    
    }/* end switch */
    break;
   }/* end heading E */
   
   
   case 'n':
   {
    switch(absS.charAt(i))
    {
     case 'E':
     {
      sym='S';
      break;
     }
     case 'n':
     {
      sym='F';
      break;
     }
     case 'N':
     {
      sym='n';
      break;
     }
     case 'W':
     {
      sym='N';
      break;
     }
     case 's':
     {
      sym='F';
      break;
     }
     case 'S':
     {
      sym='s';
      break;
     }
    
    }/* end switch */
    break;
   }/* end heading n */
  

   case 'N':
   {
    switch(absS.charAt(i))
    {
     case 'E':
     {
      sym='s';
      break;
     }
     case 'n':
     {
      sym='S';
      break;
     }
     case 'N':
     {
      sym='F';
      break;
     }
     case 'W':
     {
      sym='n';
      break;
     }
     case 's':
     {
      sym='N';
      break;
     }
     case 'S':
     {
      sym='F';
      break;
     }
    
    }/* end switch */
    break;
   }/* end heading N */
   
   
   case 'W':
   {
   
     switch(absS.charAt(i))
    {
     case 'E':
     {
      sym='F';
      break;
     }
     case 'n':
     {
      sym='s';
      break;
     }
     case 'N':
     {
      sym='S';
      break;
     }
     case 'W':
     {
      sym='F';
      break;
     }
     case 's':
     {
      sym='n';
      break;
     }
     case 'S':
     {
      sym='N';
      break;
     }
    
    }/* end switch */
   
    break;
   }/* end heading W */
  
  
   case 's':
   {
   
    switch(absS.charAt(i))
    {
     case 'E':
     {
      sym='N';
      break;
     }
     case 'n':
     {
      sym='F';
      break;
     }
     case 'N':
     {
      sym='s';
      break;
     }
     case 'W':
     {
      sym='S';
      break;
     }
     case 's':
     {
      sym='F';
      break;
     }
     case 'S':
     {
      sym='n';
      break;
     }
    
    }/* end switch */
   
    break;
   }/* end heading s*/
  

   case 'S':
   {
    switch(absS.charAt(i))
    {
     case 'E':
     {
      sym='n';
      break;
     }
     case 'n':
     {
      sym='N';
      break;
     }
     case 'N':
     {
      sym='F';
      break;
     }
     case 'W':
     {
      sym='s';
      break;
     }
     case 's':
     {
      sym='S';
      break;
     }
     case 'S':
     {
      sym='F';
      break;
     }
   
    }/* end switch */
   
    break;
   }
   
  }/* end switch heading*/
  relTemp[i] = sym;
 }/* end for i */
 
 return (relTemp);
}/* end method */




  /* This is NOT implemented yet for the sq2d!!!!!! */
  /* This is NOT implemented yet for the sq2d!!!!!! */
  /* This is NOT implemented yet for the sq2d!!!!!! */
 /** This method converts a string in the relative encoding to one in the absolute */
 public char[] rel2Abs(String relS)
 {
  int i;
  char absTemp[];

  int l = relS.length();
  char heading='E';
 
  absTemp = new char[l];
  absTemp[0]='E';
  for(i=1;i<l;i++)
  {
   switch(heading)
	 {
	  case 'E':
	  {	
		switch (relS.charAt(i)){
			case 'F':	{
						
						heading = 'E';
						
						break;
					}	

			case 'N':	{
						
						heading = 'N';
						
						break;
					}
						
			case 'n':	{
						
						heading = 'n';
						
						break;
					}
						
			case 'S':	{
						
						heading = 'S';
						
						break;
					}
			case 's':	{
						
						heading ='s';
						
						break;
						
	                               }
	     }
	     break;
	    }/* heading E */
	    case 'n':
	    {	
		switch (relS.charAt(i)){
			case 'S':	{
						
						heading = 'E';
						
						break;
					}
			case 'N':	{
					    
						heading = 'W';
											
						break;
					}
			case 'n':	{
						
						heading = 'N';
						
						break;
					}
			case 'F':	{
						
						heading = 'n';
						
						break;
				       	}
			case 's':	{
						
						heading = 'S';
						
						break;
				       	}
		}
	      
	     break;
	    }/* heading n */
	    
            case 'N':
	    {	
		switch (relS.charAt(i)){
			case 's':	{
						
						heading = 'E';
						
						break;
					}
			case 'n':	{
					    
						heading = 'W';
						
						break;
				       	}
			case 'F':	{
			      	      	      	heading = 'N';
						break;
					}
			case 'S':	{
						
						heading = 'n';
						
						break;
				        }
		
			case 'N':	{
						
						heading ='s';
						
						break;
				        }
		}
	     break;
	    }/* heading N */
	    
	    case 'W':
	    {	
		switch (relS.charAt(i)){
			
			case 'F':	{
					       
						heading = 'W';
											
						break;
					       }
			case 'S':	{
						
						heading = 'N';
						
						break;
					       }
			case 's':{
			          heading = 'n';
				  break;
				 }		
			case 'N':	{
						
						heading = 'S';
						
						break;
				       	}
			case 'n':{
			          heading = 's';
				  break;
				 }
			
		}
	     break;
	    }/* heading W */

	    case 's':
	    {	
		switch (relS.charAt(i)){
			case 'N':	{
						
						heading = 'E';
						
						break;
						}
			case 'S':	{
					    
						heading = 'W';
											
						break;
						}
			case 's':	{
						
						heading = 'N';
						
						break;
						}
			
			case 'n':	{
						
						heading = 'S';
						
						break;
					      }
			case 'F':{
			 heading ='s';
			 break;
			}		   
		} 
	     break;
	    }/* heading s */
	    
	    case 'S':
	    {	
		switch (relS.charAt(i)){
			case 'n':	{
						
						heading = 'E';
						
						break;
					 }
						
			case 's':	{
					     
						heading = 'W';
											
						break;
					       
			}
			case 'N':	{
						
						heading = 'n';
						
						break;
						}
			case 'F':	{
						
						heading = 'S';
						
						break;
						}
			case 'S':	{
						heading ='s';
						
						break;
					      }
		}
	     break;
	    }/* heading S */
	    
	    
	 
	 }/* del switch(heading) */
	 absTemp[i]=heading;
 
  }/* del for*/
  
  return(absTemp);
 } /* rel2Abs */




static String checkAlphabet(String aS)
{
    String convertedString="";
    int i;

    if ((aS.charAt(0)=='H')||(aS.charAt(0)=='h')||(aS.charAt(0)=='p')||(aS.charAt(0)=='P'))
	{
	    return(aS.toUpperCase());
	}
    
    
    for(i=0;i<aS.length();i++)
	{
	   if ( aS.charAt(i)=='1')
	       {
		   convertedString+='H';
	       }
	   else
	       {
		   convertedString+='P';
	       }
	}
    System.out.println("String Converted from 0-1 alphabet to HP alphabet :"+convertedString);

    return (convertedString);
}


}
