package MAFRA.Factory;




import java.util.StringTokenizer;
import java.io.*;
import java.awt.*;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

//M2912-2
public class TSPProblemEdgeEncoding extends ProblemFactory{
  
private static int MAXCITIES = 1000;
private static TextArea TextAreaMsg;
private static Frame myFrame;     
private static int NCities; /* size of the instance */
private static double Dist[][];
private static double  Position[][];


private double    myDist[][];
private double  myPosition[][];
private double  minX;
private double  maxX;
private double  minY;
private double  maxY;
private int     aNNTour[];

     
     



public TSPProblemEdgeEncoding(String aName)
  {
    super(0,aName);
  }
  
/** This method reads TSPLIB standard files as described in
http://www.iwr.uni-heidelberg.de/iwr/comopt/soft/TSPLIB95/TSPLIB.html .
It was written by Luciana Buriol (buriol@densis.fee.unicamp.br)  
and adapted to MAFRA by Natalio Krasnogor.
*/
 public static Object readInstance(String fileName)
   {
     int i,j;
     boolean alreadyInserted[];
     int actualCityIndex;
     int cityToInsert;
     double minDist;
     
     TextAreaMsg = new TextArea(); 
     TSPProblemEdgeEncoding myInstance=new TSPProblemEdgeEncoding(fileName); 
     ReadTSPFile(fileName,new DataOutputStream(System.out),fileName,1.0);
     myInstance.setSize(NCities);
     myInstance.myDist     = Dist;
     myInstance.myPosition = Position;

     myInstance.minX = Double.POSITIVE_INFINITY;
     myInstance.minY = Double.POSITIVE_INFINITY;
     myInstance.maxX = Double.NEGATIVE_INFINITY;
     myInstance.maxY = Double.NEGATIVE_INFINITY;
     for (i=0;i<myInstance.getSize();i++)
       {
	 if (myInstance.minX>myInstance.myPosition[i][0])
	   myInstance.minX = myInstance.myPosition[i][0];

	 if (myInstance.minY>myInstance.myPosition[i][1])
	   myInstance.minY = myInstance.myPosition[i][1];

	 if (myInstance.maxX<myInstance.myPosition[i][0])
	   myInstance.maxX = myInstance.myPosition[i][0];

	 if (myInstance.maxY<myInstance.myPosition[i][1])
	   myInstance.maxY = myInstance.myPosition[i][1];



       }

     /* we construct and save a NN tour wich can be used later for generating new random solutions from it */
  /*   myInstance.aNNTour = new int[NCities];
  
    
     alreadyInserted = new boolean[NCities];
     for(i=0;i<NCities;i++)
       {
	 alreadyInserted[i] = false;
       }
     myInstance.aNNTour[0]         = 0;
     alreadyInserted[0] = true;
     cityToInsert       = 0;
     for (i=0;i<NCities-1;i++)
       {
	 minDist            = Long.MAX_VALUE;
	 for (j=0;j<NCities;j++)
	   {
	     if ( (Dist[myInstance.aNNTour[i]][j]<=minDist) && (!alreadyInserted[j]) && (i!=j) )
	       {
		 minDist      = Dist[myInstance.aNNTour[i]][j];
		 cityToInsert = j;
	       }
	   }
	 alreadyInserted[cityToInsert]       = true;
	 myInstance.aNNTour[i+1] = cityToInsert;

       }
    */
     return myInstance;
   }



 


  public double fitness(Individual anIndividual)
    {
      double totalDistance = 0.0;
      int i;
      int aTour[];

      numberOfFitnessCalls+=1.0;

      aTour = (int[]) anIndividual.getChromosome();
      for (i=0;i<size;i++)
	{
	  totalDistance = totalDistance + myDist[i][aTour[i]];
	}
	  
      return -totalDistance;
    };
  
  public boolean feasibilityCheck(Individual anIndividual)
    {
    return true;
    };

// random tour
    public Object newSolution()
      {
	int aTour[];
	int finalTour[];
	int i;
	int howMuchToSwap;

	aTour = new int[(int)size];
	finalTour = new int[(int)size];

	for (i=0;i<(int)size;i++)
	  {
	    aTour[i]=i;
	  }

        howMuchToSwap = (int)size;
        for (i=0;i<howMuchToSwap;i++)
        {
	 twoSwap(aTour,(int)size);
        }
      
       for(i=0;i<(int)(size-1);i++)
       {
        finalTour[aTour[i]]=aTour[i+1];
       }
       finalTour[aTour[(int)size-1]]=aTour[0];
       
       return finalTour;


      }


// nearest neighbour
public Object newSolution2()
{
 int tempFinalTour[];
 int finalTour[];
 
 tempFinalTour=(int [])nnPermEncoding();
 finalTour=(int[])permEncoding2EdgeEncoding(tempFinalTour,(int)size);
 
return finalTour;
}


// nearest neighbour insertion: NO ANDA!
  public  Object newSolution8()
    {
      int permTour[];
      int finalTour[];
      int i;
      int startCity;
      int endCity;
      int valAtSwap1;
      int howMuchToSwap;
      boolean alreadyInserted[];
      int NAlreadyInserted;
      int cityToInsert;
      double minDist;
      int j;



      permTour = new int[(int)size];
      finalTour = new int[(int)size];
   
     alreadyInserted = new boolean[(int)size];
     for(i=0;i<(int)size;i++)
     {
      alreadyInserted[i] = false;
      permTour[i]=i;
     }
     
     howMuchToSwap = (int)size;
     for (i=0;i<howMuchToSwap;i++)
     {
      twoSwap(permTour,(int)size);
     }
     
    
     startCity        = permTour[0];
     endCity          = permTour[0];  
     NAlreadyInserted = 0;
     int atI=0;
     do
     {
      minDist          = Double.MAX_VALUE;
      for (i=0;i<size;i++)
      {

       if((!alreadyInserted[permTour[i]]) && (startCity!=permTour[i]))
       {
        if( (Dist[startCity][permTour[i]]<=minDist) )
        {
       	 endCity = permTour[i];
	// atI     = i;
	 minDist = Dist[startCity][permTour[i]];
        }
       }
      }
      finalTour[startCity] = endCity;
      startCity            = endCity;
      alreadyInserted[startCity] = true;
      NAlreadyInserted++;
     }while(NAlreadyInserted<size); 
     
     System.out.println("Tour:");
     for (i=0;i<size;i++)
     {
      System.out.print(finalTour[i]+",");
     }
     System.out.println("");
     return finalTour; 
     
    };

private int minimumInsertion(int cityToInsert,int aTour[],int nCities)
{
 double minDist;
 int f;
 int pos;
 
// minDist = Dist[aTour[0]][cityToInsert]+Dist[cityToInsert][aTour[1]];
 minDist = Double.POSITIVE_INFINITY;
 pos     = 0;
 for (f=0;f<nCities;f++)
 {
  if(aTour[f]!=-1)
  {
   if (minDist>Dist[f][cityToInsert]+Dist[cityToInsert][aTour[f]])
   {
    minDist = Dist[f][cityToInsert]+Dist[cityToInsert][aTour[f]];
    pos     = f;
   }
  } 
 }
 return pos;
 
}

private void insertAt(int aTour[],int cityToInsert,int toBeInsertedAt)
{
 int swap;
 
 swap = aTour[toBeInsertedAt];
 aTour[toBeInsertedAt]= cityToInsert;
 aTour[cityToInsert]=swap;
  


}

// one city insertion
  public  Object newSolution3()
    {
      int aTour[];
      int aTmpTour[];
      int i;
      int howManyInTour;
      int howMuchToSwap;
      boolean alreadyInserted[];
      int cityToInsert;
      int toBeInsertedAt;
      double actualLength;
      double minDist;
      int j;
      int pos;
      int swap;

      aTour    = new int[(int)size];
      aTmpTour = new int[(int)size];
   
      alreadyInserted = new boolean[NCities];
      for(i=0;i<NCities;i++)
      {
	 alreadyInserted[i] = false;
      }
      for (i=0;i<(int)size;i++)
      {
       aTmpTour[i]=i;
       aTour[i]=-1;
      }
      howMuchToSwap = (int)size;
      for (i=0;i<howMuchToSwap;i++)
      {
//	twoChange(aTmpTour,Dist,(int)size);
        twoSwap(aTmpTour,(int)size);
      }

      aTour[aTmpTour[0]]         = aTmpTour[1];
      alreadyInserted[aTmpTour[1]] = true;
      aTour[aTmpTour[1]]         = aTmpTour[2];
    //  alreadyInserted[aTmpTour[2]]=true;
      howManyInTour      = 2;
      System.out.print("tt2="+aTmpTour[2]);
 
      for (i=0;i<NCities;i++)
      {
       // choose city not yet in tour
       cityToInsert = aTmpTour[i];
       if (!alreadyInserted[aTmpTour[i]])
       {
        // choose where to insert
        toBeInsertedAt = minimumInsertion(cityToInsert,aTour,NCities); 
        // insert at the appropriate position
        insertAt(aTour,cityToInsert,toBeInsertedAt);
        alreadyInserted[cityToInsert]=true;
        howManyInTour++;
       }
      }
      System.out.print("T[tt2]="+aTour[aTmpTour[2]]);
      System.out.println("Tour:");
      for (i=0;i<NCities;i++)
      {
       System.out.print(aTour[i]+"-");
      }
      System.out.println(); 
      return aTour;

    };





public double[][] getDistances()
  {
    return myDist;
  }

public double[][] getPositions()
  {
    return myPosition;
    
  }

public double getMinX()
  {
    return minX;
  }


public double getMaxX()
  {
    return maxX;
  }


public double getMinY()
  {
    return minY;
  }


public double getMaxY()
  {
    return maxY;
  }



private void twoSwap(int c[],long size)
{
  int city1;
  int city2;
  int tmpCity;
  
  city1 = (int)MAFRA_Random.nextUniformLong(0,size);
  city2 = (int)MAFRA_Random.nextUniformLong(0,size);
  
  tmpCity  = c[city1];
  c[city1] = c[city2];
  c[city2] = tmpCity;
  
}


 private void twoChange(int c[], double dist[][],long size)
   {
     int startInversion;
     int endInversion;

     int tmp;

     int i;
     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     startInversion =(int)MAFRA_Random.nextUniformLong(0,size);
     endInversion =(int)MAFRA_Random.nextUniformLong(0,size);
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     for (i=startInversion;i<=endInversion;i++)
       {
	 tmp=c[endInversion-i];
	 c[endInversion-i]=c[i];
	 c[i]=tmp;
       }



   }





 /* The following methods were written by Luciana Buriol (buriol@densis.fee.unicamp.br):
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                                                 ::
::   Luciana Salete Buriol - Campinas/SP - fone: (019)242-5532     ::
::   Mestrado Faculdade Eng. Eletrica e de Computacao/UNICAMP      ::
::                                                                 ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
*/

/* ------------------------------------ ReadTSPFile
------------------------------------*/
private static String ReadTSPFile(String name, DataOutputStream ATSPFile, String
ATSPFileName, double Percentage)
{
   int token;
   String WFormat = new String(""),
	  CType = new String(""),
	  WType = new String("");

   try{
      InputStream is = new FileInputStream(name);
      StreamTokenizer stok = new StreamTokenizer(is);

      stok.resetSyntax();/*It Resets the syntax*/
      stok.wordChars(" ".charAt(0), "z".charAt(0)); /*It sets all the
chars as "word chars"*/
      stok.whitespaceChars(" ".charAt(0), " ".charAt(0));/*It sets the
char " " as the only tokens separator*/
      stok.eolIsSignificant(true);/*It sets the end of line as a word
separtor too*/
      stok.parseNumbers();/*It reads a number with point and less sinal*/

      token = stok.nextToken();/*It gets the next token...*/

      do{
	 if (token == stok.TT_WORD){
	    if (stok.sval.equals("NAME") || stok.sval.equals("NAME:"))
ATSPFile.writeBytes("NAME : " +ATSPFileName + '\n');
	    else if (stok.sval.equals("COMMENT") ||
stok.sval.equals("COMMENT:")){
	       if (stok.sval.equals("COMMENT")) token=stok.nextToken();
//put out the two points
	       ATSPFile.writeBytes("COMMENT : " + "file generated from the file ");
	       while((token=stok.nextToken()) != stok.TT_EOL) {
		 if (token == stok.TT_NUMBER)
ATSPFile.writeBytes(String.valueOf((int)stok.nval));
		 else if (token == stok.TT_WORD) ATSPFile.writeBytes(' '+stok.sval);
	       }
	       ATSPFile.writeBytes('\n' +"PERCENTAGE OF AVERAGE DISTANCE :" +String.valueOf((double)Percentage) + '\n');
	    }

	    else if (stok.sval.equals("TYPE") ||
stok.sval.equals("TYPE:")) ATSPFile.writeBytes("TYPE : TSP" + '\n');

	    else if (stok.sval.equals("DIMENSION") ||
stok.sval.equals("DIMENSION:")){
	       while(token != stok.TT_NUMBER) token = stok.nextToken();
	       NCities = (int)stok.nval;
	       ATSPFile.writeBytes("DIMENSION : "+String.valueOf((int)NCities) +'\n');
	    }

	    else if (stok.sval.equals("EDGE_WEIGHT_TYPE") ||
stok.sval.equals("EDGE_WEIGHT_TYPE:")){
	       if (stok.sval.equals("EDGE_WEIGHT_TYPE"))
token=stok.nextToken(); //put out the two points
	       token=stok.nextToken();
	       WType = stok.sval;
	    }

	    else if (stok.sval.equals("EDGE_WEIGHT_FORMAT") ||
stok.sval.equals("EDGE_WEIGHT_FORMAT:")){
	       if (stok.sval.equals("EDGE_WEIGHT_FORMAT"))
token=stok.nextToken(); //put out the two points
	       token=stok.nextToken();
	       WFormat = stok.sval;
	    }

	    else if (stok.sval.equals("NODE_COORD_TYPE") ||
stok.sval.equals("NODE_COORD_TYPE:")){
	       if (stok.sval.equals("NODE_COORD_TYPE"))
token=stok.nextToken(); //put out the two points
	       token=stok.nextToken();
	       CType = stok.sval;
	    }

	    while(token != stok.TT_EOL) token=stok.nextToken();
	    token=stok.nextToken();//pega o primeiro token da linha
	 }
      }while(token != stok.TT_NUMBER);

      Dist = new double[NCities][NCities];
      ReadDatas(WType, WFormat, CType, ATSPFile, stok);
      is.close();
   }//try
   catch(IOException e){
      System.err.println ("File not opened properly" + e.toString());
      System.exit(1);
   }
   return(WType);
}


/* ------------------------------------ ReadDatas
------------------------------------*/
private static void ReadDatas(String WType, String WFormat, String CType,
DataOutputStream ATSPFile, StreamTokenizer stok)
{
   int NodeCoorType=0;

   if(WType.equals("EUC_2D") || WType.equals("MAX_2D") ||
WType.equals("MAN_2D") || WType.equals("CEIL_2D") || WType.equals("GEO")
|| WType.equals("ATT")) NodeCoorType=2;
   else if(WType.equals("EUC_3D") || WType.equals("MAX_3D") ||
WType.equals("MAN_3D")) NodeCoorType=3;
   if (NodeCoorType != 0){
      Read_Coor(NodeCoorType,ATSPFile, stok);
      calcule_distance(WType);
   }
   else if (WType.equals("EXPLICIT") || WType.equals("")){
      Read_Matrix(WFormat, ATSPFile, stok);
   }
}


/* ------------------------------------ Read_Matrix
------------------------------------*/
private static void Read_Matrix(String WFormat, DataOutputStream ATSPFile,
StreamTokenizer stok)
{
   int i, j, token;

   try{
   if(WFormat.equals("FULL_MATRIX")){
      for(i=0; i<NCities; i++){
	 for(j=0; j<NCities; j++){
	    Dist[i][j] = Dist[j][i] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("UPPER_ROW")){
      for(i=0; i<(NCities-1); i++){
	 for(j=(i+1); j<NCities; j++){
	    Dist[i][j] = Dist[j][i] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("LOWER_ROW")){
      for(i=1; i<NCities; i++){
	 for(j=0; j<i; j++){
	    Dist[i][j] = Dist[j][i] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("UPPER_DIAG_ROW")){
      for(i=0; i<NCities; i++){
	 for(j=i; j<NCities; j++){
	    Dist[i][j] = Dist[j][i] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("LOWER_DIAG_ROW")){
      for(i=0; i<NCities; i++){
	 for(j=0; j<=i; j++){
	    Dist[i][j] = Dist[j][i] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("UPPER_COL")){
      for(i=1; i<NCities; i++){
	 for(j=0; j<i; j++){
	    Dist[j][i] = Dist[i][j] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("LOWER_COL")){
      for(i=0; i<(NCities-1); i++){
	 for(j=(i+1); j<NCities; j++){
	    Dist[j][i] = Dist[i][j] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("UPPER_DIAG_COL")){
      for(i=0; i<NCities; i++){
	 for(j=0; j<=i; j++){
	    Dist[j][i] = Dist[i][j] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   else if(WFormat.equals("LOWER_DIAG_COL")){
      for(i=0; i<NCities; i++){
	 for(j=i; j<NCities; j++){
	    Dist[j][i] = Dist[i][j] = (double)stok.nval;
	    token = stok.nextToken();
	    if (token == stok.TT_EOL) token = stok.nextToken();
	 }
      }
   }

   } catch(IOException e){
      System.err.println ("File not opened properly" + e.toString());
      System.exit(1);
   }
}


/* ------------------------------------ Read_Coor
------------------------------------*/
private static void Read_Coor(int NodeCoorType, DataOutputStream ATSPFile,
StreamTokenizer stok)
{
   int token, j;
   double tempdbl = Double.POSITIVE_INFINITY;

   try{

   /*Print on the Area to Messages to users*/
   TextAreaMsg.append("Reading the Coordenates from file..." +'\n');

   Position = new double[NCities][NodeCoorType];


   stok.resetSyntax();
   stok.whitespaceChars("\t".charAt(0), " ".charAt(0));
   stok.wordChars("-".charAt(0), "d".charAt(0));     //'e' and '+' are special chars
   stok.wordChars("f".charAt(0), "z".charAt(0));     //so we can handle them explicitly
   stok.parseNumbers();

   for(int i=0; i<NCities; i++){

      token = stok.nextToken();//tira o indice da linha

      for (j=0; j<NodeCoorType; j++){
         tempdbl = stok.nval;

         token = stok.nextToken();           //get next token to check for
	 if(token == "e".charAt(0)) {        //scientific notation
            token = stok.nextToken();
            if(token == "+".charAt(0)) token = stok.nextToken(); //number parser doesn't handle plus
            if(token == StreamTokenizer.TT_NUMBER) {
	       Position[i][j] = tempdbl * Math.pow(10, stok.nval);
	       token = stok.nextToken();
            } 
            else stok.pushBack();
         } 
         else{
            stok.pushBack();
	    Position[i][j] = tempdbl;
	    token = stok.nextToken();
         }
      }

      while (token != stok.TT_EOL) token = stok.nextToken();

       token = stok.nextToken();

   }
   } catch(IOException e){
      System.err.println ("File not opened properly" + e.toString());
      System.exit(1);
   }
}


/* ------------------------------------ calcule_distance
------------------------------------*/
private static void calcule_distance(String Type)
{
   /*Print on the Area to Messages to users*/
   TextAreaMsg.append("Calculeting the Distance Matrix..." +'\n');

   if (Type.equals("ATT")){//ok
      int j;
      double delta_x, delta_y, rij;
      double tij;

      for (int i=0; i<NCities; i++){
	 Dist[i][i]=0;
	 for (j=i+1; j<NCities; j++){
	    delta_x = Position[i][0] - Position[j][0];
	    delta_y = Position[i][1] - Position[j][1];
	    rij = Math.sqrt((delta_x*delta_x+delta_y*delta_y)/10.0);
	    tij = Math.rint(rij+0.5);
	    if(tij<rij) Dist[i][j] = tij+1;
	    else Dist[i][j] = tij;
	    Dist[j][i] = Dist[i][j];
	 }
      }
   }

   else if (Type.equals("CEIL_2D")){
      int j;
      double delta_x, delta_y;
      for (int i=0; i<NCities; i++){
	 Dist[i][i]=0;
	 for (j=i+1; j<NCities; j++){
	    delta_x = Position[i][0] - Position[j][0];
	    delta_y = Position[i][1] - Position[j][1];
	    Dist[i][j] =
(long)Math.ceil((long)(Math.sqrt(Math.pow(delta_x,2) +
Math.pow(delta_y,2)) + 0.5));
	    Dist[j][i] = Dist[i][j];
	 }
      }
   }

   else if (Type.equals("EUC_2D") || Type.equals("EUC_3D")){//ok
      int j;
      double delta_x, delta_y, delta_z;
      for (int i=0; i<NCities; i++){
	 Dist[i][i]=0;
	 for (j=i+1; j<NCities; j++){
	    delta_x = Position[i][0] - Position[j][0];
	    delta_y = Position[i][1] - Position[j][1];
	    if(Type.equals("EUC_3D"))
	      {
	       delta_z = Math.abs(Position[i][2] - Position[j][2]);
	       Dist[i][j] = Math.rint(Math.sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z)+0.5);
	      }
	    else
	      {
		Dist[i][j] = Math.rint(Math.sqrt(delta_x*delta_x+delta_y*delta_y)+0.5);
	      }
	    Dist[j][i] = Dist[i][j];
	 }
      }
   }//"EUC_2D" e "EUC_3D"


   else if (Type.equals("GEO")){//ok
      double PI=3.141592, RRR=6378.388, min, q1, q2, q3;
      long deg;
      double latitude[] = new double[NCities], longitude[] = new
double[NCities];
      int i, j;

      for (i=0; i<NCities; i++){
	 deg = (long)Position[i][0];
	 min = Position[i][0] - deg;
	 latitude[i] = PI * (deg + 5*min/3)/180;
	 deg = (long)Position[i][1];
	 min = Position[i][1] - deg;
	 longitude[i] = PI * (deg + 5*min/3)/180;
      }

      for (i=0; i<NCities; i++){
	 Dist[i][i]=0;
	 for (j=i+1; j<NCities; j++){
	    q1 = Math.cos(longitude[i] - longitude[j]);
	    q2 = Math.cos(latitude[i] - latitude[j]);
	    q3 = Math.cos(latitude[i] + latitude[j]);
	    Dist[i][j] = (long)((RRR * Math.acos(0.5 * ((1+q1)*q2 -
(1-q1)*q3)) + 1));
	    Dist[j][i] = Dist[i][j];
	 }
      }
   }

   else if (Type.equals("MAN_2D") || Type.equals("MAN_3D")){
      double delta_x, delta_y, delta_z;
      int j;

      for (int i=0; i<NCities; i++){
	 Dist[i][i]=0;
	 for (j=i+1; j<NCities; j++){
	    delta_x = Math.abs(Position[i][0] - Position[j][0]);
	    delta_y = Math.abs(Position[i][1] - Position[j][1]);

	    if(Type.equals("MAN_3D")){
	       delta_z = Math.abs(Position[i][2] - Position[j][2]);
	       Dist[i][j] = (long)(delta_x + delta_y + delta_z + 0.5);
	    }
	    else Dist[i][j] = (long)(delta_x + delta_y + 0.5);
	    Dist[j][i] = Dist[i][j];
	 }
      }
   }//"MAN_2D" e "MAN_3D"



   else if (Type.equals("MAX_2D") || Type.equals("MAX_3D")){
      double delta_x, delta_y, delta_z;
      int j;

      for (int i=0; i<NCities; i++){
	 Dist[i][i]=0;
	 for (j=i+1; j<NCities; j++){
	    delta_x = Math.abs(Position[i][0] - Position[j][0]);
	    delta_y = Math.abs(Position[i][1] - Position[j][1]);

	    if(Type.equals("MAX_3D")){
	       delta_z = Math.abs(Position[i][2] - Position[j][2]);
	       Dist[i][j] = Math.max((long)(delta_x + 0.5),(long)(delta_y
+ 0.5));
	       Dist[i][j] = Math.max(Dist[i][j] ,(long)(delta_z + 0.5));
	    }
	    else Dist[i][j] = Math.max((long)(delta_x +
0.5),(long)(delta_y + 0.5));
	    Dist[j][i] = Dist[i][j];
	 }
      }
   }//"MAX_2D" e "MAX_3D"
}


 /* The presceding methods were written by Luciana Buriol (buriol@densis.fee.unicamp.br):
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                                                 ::
::   Luciana Salete Buriol - Campinas/SP - fone: (019)242-5532     ::
::   Mestrado Faculdade Eng. Eletrica e de Computacao/UNICAMP      ::
::                                                                 ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
*/



//nearest neighbour insertion
  public  Object nnPermEncoding()
    {
      int aTour[];
      int i;
      int swap1;
      int swap2;
      int valAtSwap1;
      int howMuchToSwap;
      boolean alreadyInserted[];
      int cityToInsert;
      double minDist;
      int j;


      aTour = new int[(int)size];

   
     alreadyInserted = new boolean[NCities];
     for(i=0;i<NCities;i++)
       {
	 alreadyInserted[i] = false;
       }
     aTour[0]         = (int)(MAFRA_Random.nextUniformLong(0,(int)size-1));
     alreadyInserted[aTour[0]] = true;
     cityToInsert       = 0;
     for (i=0;i<NCities-1;i++)
       {
	 minDist            = Double.MAX_VALUE;
	 for (j=0;j<NCities;j++)
	   {
	     if ( (Dist[aTour[i]][j]<=minDist) && (!alreadyInserted[j]) && (i!=j) )
	       {
		 minDist      = Dist[aTour[i]][j];
		 cityToInsert = j;
	       }
	   }
	 alreadyInserted[cityToInsert]       = true;
	 aTour[i+1] = cityToInsert;

       }



  
    howMuchToSwap = (int)(size*0.1);
    for (i=0;i<howMuchToSwap;i++)
      {
	twoChangePermEncoding(aTour,(long)size);
	//twoSwap(aTour,(int)size);
      }
      
    return aTour;
    };





 private void twoChangePermEncoding(int c[],long size)
   {
     int startInversion;
     int endInversion;
     int maxI;
     int tmp;
     int i;
     double oldDs;
     double oldDe;
     double newDs;
     double newDe;



     startInversion =(int)MAFRA_Random.nextUniformLong(0,size-2);
     endInversion =(int)MAFRA_Random.nextUniformLong(0,size-1);
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     maxI = (int)((endInversion-startInversion)/2);
     for(i=0;i<maxI;i++)
     {
      tmp=c[startInversion+i];
      c[startInversion+i]=c[endInversion-i];
      c[endInversion-i]=tmp;
     }



   }



 private int[] permEncoding2EdgeEncoding(int aTour[], int size)
 {
  int i;
  int finalTour[];
  
  finalTour = new int[size];
  
  for(i=0;i<(size-1);i++)
  {
   finalTour[aTour[i]]=aTour[i+1];
  }
  finalTour[aTour[(int)size-1]]=aTour[0];
  return finalTour;    
 }




}
