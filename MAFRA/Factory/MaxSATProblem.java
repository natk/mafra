package MAFRA.Factory;



import java.util.BitSet;
import java.util.StringTokenizer;
import java.io.*;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


public class MaxSATProblem extends ProblemFactory{
  
  protected String Clauses;
  

 public static Object readInstance(String fileName)
   {
     /* Problem definition variables */
     double numberOfVariables = -1;
     String collectionOfClauses="";
     MaxSATProblem aProblemInstance= null;


     /* Variable definitions to read the instance file which must
	be in DIMACS CNF format */
     File              instanceFile;
     FileInputStream   fileInputStream;
     InputStreamReader inputStreamReader;
     BufferedReader    bufferedReader;
     String            fileLine;
     StringTokenizer   aLineTokenizer;
     String            aLineToken;

     try
       {
	 instanceFile      = new File(fileName);
	 fileInputStream   = new FileInputStream(instanceFile);
	 inputStreamReader = new InputStreamReader(fileInputStream);
	 bufferedReader    = new BufferedReader(inputStreamReader); /* This is _actually_ from
                                                                       where we are gona read */

	 do
	   {
	     fileLine = bufferedReader.readLine();
	     aLineTokenizer = new StringTokenizer(fileLine," ");
	     aLineToken     = (aLineTokenizer.nextToken()).trim();
	     if (aLineToken.equals("p"))
	       {
		 aLineTokenizer.nextToken();
		 aLineToken = (aLineTokenizer.nextToken()).trim();
		 /* Number of variables read */
		 numberOfVariables = (new Double(aLineToken)).doubleValue();
		
	       } else
	     if((!aLineToken.equals("c")) &&  (!aLineToken.equals("0")) && (!aLineToken.equals("%")))
	       { /* this is a clause, and we are gonna read it */
		 collectionOfClauses= collectionOfClauses+MaxSATProblem.newClause(fileLine)+";";
	       } else
	     if ( (aLineToken.equals("0")) || (aLineToken.equals("%")) )
	       {
		 fileLine = null;
		 collectionOfClauses = collectionOfClauses.substring(0,collectionOfClauses.length()-1)+".";
	       }
	     
	   } while (fileLine != null);
	       
	   if (numberOfVariables>0)
	     { /* The file was read correctly */
	       System.out.println(collectionOfClauses+",|"+numberOfVariables+"|");

	       aProblemInstance = new MaxSATProblem(numberOfVariables,collectionOfClauses);
	     }



       }
     catch (Throwable e)
       {
	 System.out.println("ERROR - exception "+e+" was generated in readInstance() in MaxSATProblem.java");
       }

     return aProblemInstance;
     

   }


 private static String newClause(String aLine)
   {
     String aClause;
     StringTokenizer aLineTokenizer;
     String aToken;
     aClause = "(";
     aLineTokenizer = new StringTokenizer(aLine," ");
     while(aLineTokenizer.hasMoreTokens())
       {
	aToken = (aLineTokenizer.nextToken()).trim();

	if (!aToken.equals("0"))
	{
	      aClause= aClause + aToken;
	      if (aLineTokenizer.countTokens()>1)
		{
		  aClause = aClause + ",";
		}
	}
       }
     aClause = aClause+")";
     return aClause;
   }


 public MaxSATProblem(double numberOfVariables,String collectionOfClauses)
   {
     super(numberOfVariables,"Max"+" "+numberOfVariables+" SAT","Maximize");
     Clauses = new String(collectionOfClauses);

   }

 /* This method recomputes the fitness of the individual.*/
  public double fitness(Individual anIndividual)
    {
      
      long j; /* number of satisfied clauses */
      long aCSize;
      BitSet  aC;
      StringTokenizer aClauseTokenizer;
      String          aClause;

      numberOfFitnessCalls+=1.0;
      
      j=0;
      aCSize = (long) anIndividual.getSize();
      aC     = ((BitSet)anIndividual.getChromosome());
      aClauseTokenizer = new StringTokenizer(Clauses,";.");
      //      anIndividual.acceptVisitor(new DisplayVisitor());
      while(aClauseTokenizer.hasMoreTokens())
	{
	  aClause = aClauseTokenizer.nextToken();
	  //  System.out.print(aClause+"="+evaluate(aClause,aC));
	  if (evaluate(aClause,aC)) j++;

	 
	}
      //      System.out.println();


      return (double)j;
    }
  

  private boolean evaluate(String aClause, BitSet aTruthAssigment)
    {
     StringTokenizer aVariableTokenizer;
     int    aVariableNumber;
     boolean value = false;
     boolean bit;

     
     aVariableTokenizer = new StringTokenizer(aClause,"(,)");
     while(aVariableTokenizer.hasMoreTokens())
       {
	aVariableNumber = Integer.parseInt(aVariableTokenizer.nextToken());
	bit = aTruthAssigment.get(Math.abs(aVariableNumber)-1); /* BitSet bit starts from zero */


	if (aVariableNumber <0)
	  {
	    value = value || (!bit);
	  }
	else
	  {
	    value = value || bit;
	  }

       
       }
     return value;
    }


  public boolean feasibilityCheck(Individual anIndividual)
    {
      return true;
    };

  public Object newSolution()
    {
     Object aChromosome;
     long j;

     aChromosome  = new BitSet();
    
        for (j=0;j<size;j++)
	  {
	     if( MAFRA_Random.nextUniformDouble(0.0,1.0)>0.5 )
	     {
	      ((BitSet)aChromosome).set((int)j);
	   
	     }
	  }
      return aChromosome;
    }


}
