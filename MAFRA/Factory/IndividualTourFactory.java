
package MAFRA.Factory;

import MAFRA.Population_Individual.*;


/** This factory is used to initialize a population with Individuals whose
Chromosome is a tour.
 @version Memetic Algorithms Framework - V 1.2 - October 1999
 @author  Natalio Krasnogor
 */
public class IndividualTourFactory extends IndividualFactory{

  protected ProblemFactory myProblem;

  public IndividualTourFactory(ProblemFactory myP)
    {
      myProblem = myP;
    }

  public Individual createNewIndividual()
    {
      TourIndividual aI;

      aI = new TourIndividual(myProblem);
      return aI;
    }
}
      
 
