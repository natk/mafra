package MAFRA.Factory;


import java.util.BitSet;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


public class OneMaxProblem extends ProblemFactory{

 public OneMaxProblem(double aSize)
   {
     super(aSize);
   }





 /* This method recomputes the fitness of the individual.*/
  public double fitness(Individual anIndividual)
    {
      long i;
      long j;
      long aCSize;
      Object aC;
      
      j=0;
      aCSize = (long) anIndividual.getSize();
      aC     = anIndividual.getChromosome();
      for(i=0;i<aCSize;i++)
	{
	  if(((BitSet)aC).get((int)i)) j++;
	}

      return (double)j;
    }
  




  public boolean feasibilityCheck(Individual anIndividual)
    {
      return true;
    };

  public Object newSolution()
    {
     Object aChromosome;
     long j;

     aChromosome  = new BitSet();
    
        for (j=0;j<size;j++)
	  {
	     if( MAFRA_Random.nextUniformDouble(0.0,1.0)>0.5 )
	     {
	      ((BitSet)aChromosome).set((int)j);
	   
	     }
	  }
      return aChromosome;
    }


}
