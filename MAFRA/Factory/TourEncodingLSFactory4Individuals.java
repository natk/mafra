

package MAFRA.Factory;

import MAFRA.LocalSearcher.*;
/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourEncodingLSFactory4Individuals extends LocalSearchFactory{


 public  AbstractLocalSearcher createBoltzmannLocalSearcher()
   {
    TourBoltzmannLocalSearcher4Individuals ls;
    ls = new TourBoltzmannLocalSearcher4Individuals();
    return (ls);
   };


    public AbstractLocalSearcher createAnnealingLocalSearcher()
    {
	/* not impemented */
	return (null);
    };


}
