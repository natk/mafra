

package MAFRA.Factory;

import MAFRA.LocalSearcher.*;



/** LocalSearchFactory is an abstract class meant to be the superclass of concrete local search factories. 
    A LocalSearchOperator instance will receive a LocalSearchFactory as an argument for its
    constructor. At runtime the _actual_ parameter will be any of LocalSearchFactory subclasses
    which are not abstract and hence implements a set of different local searchers for different
    encodings. The AbstractLocalSearch class is used just as a memebership class (superclass) of
    the objects (concrete local searchers for a concrete encoding) returned by createXXXX operations.
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public abstract class LocalSearchFactory extends Factory{

 public  abstract AbstractLocalSearcher createBoltzmannLocalSearcher();
 public  abstract AbstractLocalSearcher createAnnealingLocalSearcher();
   




}


