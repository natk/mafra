

package MAFRA.Factory;

import MAFRA.Population_Individual.*;



/** This factory is used to initialize a population with the apropriate kind
    of Individuals by subclassifying this one.
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public abstract class IndividualFactory extends Factory{

 public abstract Individual createNewIndividual();
}
