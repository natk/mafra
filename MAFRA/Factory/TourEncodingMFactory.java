

package MAFRA.Factory;

import MAFRA.Mutation.*;

/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourEncodingMFactory extends MutationFactory{


 public AbstractMutation createOnePointMutationEdgeEncoding()
 {
  EdgeEncodingOnePointMutation EEoPM;
  EEoPM = new EdgeEncodingOnePointMutation();
  return (EEoPM);
 }
 
 public  AbstractMutation createOnePointMutation()
   {
    TourOnePointMutation oPM;
    oPM = new TourOnePointMutation();
    return (oPM);
   };

   public  AbstractMutation createTwoPointMutation()
   {
    TourTwoPointMutation tPM;
    tPM = new TourTwoPointMutation();
    return(tPM);
   };

 public  AbstractMutation createThreePointMutation()
   {
    TourThreePointMutation thPM;
    thPM = new TourThreePointMutation();
    return(thPM);
   };

 public  AbstractMutation createFourPointMutation()
   {
    TourFourPointMutation fPM;
    fPM = new TourFourPointMutation();
    return(fPM);
   };
   


}
