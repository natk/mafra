

package MAFRA.Factory;

import MAFRA.Mutation.*;

/** @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */
public class BitSetEncodingMFactory extends MutationFactory{


 public  AbstractMutation createOnePointMutation()
   {
    BitSetOnePointMutation oPM;
    oPM = new BitSetOnePointMutation();
    return (oPM);
   };

   public  AbstractMutation createTwoPointMutation()
   {
    BitSetTwoPointMutation tPM;
    tPM = new BitSetTwoPointMutation();
    return(tPM);
   };

 public  AbstractMutation createThreePointMutation()
   {
    BitSetThreePointMutation thPM;
    thPM = new BitSetThreePointMutation();
    return(thPM);
   };

 public  AbstractMutation createFourPointMutation()
   {
    BitSetFourPointMutation fPM;
    fPM = new BitSetFourPointMutation();
    return(fPM);
   };
   


}
