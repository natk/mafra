



package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;


/** @version Memetic Algorithms Framework - V 1.0 - September 1999
    @author  Natalio Krasnogor
    */
public class TourBoltzmannLocalSearcherEdgeEncoding extends AbstractTourLocalSearcher{





 public Individual individualLocalSearch(Individual parent1, double temperature,double distances[][],FitnessVisitor aFitnessVisitor)
 
    {


      long    rndMut; 
      double  previousFitness;
      double  newFitness;
    
      // double K=0.01;/* this was the original for steady state;*/
     
       double  K = 0.0000461; /*for generational */

      double  threshold;
      double  deltaE;
      double  rndNumber;
      int     numberChanges;
      int     numberConsecutiveChanges;
      int     i;
      int     j;
      TourIndividual tmpIndi;       
      int inversionSize;
      double size;



      tmpIndi         = (TourIndividual)(parent1.copyIndividual());
      size            = tmpIndi.getSize();
      previousFitness = tmpIndi.getFitness();
     


     // numberChanges =(int) MAFRA_Random.nextUniformLong(1,(int)(tmpIndi.getSize()*0.03));
     numberChanges = 1 ; //M2912-1
    // numberConsecutiveChanges = (int) ((tmpIndi.getSize()*0.03)/numberChanges);
     numberConsecutiveChanges = (int)Math.ceil(tmpIndi.getSize()*0.01); //M2912-1
      //    System.out.println("#C="+numberChanges+", #CC"+numberConsecutiveChanges);
      //  numberChanges =(int)tmpIndi.getSize()-1;
      // numberChanges = (int)Math.ceil(size/inversionSize)-1;
      //inversionSize = (int)(MAFRA_Random.nextUniformLong(2,(int)(size)-2));
      //numberChanges = (int)(size-inversionSize)-1;
      for (i=0;i<numberChanges;i++)
	{
	  for(j=0;j<numberConsecutiveChanges;j++)
	    {
	    /* we perform an inversion like in EdgeEncodingOnePointMutation */
	       twoChange(tmpIndi); 
	    }
	  tmpIndi.acceptVisitor(aFitnessVisitor);
	  newFitness = tmpIndi.getFitness();
	  /* Boltzman acceptance */
	  if (Math.abs(previousFitness)<=Math.abs(newFitness))
	    {
	      deltaE    =  Math.abs(newFitness) - Math.abs(previousFitness) ;
	      threshold = Math.exp(-K*deltaE/temperature); 
	     
	      rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
	      //  System.out.println("deltaE="+deltaE+",threshold="+threshold+", temperature="+temperature+", rndNumber="+rndNumber);
	      if(rndNumber<threshold)
		{
		  parent1.setChromosome((int[])(tmpIndi.getChromosome()));
		  parent1.setFitness(newFitness);
		  // System.out.println("Accepting worst");
		  previousFitness = newFitness;
		}
	      else
		{
		  //  System.out.println("Rejecting worst");
		  tmpIndi.setChromosome((int[])parent1.getChromosome());
		  tmpIndi.setFitness(parent1.getFitness());
		  previousFitness = parent1.getFitness();
		}
	      
	    }
	  else
	    { /* accept configuration because it is better */
	      parent1.setChromosome((int [])(tmpIndi.getChromosome()));
	      parent1.setFitness(newFitness);
	      //  System.out.println("Accepting Better");
	      previousFitness = newFitness;
	    }
	}

      return parent1;
      
    }



 // does not implement
 public  Individual individualLocalSearch(Individual parent1, double temperature, long distances[][],FitnessVisitor aFitnessVisitor)
 {
  return null;
 }

 // does not implement
 public Individual individualLocalSearch(Individual parent1, double temperature, double K, double distances[][],FitnessVisitor aFitnessVisitor)
 {
  return null;
 }


/* Copied from EdgeEncodingOnePointMutation */


 private void twoChange(Individual parent1)
 
    {
      int  c1[];
      int  c2[];
      int  rndPoint1;
      int  rndPoint2;
      int  size; 
      boolean allDifferent;
       
      allDifferent = false;
      c1           = (int[])(parent1.getChromosome());
      size         = (int)(parent1.getSize());
      c2           = new int[size];
      do
      {
       rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       
       allDifferent = (rndPoint1!=rndPoint2);
	 
      }while(!allDifferent);
      
      c2 = edgeEncoding2PermEncoding(c1,size);
      invertion(c2,rndPoint1,rndPoint2);
      c1 = permEncoding2EdgeEncoding(c2,size);


      parent1.setChromosome(c1);
      ((TourIndividual)parent1).setTemperature(0.0);
      ((TourIndividual)parent1).setNumberGenerationsWithoutChange(0);


           
    }

 private int[] edgeEncoding2PermEncoding(int c[],int size)
 {
  int i;
  int c2[];
  int s;
  int e;
  
  c2 = new int[size];
  
  s=0;
  e=c[s];
  for(i=0;i<size;i++)
  {
   c2[s]=e;
   s=e;
   e=c[s];
  }
  return c2;
 }

 private int[] permEncoding2EdgeEncoding(int aTour[], int size)
 {
  int i;
  int finalTour[];
  
  finalTour = new int[size];
  
  for(i=0;i<(size-1);i++)
  {
   finalTour[aTour[i]]=aTour[i+1];
  }
  finalTour[aTour[(int)size-1]]=aTour[0];
  return finalTour;    
 }


 private int[] invertion(int c[],int start,int end)
 
    {
      int   tmp; 
      int   i;
      int   l;
      
      
      if(end<start)
	{
	  tmp   = start;
	  start = end;
	  end   = tmp;
	}
      l = (int)((end-start)/2);	
     
      for(i=0;i<=l;i++)
	{
	  tmp         = c[start+i];
	  c[start+i]  = c[end-i];
	  c[end-i]    = tmp;
	}

      return c;
      
    }



 public Individual individualLocalSearch(Individual parent1, double temperature, double sigma, double popSize,FitnessVisitor aFitnessVisitor)
{
 return null;
}










}


