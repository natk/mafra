


package MAFRA.LocalSearcher;

import java.util.BitSet;
import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;


/** @version Memetic Algorithms Framework - V 1.0 - September 1999
    @author  Natalio Krasnogor
    */
public class BitSetBoltzmannLocalSearcher extends AbstractBitSetLocalSearcher{


 public Individual individualLocalSearch(Individual parent1, double temperature, FitnessVisitor myFitnessVisitor)
 
    {
      BitSet c1;
      char temp[];
      long   rndMut; 
      double  previousFitness;
      double  newFitness;
      BitSetIndividual tmpIndi;
      double  K=0.1;
      double threshold;
      double deltaE;
      double rndNumber;
       



      tmpIndi         = (BitSetIndividual)(parent1.copyIndividual());
      previousFitness = tmpIndi.getFitness();
      maxMin2Flips(tmpIndi,myFitnessVisitor);
      newFitness = tmpIndi.getFitness();

      /* Boltzman acceptance */
      if (previousFitness>=newFitness)
	{
	  deltaE    = previousFitness - newFitness;
	  threshold = Math.exp(-K*deltaE/temperature);
	  rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
	  System.out.println("deltaE="+deltaE+",threshold="+threshold+", temperature="+temperature+", rndNumber="+rndNumber);
	  if(rndNumber<threshold)
	    {
	      parent1.setChromosome((BitSet)(tmpIndi.getChromosome()));
	      parent1.setFitness(newFitness);
	      System.out.println("Accepting worst");
	    }
	  else
	    {
	      System.out.println("Rejecting worst");
	    }

	}
      else
	{ /* accept configuration because it is better */
	  parent1.setChromosome((BitSet)(tmpIndi.getChromosome()));
	  parent1.setFitness(newFitness);
	  System.out.println("Accepting Better");
	  	}


      return parent1;
      
    }


 private void operate(BitSetIndividual tmpIndi,FitnessVisitor myFitnessVisitor)
   {
      long   i;
      long   iForMaxDelta;
      long   instanceSize;
      double delta;
      double maxDelta;
      double originalFitness;
      BitSet c1;


      instanceSize = (long)(tmpIndi.getSize());
      originalFitness = tmpIndi.getFitness();


      iForMaxDelta    = 0;
      maxDelta        = Double.NEGATIVE_INFINITY;
      delta           = Double.POSITIVE_INFINITY;
      c1              = (BitSet)tmpIndi.getChromosome();
      for (i=0;i<instanceSize;i++)
      	{


	  if(c1.get((int)i))
	    {
	      c1.clear((int)i);
	    }
	  else
	    {
	      c1.set((int)i);
	    }
	  tmpIndi.setChromosome(c1);
	  tmpIndi.acceptVisitor(myFitnessVisitor);
	  delta = Math.abs(originalFitness-tmpIndi.getFitness());
	  if (delta>maxDelta)
	    {
	      iForMaxDelta = i;
	      maxDelta     = delta;
	    }

	  if(c1.get((int)i))
	    {
	      c1.clear((int)i);
	    }
	  else
	    {
	      c1.set((int)i);
	    }

	}

      if(c1.get((int)iForMaxDelta))
	{
	  c1.clear((int)iForMaxDelta);
	}
      else
	{
	  c1.set((int)iForMaxDelta);
	}
      tmpIndi.setChromosome(c1);
      tmpIndi.acceptVisitor(myFitnessVisitor);
      
   }



 private void maxMin2Flips(BitSetIndividual tmpIndi,FitnessVisitor myFitnessVisitor)
   {
      long   i;
      long   j;
      long   jToTry;
      long   maximizingI;
      long   instanceSize;
      long   howManyToTry;

      double maximumMinimum;
      double actualMinimum;
      double originalFitness;
      BitSet c1;


      instanceSize = (long)(tmpIndi.getSize());
      originalFitness = tmpIndi.getFitness();



      maximumMinimum  = Double.NEGATIVE_INFINITY;
      maximizingI     = 0;
      c1              = (BitSet)tmpIndi.getChromosome();

      for (i=0;i<instanceSize;i++)
      	{
	  complementBitInBitSet(c1,(int)i);

	  /* We search for the 2nd bit change that minimizes the fitness */
	  actualMinimum = Double.POSITIVE_INFINITY;
	  howManyToTry = MAFRA_Random.nextUniformLong(0,instanceSize);
	  for (j=0;j<howManyToTry;j++)
	    {
	      jToTry = MAFRA_Random.nextUniformLong(0,instanceSize);
	      if (jToTry!=i)
		{
		  complementBitInBitSet(c1,(int)jToTry);
		}
	      tmpIndi.setChromosome(c1);
	      tmpIndi.acceptVisitor(myFitnessVisitor);
	      if (tmpIndi.getFitness()<actualMinimum)
		{
		  actualMinimum = tmpIndi.getFitness();
		}
	      if (jToTry!=i)
		{
		  complementBitInBitSet(c1,(int)jToTry);
		}

	    }

	  if (actualMinimum>maximumMinimum)
	    {
	      maximumMinimum = actualMinimum;
	      maximizingI   = i;
	    }
	    complementBitInBitSet(c1,(int)i);
      
	}
      complementBitInBitSet(c1,(int)maximizingI);
      tmpIndi.setChromosome(c1);
      tmpIndi.acceptVisitor(myFitnessVisitor);
	

   }


private void complementBitInBitSet(BitSet c, int i)
  {
		  if(c.get((int)i))
		    {
		      c.clear((int)i);
		    }
		  else
		    {
		      c.set((int)i);
		    }
  }

}
