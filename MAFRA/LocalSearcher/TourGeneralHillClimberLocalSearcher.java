


package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;
import MAFRA.Factory.*;


/** @version Memetic Algorithms Framework - V 1.2 - August 2000
    @author  Natalio Krasnogor
    
    
    This class implements a general hill climber local searcher.
    At creation it receives:
    
    1) a boolean, SinOpt, such that if SinOpt=true then the algorithm will be
       a move-Opt, that is, it will try to apply move until no improvement can be found.
       If SinOpt=false, only a specified number of applications of move will be done.
    2) an int, blindFirstBest, such that if blindFirstBest=0 then a blind (random) application
       of Move will occurr. If blindFirstBest=1 the first improvement will
       be accepted, otherwise (blindFirstBest=2) then the best improvement will be accepted.
    3) an int, NumIt, that specifies the number of iterations. This is valid only when
       SinOpt = false.
    4) an intr, Move, that specifies the move to be applied :
       
      1: 2-exchange.   (complete)
      2: 3-exchange.   (=1-insertion, this is a sub neighborhood of the complete 3-exchange)
      3: 4-exchange.   (=2-swap, this is a sub neighborhood of the complete 4-exchange).
      4: Or-insertion. (when used with SinOpt=true this neighborhhod is included in that of 3-opt (3-echange
                       with SinOpt=true)).
      5: 3-echange.    (complete)
     5) a boolean, HcBhc, such that if HcBhc=false then the points 1,2,3 and 4 above are performed
        in a strictly HillClimber manner. If, however, HcBhc=true then 1,2,3 and 4 are interpreted
        and performed as a BoltzmannHillclimber.
    */
public class TourGeneralHillClimberLocalSearcher extends AbstractTourLocalSearcher{

 private boolean sinOpt;
 private int blindFirstBest;
 private int numIt;
 private int move;
 private boolean hcBhc;
 private ProblemFactory myProblem;
 private FitnessVisitor myFitnessVisitor;
 private double temperature;
 private double K;
 private double errorThreshold;
 private int forbidTable[][];
 private int fPos;

 public TourGeneralHillClimberLocalSearcher(boolean SinOpt,boolean HcBhc, int BlindFirstBest, int NumIt, int Move, ProblemFactory aProblemFactory)
 {
  sinOpt = SinOpt;
  hcBhc  = HcBhc;
  blindFirstBest = BlindFirstBest;
  numIt     = NumIt;
  move      = Move; 
  myProblem  = aProblemFactory; 
  myFitnessVisitor = new FitnessVisitor(myProblem);
  forbidTable = new int[2][NumIt];
  fPos = 0;
 }

 

 public Individual individualLocalSearch(Individual parent1)
 {
  if (parent1==null) 
  {
   System.out.println("ERROR - individual in local searcher TourGeneralHillClimberLocalSearcher is null");
   System.exit(0);
  }
  
  
  ((SelfAdaptingStaticTourIndividual)parent1).setTemperature(temperature);
  if(sinOpt)
  {/* perform an Opt-move search*/
   /* to be done later */
  }
  else
  {/* perform an interative search with numIt iterations */
   if(hcBhc)
   { /*Boltzmann Hill climber*/
    switch(move)
    {
     case 1:
      	    {
      
	     twoExchangeBoltzmann(blindFirstBest,numIt,parent1);
	     break;
	     }
     case 2:
      	    {
	     threeExchangeBoltzmann(blindFirstBest,numIt,parent1);
	     break;
	    }
  
     case 3:
      	    {
	     fourExchangeBoltzmann(blindFirstBest,numIt,parent1);
	     break;
	    }
     case 4:
      	    {
	    
	     break;
	    }	
     case 5:
      	    {
	    
	     break;
	    }
    }
   }
   else
   {/* Normal Hill climber */
    switch(move)
    {
     case 1:
      	    {
	     twoExchange(blindFirstBest,numIt,parent1);
	   
	     break;
	     }
     case 2:
      	    {
	     threeExchange(blindFirstBest,numIt,parent1);
	     break;
	    }
  
     case 3:
      	    {
	     fourExchange(blindFirstBest,numIt,parent1);
	     break;
	    }
     case 4:
      	    {
	     orInsertion(blindFirstBest,numIt,parent1);
	     break;
	    }	
     case 5:
      	    {
	     threeExchangeComplete(blindFirstBest,numIt,parent1);
	     break;
	    }
   }
   }     	    
  
  }
  return(parent1);
  
 }
 
 
 // does not implement
 public  Individual individualLocalSearch(Individual parent1, double temperature, long distances[][],FitnessVisitor aFitnessVisitor)
 {
  return null;
 }
 
 

/* ############################################### */
/* Definition of twoExchange and auxiliary methods */
/* ############################################### */
private	void twoExchange(int blindFirstBest,int numIt, Individual indi)
{
     
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];

 
 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

  
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
  if(tourChanged)
  {
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
        for(at=0;at<size;at++)
        {
         for(longitud=1;longitud<=((size/2)+1);longitud++)
         {
	  newDelta      = computeTwoExchangeDelta(indi,at,longitud);

          if((newDelta>previousDelta)&&(newDelta>0))
          {/* an improvement was found */
           betterAt       = at;
           betterLongitud = longitud;
           previousDelta  = newDelta;
           tourChanged    = true;
	   	   
          }
         }
        }
        if(tourChanged)
        {

         twoExchangeAtSize(indi,betterAt,betterLongitud); 
	// System.out.println("Delta="+previousDelta+", AT="+betterAt+", Lon="+betterLongitud);
	 previousDelta = - Double.MAX_VALUE; 
        }
	break;
       }
    case 1:
       {/* First improvement = next ascend */
      	improvementFound = false;
      	at               = 0;
      	tourChanged    = false;
      	scramble(scrambleVector,size);
      	while( (!improvementFound) && (at<size ))
      	{
      	  longitud = 1;
          while( (!improvementFound) && (longitud<=((size/2)+1) ) )
	  {
      	    improvementFound = false;
      	    newDelta      = computeTwoExchangeDelta(indi,scrambleVector[at],longitud);
      	    if(newDelta>0)
      	    {/* an improvement was found */
       	      improvementFound = true;
       	      betterAt         = scrambleVector[at];
       	      betterLongitud   = longitud;
      	    }
      	    longitud = longitud + 1;
      	  }
      	  at = at+1;
      	}
      	if (improvementFound)
      	{
      	  twoExchangeAtSize(indi,betterAt,betterLongitud);
      	  tourChanged      = true;
      	}
	break;
       }
    case 0:
       {/*blind*/
        twoExchangeAtSize(indi,((int)MAFRA_Random.nextUniformLong(0,size)),((int)MAFRA_Random.nextUniformLong(0,size)));
        tourChanged = true;
        break;
       }
   }
  }
 }/* number of iterations */

}

private double computeTwoExchangeDelta(Individual indi, int at, int longitud)
{
  int c[];
  int startInversion;
  int endInversion;
 
  int size;
  int c1,c2,c3,c4;
  double originalLength = 0.0;
  double newLength      = 0.0;
  
    size           = (int)indi.getSize();
    c              = (int[])indi.getChromosome();
    startInversion = at;
    endInversion   = at+longitud;
    
    
    
  
    c1 = c[mod(startInversion-1,size)];
    c2 = c[mod(startInversion,size)];
    c3 = c[mod(endInversion,size)];
    c4 = c[mod(endInversion+1,size)];
      
    
    originalLength = ((TSPProblem)myProblem).getDistance(c1,c2)+((TSPProblem)myProblem).getDistance(c3,c4);
    newLength      = ((TSPProblem)myProblem).getDistance(c1,c3)+((TSPProblem)myProblem).getDistance(c2,c4);
     
    return (originalLength-newLength);
 
 
 
}



private  void twoExchangeAtSize(Individual indi,int at, int longitud)
 {

  int c[];
  int startInversion;
  int endInversion;
  int i;
  int maxI;
  int tmp;
  int size;
  int tmpC[];
  int pos;
    

    size           = (int)indi.getSize();
    c              = (int[])indi.getChromosome();
    startInversion = at;
    endInversion   = at+longitud;
  
     tmpC = new int[longitud+1];
     pos  = 0;
     for(i=endInversion;i>=startInversion;i--)
     {
      tmpC[pos]=c[mod(i,size)];
      pos++;
     }
     pos = 0;
     for(i=startInversion;i<=endInversion;i++)
     {
      c[mod(i,size)]=tmpC[pos];
      pos++;
     }

     indi.setChromosome(c);
 }   
    
    



/* ######################################################### */
/* Definition of threeExchangeComplete and auxiliary methods */
/* ######################################################### */
private	void threeExchangeComplete(int blindFirstBest,int numIt, Individual indi)
{
     
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 int at2                  = 0;
 int betterAt2		  = 0;
 int longitud2		  = 0;
 int betterLongitud2      = 0;
 int betterDirection      = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];

 
 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

  
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
  if(tourChanged)
  {
   switch(blindFirstBest)
   {
    case 2:
      {/* Best improvement = steepest ascend */
      	tourChanged   = false;
      	previousDelta = - Double.MAX_VALUE;
      	for(at=0;at<size;at++)
      	{
      	 for(longitud=3;longitud<=((size/2)+1);longitud++)
      	 {
      	  /* Case 1: Left Sticky end outside the previous 2-exchange */
      	  at2 = at;
      	  for(longitud2=1;longitud2<=(size-longitud-2);longitud2++)
      	  {
       	    newDelta      = computeThreeExchangeCompleteDelta(indi,at,longitud,at2,longitud2,1);
       	    if(newDelta>previousDelta)
       	    {/* an improvement was found */
              betterAt        = at;
              betterLongitud  = longitud;
	      betterAt2       = at2;
	      betterLongitud2 = longitud2;
              previousDelta   = newDelta;
              tourChanged     = true;
	      betterDirection       = 1;
       	    }
      	  }
      	  /* Case 2: Left Sticky end inside the previous 2-exchange */
      	  at2 = at;
      	  for(longitud2=1;longitud2<=(longitud-1);longitud2++)
      	  {
       	    newDelta      = computeThreeExchangeCompleteDelta(indi,at,longitud,at2,longitud2,2);
       	    if(newDelta>previousDelta)
       	    {/* an improvement was found */
              betterAt        = at;
              betterLongitud  = longitud;
	      betterAt2       = at2;
	      betterLongitud2 = longitud2;
              previousDelta   = newDelta;
              tourChanged     = true;
	      betterDirection       = 2;
       	    }
      	  }
      	  /* Case 3: Right Sticky end inside the previous 2-exchange */
      	  at2 = mod(at+longitud,size);
      	  for(longitud2=1;longitud2<=(longitud-1);longitud2++)
      	  {
       	    newDelta      = computeThreeExchangeCompleteDelta(indi,at,longitud,at2,longitud2,3);
       	    if(newDelta>previousDelta)
       	    {/* an improvement was found */
              betterAt        = at;
              betterLongitud  = longitud;
	      betterAt2       = at2;
	      betterLongitud2 = longitud2;
              previousDelta   = newDelta;
              tourChanged     = true;
              betterDirection       = 3;
	    }
      	  }
      	  /* Case 4: Right Sticky end outside the previous 2-exchange */
      	  at2 =mod( at+longitud,size);
      	  for(longitud2=1;longitud2<=(size-longitud-2);longitud2++)
      	  {
	    newDelta      = computeThreeExchangeCompleteDelta(indi,at,longitud,at2,longitud2,4);
       	    if(newDelta>previousDelta)
       	    {/* an improvement was found */
              betterAt        = at;
              betterLongitud  = longitud;
	      betterAt2       = at2;
	      betterLongitud2 = longitud2;
              previousDelta   = newDelta;
              tourChanged     = true;
	      betterDirection       = 4;
       	    }
      	  }
      
      	 }
        }
        if(tourChanged)
        {
         threeExchangeAtSizeAtSizeDirection(indi,betterAt,betterLongitud,betterAt2,betterLongitud2,betterDirection);  
        }
	break;
      }
    case 1:
    {/* First improvement = next ascend */
     improvementFound = false;
     at               = 0;
     tourChanged    = false;
     scramble(scrambleVector,size);
     while( (!improvementFound) && (at<size ))
     {
      longitud = 3;
      while( (!improvementFound) && (longitud<=((size/2)+1)) )
      {
       /* Case 1: Left Sticky end outside the previous 2-exchange */
       at2       = scrambleVector[at];
       longitud2 = 1;
       while((!improvementFound) && (longitud2<=(size-longitud-2)) )
       {
        newDelta      = computeThreeExchangeCompleteDelta(indi,scrambleVector[at],longitud,at2,longitud2,1);
        if(newDelta>0)
        {/* an improvement was found */
         betterAt         = scrambleVector[at];
         betterLongitud   = longitud;
   	 betterAt2        = at2;
	 betterLongitud2  = longitud2;
         improvementFound = true;
         tourChanged      = true;
	 betterDirection       = 1;
        }
        longitud2++;
       }
       /* Case 2: Left Sticky end inside the previous 2-exchange */
       at2       = scrambleVector[at];
       longitud2 = 1;
       while( (!improvementFound) && (longitud2<=(longitud-1)) )
       {
        newDelta      = computeThreeExchangeCompleteDelta(indi,scrambleVector[at],longitud,at2,longitud2,2);
        if(newDelta>0)
        {/* an improvement was found */
         betterAt         = scrambleVector[at];
         betterLongitud   = longitud;
	 betterAt2        = at2;
	 betterLongitud2  = longitud2;
         improvementFound = true;
         tourChanged      = true;
	 betterDirection       = 2;
        }
        longitud2++;
       }
       /* Case 3: Right Sticky end inside the previous 2-exchange */
       at2       = mod(scrambleVector[at]+longitud,size);
       longitud2 = 1;
       while( (!improvementFound) && (longitud2<=(longitud-1)) )
       {
        newDelta      = computeThreeExchangeCompleteDelta(indi,scrambleVector[at],longitud,at2,longitud2,3);
        if(newDelta>0)
        {/* an improvement was found */
         betterAt         = scrambleVector[at];
         betterLongitud   = longitud;
  	 betterAt2        = at2;
	 betterLongitud2  = longitud2;
         improvementFound = true;
         tourChanged      = true;
	 betterDirection       = 3;
        }
        longitud2++;
       }
       /* Case 4: Right Sticky end outside the previous 2-exchange */
       at2       =mod( scrambleVector[at]+longitud,size);
       longitud2 = 1;
       while( (!improvementFound) && (longitud2<=(size-longitud-2)) )
       {
        newDelta      = computeThreeExchangeCompleteDelta(indi,scrambleVector[at],longitud,at2,longitud2,4);
        if(newDelta>0)
        {/* an improvement was found */
         betterAt         = scrambleVector[at];
         betterLongitud   = longitud;
	 betterAt2        = at2;
	 betterLongitud2  = longitud2;
      	 improvementFound = true;
         tourChanged      = true;
	 betterDirection       = 4;
        }
        longitud2++;
       }
       longitud = longitud + 1;
      }
      at = at+1;
     }
     if (improvementFound)
     {
      threeExchangeAtSizeAtSizeDirection(indi,betterAt,betterLongitud,betterAt2,betterLongitud2,betterDirection);
      tourChanged      = true;
     }
     break;
    }
   case 0:
   {/* blind */
    tourChanged = false;
    break;
   } 
  }
 }
 }/* number of iterations */

}



double computeThreeExchangeCompleteDelta(Individual indi,int at,int longitud,int  at2,int longitud2, int direction)
{
  int c[];
  int startInversion1;
  int endInversion1;
  int startInversion2;
  int endInversion2;
 
  int size;
  int c1,c2,c3,c4,c5,c6,c7,c8;
  double originalLength = 0.0;
  double newLength      = 0.0;
  
    size            = (int)indi.getSize();
    c               = (int[])indi.getChromosome();
        
    switch(direction)
    {
      case 1:
      	{
	 c1 = c[mod(at-longitud2-1,size)];
         c2 = c[mod(at-longitud2,size)];
         c3 = c[at];
         c4 = c[mod(at-1,size)];
         c5 = c[mod(at+longitud,size)];
	 c6 = c[mod(at+longitud+1,size)];
	 
	 originalLength =  ((TSPProblem)myProblem).getDistance(c1,c2)+ ((TSPProblem)myProblem).getDistance(c3,c4)+ ((TSPProblem)myProblem).getDistance(c5,c6);
	 newLength      =  ((TSPProblem)myProblem).getDistance(c3,c1)+ ((TSPProblem)myProblem).getDistance(c5,c2)+ ((TSPProblem)myProblem).getDistance(c3,c6);
	 break;
	}
      case 2:
      	{
	 c1 = c[mod(at-1,size)];
         c2 = c[mod(at,size)];
         c3 = c[mod(at2+longitud2,size)];
         c4 = c[mod(at2+longitud2+1,size)];
         c5 = c[mod(at+longitud,size)];
	 c6 = c[mod(at+longitud+1,size)];
	 c7 = c[mod(at+longitud-longitud2,size)];
	 c8 = c[mod(at+longitud-longitud2-1,size)];
	 
	 originalLength =  ((TSPProblem)myProblem).getDistance(c1,c2)+ ((TSPProblem)myProblem).getDistance(c3,c4)+ ((TSPProblem)myProblem).getDistance(c5,c6);
	 newLength      =  ((TSPProblem)myProblem).getDistance(c1,c7)+ ((TSPProblem)myProblem).getDistance(c5,c8)+ ((TSPProblem)myProblem).getDistance(c2,c6);
	 break;
	}
      case 3:
      	{
      	 c1 = c[mod(at-1,size)];
         c2 = c[mod(at,size)];
         c3 = c[mod(at+longitud2,size)];
         c5 = c[mod(at+longitud,size)];
         c6 = c[mod(at+longitud+1,size)];
         c7 = c[mod(at+longitud-longitud2,size)];
         c8 = c[mod(at+longitud-longitud2-1,size)];
	 
	 originalLength =  ((TSPProblem)myProblem).getDistance(c1,c2)+ ((TSPProblem)myProblem).getDistance(c5,c6)+ ((TSPProblem)myProblem).getDistance(c7,c8);
	 newLength      =  ((TSPProblem)myProblem).getDistance(c1,c5)+ ((TSPProblem)myProblem).getDistance(c3,c6)+ ((TSPProblem)myProblem).getDistance(c7,c2);
	 break;
	}
      case 4:
      	{
	 c1 = c[mod(at-1,size)];
         c2 = c[mod(at,size)];
         c3 = c[mod(at2+longitud2,size)];
         c4 = c[mod(at2+longitud2+1,size)];
         c5 = c[mod(at+longitud,size)];
	 c6 = c[mod(at+longitud+1,size)];
	 c7 = c[mod(at+longitud+longitud2,size)];
	 c8 = c[mod(at+longitud+longitud2+1,size)];
	 
	 originalLength =  ((TSPProblem)myProblem).getDistance(c1,c2)+ ((TSPProblem)myProblem).getDistance(c5,c6)+ ((TSPProblem)myProblem).getDistance(c7,c8);
	 newLength      =  ((TSPProblem)myProblem).getDistance(c1,c5)+ ((TSPProblem)myProblem).getDistance(c5,c7)+ ((TSPProblem)myProblem).getDistance(c6,c8);

	 break;
	}
    }
    
    return (originalLength-newLength);
 
  
}



void threeExchangeAtSizeAtSizeDirection(Individual indi,int betterAt,int betterLongitud,int betterAt2,int betterLongitud2, int direction)
{

 int size;
 
 
 size = ((int)indi.getSize());
 switch(direction)
 {
  case 1:
  { /* Case 1: Left Sticky end outside the previous 2-exchange */
   twoExchangeAtSize(indi,betterAt,betterLongitud);
   twoExchangeAtSize(indi,mod(betterAt2-betterLongitud2,size),betterLongitud2-1);
   break;
  }
  case 2:
  { /* Case 2: Left Sticky end inside the previous 2-exchange */
   twoExchangeAtSize(indi,betterAt,betterLongitud);
   twoExchangeAtSize(indi,betterAt2,betterLongitud2);
   break;
  }
  case 3:
  { /* Case 3: Right Sticky end inside the previous 2-exchange */
   twoExchangeAtSize(indi,betterAt,betterLongitud);
   twoExchangeAtSize(indi,mod(betterAt2-betterLongitud2,size),betterLongitud2);
   break;
  }
  case 4:
  { /* Case 4: Right Sticky end outside the previous 2-exchange */
   twoExchangeAtSize(indi,betterAt,betterLongitud);
   twoExchangeAtSize(indi,mod(betterAt2+1,size),betterLongitud2-1);
   break;
  }
 }
 

 
}




/* ################################################# */
/* Definition of threeExchange and auxiliary methods */
/* ################################################# */
private	void threeExchange(int blindFirstBest,int numIt, Individual indi)
{
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];

 

 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

  
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
  if(tourChanged)
  {
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
        for(at=0;at<size;at++)
        {
         for(longitud=0;longitud<size;longitud++)
         {
	  if((at!=longitud)&&(mod(at-1,size)!=longitud)&&(mod(at+1,size)!=longitud))
	  {
	   newDelta      = computeThreeExchangeDelta(indi,at,longitud);
           if((newDelta>previousDelta)&&(newDelta>0))
           {/* an improvement was found */
            betterAt       = at;
            betterLongitud = longitud;
            previousDelta  = newDelta;
            tourChanged    = true;
           }
	  }
	 }
        }
        if(tourChanged)
        {
         threeExchangeAtSize(indi,betterAt,betterLongitud);  
        }
	break;
       }
    case 1:
       {/* First improvement = next ascend */
      	improvementFound = false;
      	at               = 0;
      	tourChanged    = false;
      	scramble(scrambleVector,size);
      	while( (!improvementFound) && (at<size) )
      	{
      	  longitud = 0;
      	  while( (!improvementFound) && (longitud<size) )  
      	  {
      	    improvementFound = false;
	    if((scrambleVector[at]!=longitud)&&(mod(scrambleVector[at]-1,size)!=longitud)&&(mod(scrambleVector[at]+1,size)!=longitud))
	    {
      	     newDelta      = computeThreeExchangeDelta(indi,scrambleVector[at],longitud);
      	     if(newDelta>0)
      	     {/* an improvement was found */
       	       improvementFound = true;
       	       betterAt         = scrambleVector[at];
       	       betterLongitud   = longitud;
      	     }
	    }
      	    longitud = longitud + 1;
      	  }
      	  at = at+1;
	 }
        if (improvementFound)
      	{
      	  threeExchangeAtSize(indi,betterAt,betterLongitud);
      	  tourChanged      = true;
      	}
	break;
       }
    case 0:
       {/*blind*/
        do
	{
      	 at =((int)MAFRA_Random.nextUniformLong(0,size));
	 longitud =((int)MAFRA_Random.nextUniformLong(0,size));
	} while((at==longitud)||(mod(at-1,size)==longitud)||(mod(at+1,size)==longitud));
	threeExchangeAtSize(indi,at,longitud);
        tourChanged = true;
        break;
       }
   }
  }
 }/* number of iterations */

}


double computeThreeExchangeDelta(Individual indi,int at,int at2)
{
 int c[];
 int size;
 int c1,c2,c3,c4,c5,c6;
 double originalLength = 0.0;
 double newLength      = 0.0;
  
 size            = (int)indi.getSize();
 c               = (int[])indi.getChromosome();
 

 c1 = c[mod(at,size)];
 c2 = c[mod(at2,size)];
 c3 = c[mod(at2+1,size)];
 c4 = c[mod(at+1,size)];
 c5 = c[mod(at-1,size)];
 
 
 
  originalLength =  ((TSPProblem)myProblem).getDistance(c5,c1) + ((TSPProblem)myProblem).getDistance(c1,c4) + ((TSPProblem)myProblem).getDistance(c2,c3) ;
  newLength      =  ((TSPProblem)myProblem).getDistance(c4,c5) + ((TSPProblem)myProblem).getDistance(c2,c1) + ((TSPProblem)myProblem).getDistance(c1,c3);



 return(originalLength-newLength);
 
}





private  void threeExchangeAtSize(Individual indi,int at, int at2)
 {

  int c[];
  int tmp;
  int cToMove;
  int i;
  int from,to,downto;
  int size;
  
    c         = (int[])indi.getChromosome();
    size      = (int)indi.getSize();
    
    
    if(at<at2)
    {
     from      = at;
     to        = at2;
     cToMove   = c[mod(from,size)];
     for(i=from;i<to;i++)
     {
      c[mod(i,size)] = c[mod(i+1,size)];
     }
     c[mod(to,size)]=cToMove;
    }
    else
    {
     from      = at;
     downto        = at2;
     cToMove   = c[mod(from,size)];
     for(i=from;i>(downto+1);i--)
     {
      c[mod(i,size)] = c[mod(i-1,size)];
     }
     c[mod(downto+1,size)]=cToMove;
    } 
     
     
    indi.setChromosome(c);
 }   
    
    





/* ############################################### */
/* Definition of fourExchange and auxiliary methods */
/* ############################################### */
private	void fourExchange(int blindFirstBest,int numIt, Individual indi)
{
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];

 
 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

  
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
  if(tourChanged)
  {
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
        for(at=0;at<=size-2;at++)
        {
         for(longitud=at+1;longitud<size;longitud++)
         {
	 /* if((longitud!=mod((at-1),size)) && (longitud!=(at+1)))
	  {*/
           newDelta      = computeFourExchangeDelta(indi,at,longitud);
           if((newDelta>previousDelta)&&(newDelta>0))
           {/* an improvement was found */
            betterAt       = at;
            betterLongitud = longitud;
            previousDelta  = newDelta;
            tourChanged    = true;
           }
	 /* }*/
         }
        }
        if(tourChanged)
        {
         fourExchangeAtSize(indi,betterAt,betterLongitud);  
        }
	break;
       }
    case 1:
       {/* First improvement = next ascend */
      	improvementFound = false;
      	at               = 0;
      	tourChanged    = false;
      	scramble(scrambleVector,size);
      	while( (!improvementFound) && (at<=(size-2) ) )
      	{
      	  longitud = at+1;
      	  while( (!improvementFound) && (longitud<size) ) 
      	  {
      	    improvementFound = false;
	   /* if((longitud!=mod((at-1),size))&&(longitud!=(at+1)))
	    {*/
      	     newDelta      = computeFourExchangeDelta(indi,scrambleVector[at],longitud);
      	     if(newDelta>0)
      	     {/* an improvement was found */
       	       improvementFound = true;
       	       betterAt         = scrambleVector[at];
       	       betterLongitud   = longitud;
      	     }
	   /* }*/
      	    longitud = longitud + 1;
      	  }
      	  at = at+1;
	 }
        if (improvementFound)
      	{
      	  fourExchangeAtSize(indi,betterAt,betterLongitud);
      	  tourChanged      = true;
      	}
	break;
       }
    case 0:
       {/*blind*/
        do
	{
	 at =((int)MAFRA_Random.nextUniformLong(0,size));
	 longitud =((int)MAFRA_Random.nextUniformLong(0,size));
	}while((at==longitud)||(longitud==(at-1))||(longitud==(at+1)));
        fourExchangeAtSize(indi,at,longitud);
        tourChanged = true;
        break;
       }
   }
  }
 }/* number of iterations */

}


double computeFourExchangeDelta(Individual indi,int at,int at2)
{
 int c[];
 int size;
 int c1,c2,c3,c4,c5,c6;
 double originalLength = 0.0;
 double newLength      = 0.0;
  
 size            = (int)indi.getSize();
 c               = (int[])indi.getChromosome();
 
 c1 = c[at];
 c2 = c[at2];
 c3 = c[mod(at-1,size)];
 c4 = c[mod(at+1,size)];
 c5 = c[mod(at2-1,size)];
 c6 = c[mod(at2+1,size)];
 
 if(at==at2-1)
 {
  originalLength =  ((TSPProblem)myProblem).getDistance(c3,c1)  + ((TSPProblem)myProblem).getDistance(c2,c6) ;
  newLength      =  ((TSPProblem)myProblem).getDistance(c3,c2) + ((TSPProblem)myProblem).getDistance(c1,c6);
 }
 else
 {
  if(mod(at2+1,size)==at)
  {
   originalLength =  ((TSPProblem)myProblem).getDistance(c1,c4)  + ((TSPProblem)myProblem).getDistance(c2,c5) ;
   newLength      =  ((TSPProblem)myProblem).getDistance(c2,c4) + ((TSPProblem)myProblem).getDistance(c1,c5);
  }
  else
  {
   originalLength =  ((TSPProblem)myProblem).getDistance(c3,c1) + ((TSPProblem)myProblem).getDistance(c1,c4) + ((TSPProblem)myProblem).getDistance(c5,c2) + ((TSPProblem)myProblem).getDistance(c2,c6) ;
   newLength      =  ((TSPProblem)myProblem).getDistance(c3,c2) + ((TSPProblem)myProblem).getDistance(c2,c4) + ((TSPProblem)myProblem).getDistance(c5,c1) + ((TSPProblem)myProblem).getDistance(c1,c6);
  }
 }
 
 return(originalLength-newLength);
 
}





private  void fourExchangeAtSize(Individual indi,int at, int at2)
 {

  int c[];
  int tmp;
  
    c              = (int[])indi.getChromosome();

    tmp=c[at];
    c[at]=c[at2];
    c[at2]=tmp;
    
    indi.setChromosome(c);
 }   
    
    


/* ################################################ */
/* Definition of orInsertion and auxiliary methods  */
/* ################################################ */
/* this is a simplified version of the or-insertion */
/* ################################################ */
private	void orInsertion(int blindFirstBest,int numIt, Individual indi)
{
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta     = 0.0;
 double newDelta          = 0.0;
 boolean tourChanged      = true;
 int  scrambleVector[];
 int to                   = 0;
 int betterTo             = 0; 
 

 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

  
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
  if(tourChanged)
  {
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
	at       = (int) MAFRA_Random.nextUniformLong(0,size-1);
	longitud = (int) MAFRA_Random.nextUniformLong(1,size-at);
        for(to=0;to<size;to++)
        {
      	  if((to<at-1)||(to>at+longitud))
	  {
	   newDelta      = computeOrInsertionDelta(indi,at,longitud,to);
           if((newDelta>previousDelta)&&(newDelta>0))
           {/* an improvement was found */
            betterAt       = at;
            betterLongitud = longitud;
	    betterTo       = to;
            previousDelta  = newDelta;
            tourChanged    = true;
           }
	  }
	 }
        if(tourChanged)
        {
         orInsertionAtLongitudTo(indi,betterAt,betterLongitud,betterTo);  
        }
	break;
       }
    case 1:
       {/* First improvement = next ascend */
      	improvementFound = false;
        tourChanged    = false;
	to = 0;
      	scramble(scrambleVector,size);
	at       = (int) MAFRA_Random.nextUniformLong(0,size-1);
	longitud = (int) MAFRA_Random.nextUniformLong(1,size-at);
        while( (!improvementFound) && (to<size) )  
      	  {
      	    improvementFound = false;
	    if((scrambleVector[to]<at-1)||(scrambleVector[to]>at+longitud))
	    {
      	     newDelta      = computeOrInsertionDelta(indi,at,longitud,scrambleVector[to]);
      	     if(newDelta>0)
      	     {/* an improvement was found */
       	       improvementFound = true;
       	       betterAt         = at;
       	       betterLongitud   = longitud;
	       betterTo         = scrambleVector[to];     
      	     }
	    }
      	    to++;
      	  }
      	if (improvementFound)
      	{
      	  orInsertionAtLongitudTo(indi,betterAt,betterLongitud,betterTo);
      	  tourChanged      = true;
      	}
	break;
       }
    case 0:
       {/*blind*/
        at       = (int) MAFRA_Random.nextUniformLong(0,size-1);
	longitud = (int) MAFRA_Random.nextUniformLong(1,size-at);
        do
	{
	 to = (int) MAFRA_Random.nextUniformLong(0,size);
	} while((to>=at)&&(to<=at+longitud));
	orInsertionAtLongitudTo(indi,at,longitud,to);
        tourChanged = true;
        break;
       }
   }
  }
 }/* number of iterations */


}



private double computeOrInsertionDelta(Individual indi, int at, int longitud, int to)
{

 int c[];
 int size;
 int c1,c2,c3,c4,c5,c6;
 double originalLength = 0.0;
 double newLength      = 0.0;
  
 size            = (int)indi.getSize();
 c               = (int[])indi.getChromosome();
 

 c1 = c[mod(at,size)];
 c2 = c[mod(at+longitud,size)];
 c3 = c[mod(at-1,size)];
 c4 = c[mod(at+longitud+1,size)];
 c5 = c[mod(to,size)];
 c6 = c[mod(to+1,size)];
 
 
  originalLength =  ((TSPProblem)myProblem).getDistance(c3,c1) + ((TSPProblem)myProblem).getDistance(c2,c4) + ((TSPProblem)myProblem).getDistance(c5,c6) ;
  newLength      =  ((TSPProblem)myProblem).getDistance(c3,c4) + ((TSPProblem)myProblem).getDistance(c5,c1) + ((TSPProblem)myProblem).getDistance(c2,c6);



 return(originalLength-newLength);
 
}




private  void orInsertionAtLongitudTo(Individual indi,int at, int longitud,int to)
{

  int c[];
  int tmp;
  int segment[];
  int size;
  int f;
  int i;
  
    c        = (int[])indi.getChromosome();
    size     = (int)(indi.getSize());
    segment  = new int[size];
    
  //  System.out.println("at="+at+" longitud="+longitud+" to:"+to);

    i=0;    
    if(to>(at+longitud))
    {
     for(f=0;f<at;f++)
     {
      segment[i]=c[f];
      i++;
     }
     for(f=(at+longitud);f<=to;f++)
     {
      segment[i]=c[f];
      i++;
     }
     for(f=at;f<(at+longitud);f++)
     {
      segment[i]=c[f];
      i++;
     }
     for(f=(to+1);f<size;f++)
     {
      segment[i]=c[f];
      i++;
     }
    }
    else
    {
     for(f=0;f<=to;f++)
     { 
      segment[i]=c[f];
      i++;
     }
     for(f=at;f<(at+longitud);f++)
     {
      segment[i]=c[f];
      i++;
     }
     for(f=(to+1);f<at;f++)
     {
      segment[i]=c[f];
      i++;
     }
     for(f=(at+longitud);f<size;f++)
     {
      segment[i]=c[f];
      i++;
     }
    }
    
    for(f=0;f<size;f++)
    {
     c[f] = segment[f];
    }
    
   indi.setChromosome(c); 

}


/* ############################################################################# */
/* ############################################################################# */
/* ####################### Definition of Boltzmann Methods ##################### */
/* ############################################################################# */
/* ############################################################################# */

private	void twoExchangeBoltzmann(int blindFirstBest,int numIt, Individual indi)
{
     
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];
 double threshold;
 double rndNumber;

 
 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }
 clearForbidTable();
 //System.out.println("###################### Begin of Local optimiation #######################");
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
 
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
        for(at=0;at<size;at++)
        {
         for(longitud=1;longitud<=((size/2)+1);longitud++)
         {
          if (!forbiden(at,longitud))
          {
	       newDelta      = computeTwoExchangeDelta(indi,at,longitud);
           if((newDelta>previousDelta) && (newDelta>0))
           { /* A change was found. If it was an improvement it will take the best
              of them. */
            betterAt       = at;
            betterLongitud = longitud;
            previousDelta  = newDelta;
            tourChanged    = true;
	   	   
           }
          }
         }
        }
        if(tourChanged)
        {
         /* this is the best possible improvement */
          twoExchangeAtSize(indi,betterAt,betterLongitud); 
          //System.out.println("Improving ---> Delta="+previousDelta+", AT="+betterAt+", Lon="+betterLongitud);
        }
        else
        {/* the move is a random demeliorating moves*/
          betterAt       = ((int)MAFRA_Random.nextUniformLong(0,size));
      	  betterLongitud = ((int)MAFRA_Random.nextUniformLong(1,(int)((size/2)+1)));
      	  previousDelta      = Math.abs(computeTwoExchangeDelta(indi,betterAt,betterLongitud));
          threshold = Math.exp(-K*previousDelta/temperature);
          rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
          tourChanged = true;
          if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           twoExchangeAtSize(indi,betterAt,betterLongitud); 
           forbid(betterAt,betterLongitud);
           //System.out.println("Demeliorating ---> Delta="+previousDelta+", AT="+betterAt+", Lon="+betterLongitud);
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
          
        }
         
	     // System.out.println("Delta="+previousDelta+", AT="+betterAt+", Lon="+betterLongitud);
	     previousDelta = - Double.MAX_VALUE; 
	     break;
        }
    case 1:
       {/* First improvement = next ascend */
      	improvementFound = false;
      	at               = 0;
      	tourChanged    = false;
      	scramble(scrambleVector,size);
      	previousDelta = - Double.MAX_VALUE;
      	
      	while( (!improvementFound) && (at<size ))
      	{
      	  longitud = 1;
          while( (!improvementFound) && (longitud<=((size/2)+1) ) )
	      {
      	    improvementFound = false;
      	    if (!forbiden(scrambleVector[at],longitud))
      	    {
      	     newDelta      = computeTwoExchangeDelta(indi,scrambleVector[at],longitud);
      	     if(newDelta>0)
      	     {/* an improvement was found */
       	       improvementFound = true;
       	       betterAt         = scrambleVector[at];
       	       betterLongitud   = longitud;
      	     }
      	     
      	    }
      	    longitud = longitud + 1;
      	  }
      	  at = at+1;
      	}
      	if (improvementFound)
      	{
      	  twoExchangeAtSize(indi,betterAt,betterLongitud);
      	  tourChanged      = true;
      	  
      	}
      	else
      	{/* we need to apply a boltzmann criteria to a randomly selected move        */
      	 /* we know the move is a bad one because we couldn't find a good one before */
      	 betterAt       = ((int)MAFRA_Random.nextUniformLong(0,size));
      	 betterLongitud = ((int)MAFRA_Random.nextUniformLong(1,(int)((size/2)+1)));
      	 newDelta      = Math.abs(computeTwoExchangeDelta(indi,betterAt,betterLongitud));
      	 threshold = Math.exp(-K*newDelta/temperature);
      	 rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);

      	/* System.out.println("delta="+newDelta+", temperature="+temperature+", K="+K+", ro="+threshold+"("+errorThreshold+"), rnd="+rndNumber);
         System.out.println();
      	 if (threshold>errorThreshold)
      	 {
      	  threshold = errorThreshold;
      	 }*/
        
         tourChanged      = true; /* we say that the tour change even it can be rejected         */
                                  /* because in the next iteration (if any) it can be changed by */
                                  /* a different boltzmann random change                         */
         if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           twoExchangeAtSize(indi,betterAt,betterLongitud);            
           forbid(betterAt,betterLongitud);
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
          
      	 
      	}
      	
    	break;
       }
    case 0:
       {/*blind - this was the case studied in gecco2000 paper. BUT because we
          don't check if newDelta is positive the acceptance prob. varies accordingly
          to the sign of newDelta */
          
         betterAt       = ((int)MAFRA_Random.nextUniformLong(0,size));
      	 betterLongitud = ((int)MAFRA_Random.nextUniformLong(1,(int)((size/2)+1)));
      	 newDelta      = computeTwoExchangeDelta(indi,betterAt,betterLongitud);
      	 threshold = Math.exp(-K*Math.abs(newDelta)/temperature);
         rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
         if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           twoExchangeAtSize(indi,betterAt,betterLongitud); 
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
          tourChanged      = true;
          break;
       }
   }/* end of switch statement */
  
 }/* number of iterations */
 //System.out.println("###################### End of Local optimiation #######################");

}


private	void threeExchangeBoltzmann(int blindFirstBest,int numIt, Individual indi)
{
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];
 double threshold;
 double rndNumber;
 

 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

 clearForbidTable();
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
 
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
        for(at=0;at<size;at++)
        {
         for(longitud=0;longitud<size;longitud++)
         {
	      if((at!=longitud)&&(mod(at-1,size)!=longitud)&&(mod(at+1,size)!=longitud)&&(!forbiden(at,longitud)))
	      {
	       newDelta      = computeThreeExchangeDelta(indi,at,longitud);
           if((newDelta>previousDelta)&&(newDelta>0))
           {/* A change was found. If it was an improvement it will take the best
              of them*/
            betterAt       = at;
            betterLongitud = longitud;
            previousDelta  = newDelta;
            tourChanged    = true;
           }
	      }
	     }
        }
        
        if(tourChanged)
        {
         /* this is the best possible improvement */
         threeExchangeAtSize(indi,betterAt,betterLongitud); 
        }
        else
         {/* the move is a random demeliorating move*/
          do
	      {
      	   at =((int)MAFRA_Random.nextUniformLong(0,size));
	       longitud =((int)MAFRA_Random.nextUniformLong(0,size));
	      } while((at==longitud)||(mod(at-1,size)==longitud)||(mod(at+1,size)==longitud));
	      newDelta      = Math.abs(computeThreeExchangeDelta(indi,at,longitud));
          threshold = Math.exp(-K*newDelta/temperature);
          rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
          tourChanged = true;
          if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           threeExchangeAtSize(indi,at,longitud); 
           forbid(at,longitud);
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
          
         }
         // System.out.println("Delta="+previousDelta+", AT="+betterAt+", Lon="+betterLongitud);
	     previousDelta = - Double.MAX_VALUE; 
         break;
    }
    case 1:
       {/* First improvement = next ascend */
       
      	improvementFound = false;
      	at               = 0;
      	tourChanged    = false;
      	scramble(scrambleVector,size);
      	while( (!improvementFound) && (at<size) )
      	{
      	  longitud = 0;
      	  while( (!improvementFound) && (longitud<size) )  
      	  {
      	    improvementFound = false;
	        if((scrambleVector[at]!=longitud)&&(mod(scrambleVector[at]-1,size)!=longitud)&&(mod(scrambleVector[at]+1,size)!=longitud)&& (!forbiden(scrambleVector[at],longitud)))
	        {
      	     newDelta      = computeThreeExchangeDelta(indi,scrambleVector[at],longitud);
      	     if(newDelta>0)
      	     {/* an improvement was found */
       	       improvementFound = true;
       	       betterAt         = scrambleVector[at];
       	       betterLongitud   = longitud;
      	     }
	      }
      	  longitud = longitud + 1;
      	 }
      	 at = at+1;
	    }
	    
	   // System.out.println("Improvement="+improvementFound+", Delta="+newDelta);
        if (improvementFound)
      	{
      	  threeExchangeAtSize(indi,betterAt,betterLongitud);
      	  tourChanged      = true;
      	 
      	}
      	else
      	{/* we need to apply a boltzmann criteria to a randomly selected move        */
      	 /* we know the move is a bad one because we couldn't find a good one before */
      	  System.out.println("No improvement!,");
      	 do
	     {
      	  at =((int)MAFRA_Random.nextUniformLong(0,size));
	      longitud =((int)MAFRA_Random.nextUniformLong(0,size));
	     } while((at==longitud)||(mod(at-1,size)==longitud)||(mod(at+1,size)==longitud));
      	 newDelta      = Math.abs(computeThreeExchangeDelta(indi,at,longitud));
      	 threshold = Math.exp(-K*newDelta/temperature);
      	 rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
      	/* System.out.println("delta="+newDelta+", temperature="+temperature+",K="+K+", ro="+threshold+"("+errorThreshold+"), rnd="+rndNumber);
         System.out.println();
      	 if (threshold>errorThreshold)
      	 {
      	  threshold = errorThreshold;
      	 }*/
         
         tourChanged = true;
         if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           threeExchangeAtSize(indi,at,longitud); 
           forbid(at,longitud);
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
         
      	 
      	}
	    break;
       }
       
    case 0:
       { /*blind - this was the case studied in gecco2000 paper. BUT because we
          don't check if newDelta is positive the acceptance prob. varies accordingly
          to the sign of newDelta */
         do
	     {
      	  at =((int)MAFRA_Random.nextUniformLong(0,size));
	      longitud =((int)MAFRA_Random.nextUniformLong(0,size));
	     } while((at==longitud)||((at-1)==longitud)||((at+1)==longitud));
      	 newDelta      = Math.abs(computeThreeExchangeDelta(indi,at,longitud));
      	 threshold = Math.exp(-K*newDelta/temperature);
         rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
         if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           threeExchangeAtSize(indi,at,longitud); 
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
          tourChanged      = true;
          break;
       }
   }
  
 }/* number of iterations */

}

private	void fourExchangeBoltzmann(int blindFirstBest,int numIt, Individual indi)
{
 int numberOfIterations   = 0;
 int size                 = (int)indi.getSize();
 boolean improvementFound = false;
 int at                   = 0;
 int betterAt   	  = 0;
 int longitud             = 0;
 int betterLongitud       = 0;
 double previousDelta = 0.0;
 double newDelta = 0.0;
 boolean tourChanged = true;
 int  scrambleVector[];
 double threshold;
 double rndNumber;
 
 scrambleVector= new int[size];
 for (at=0;at<size;at++)
 {
  scrambleVector[at]=at;
 }

 clearForbidTable();
 for(numberOfIterations=0;numberOfIterations<numIt;numberOfIterations++)
 {
  
   switch(blindFirstBest)
   {
    case 2:
       {/* Best improvement = steepest ascend */
        tourChanged   = false;
        previousDelta = - Double.MAX_VALUE;
        for(at=0;at<=size-2;at++)
        {
         for(longitud=at+1;longitud<size;longitud++)
         {
          if(!forbiden(at,longitud))
          {
           newDelta      = computeFourExchangeDelta(indi,at,longitud);
           if((newDelta>previousDelta)&&(newDelta>0))
           {/* A change was found. If it was an improvement it will take the best
              of them, if there are not improvements it will take the move that
              make the smallest demelioration.*/
            betterAt       = at;
            betterLongitud = longitud;
            previousDelta  = newDelta;
            tourChanged    = true;
           }
          }	 
         }
        }
        if(tourChanged)
        {
         /* this is the best possible improvement */
         fourExchangeAtSize(indi,betterAt,betterLongitud); 
        }
        else
        {/* the move is the leaser of the demeliorating moves*/
         at =((int)MAFRA_Random.nextUniformLong(0,size-2));
	     longitud =((int)MAFRA_Random.nextUniformLong(0,size));
	     newDelta      = Math.abs(computeFourExchangeDelta(indi,at,longitud));
         threshold = Math.exp(-K*newDelta/temperature);
         rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
         tourChanged = true;
	     if(rndNumber<threshold)
         {/* we applied the move any way, no mater it is a bad one */
          fourExchangeAtSize(indi,betterAt,betterLongitud); 
          forbid(at, longitud);
         }
         else
         {/* we don't do anything. we reject the bad move */
         }
          
        }
        // System.out.println("Delta="+previousDelta+", AT="+betterAt+", Lon="+betterLongitud);
	    previousDelta = - Double.MAX_VALUE; 
        break;
       }
    case 1:
       {/* First improvement = next ascend */
      	improvementFound = false;
      	at               = 0;
      	tourChanged    = false;
      	scramble(scrambleVector,size);
      	while( (!improvementFound) && (at<=(size-2) ) )
      	{
      	  longitud = at+1;
      	  while( (!improvementFound) && (longitud<size) ) 
      	  {
      	    improvementFound = false;
	        if(!forbiden(scrambleVector[at],longitud))
	        {
      	     newDelta      = computeFourExchangeDelta(indi,scrambleVector[at],longitud);
      	     if(newDelta>0)
      	     {/* an improvement was found */
       	       improvementFound = true;
       	       betterAt         = scrambleVector[at];
       	       betterLongitud   = longitud;
      	     }
	        }
      	    longitud = longitud + 1;
      	  }
      	  at = at+1;
	    }
        if (improvementFound)
      	{
      	  fourExchangeAtSize(indi,betterAt,betterLongitud);
      	  tourChanged      = true;
      	}
        else
      	{/* we need to apply a boltzmann criteria to a randomly selected move        */
      	 /* we know the move is a bad one because we couldn't find a good one before */
      	 at =((int)MAFRA_Random.nextUniformLong(0,size-2));
	     longitud =((int)MAFRA_Random.nextUniformLong(0,size));
      	 newDelta      = Math.abs(computeFourExchangeDelta(indi,at,longitud));
      	 threshold = Math.exp(-K*newDelta/temperature);
     	 rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
      	 /*System.out.println("delta="+newDelta+", temperature="+temperature+" ,K="+K+", ro="+threshold+"("+errorThreshold+"), rnd="+rndNumber);
         System.out.println();
      	 if (threshold>errorThreshold)
      	 {
      	  threshold = errorThreshold;
      	 }*/
         tourChanged = true;
         if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           fourExchangeAtSize(indi,at,longitud); 
           forbid(at,longitud);
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
         }
	    break;
       }
    case 0:
       {/*blind - this was the case studied in gecco2000 paper. BUT because we
          don't check if newDelta is positive the acceptance prob. varies accordingly
          to the sign of newDelta */
         at =((int)MAFRA_Random.nextUniformLong(0,size-2));
	     longitud =((int)MAFRA_Random.nextUniformLong(0,size));
      	 newDelta      = Math.abs(computeFourExchangeDelta(indi,at,longitud));
      	 threshold = Math.exp(-K*newDelta/temperature);
         rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
         if(rndNumber<threshold)
          {/* we applied the move any way, no mater it is a bad one */
           fourExchangeAtSize(indi,at,longitud); 
          }
          else
          {/* we don't do anything. we reject the bad move */
          }
          tourChanged      = true;
      	 
        break;
       }
   }
  
 }/* number of iterations */

}





/* ############################################### */
/* General           auxiliary             methods */
/* ############################################### */

int mod(int a, int b)
{
// System.out.print("mod("+a+","+b+")=");
 if (a>=0)
 {
 // System.out.println((b+a)%b);
  return (a%b);
 }
 else
 {
  while(b+a<0)
  {
   a=b+a;
  }
 // System.out.println((b+a)%b);
  return((b+a)%b);
 }
 
}  


private void scramble(int scrambleVector[],int size)
{
  int rnd1,rnd2,tmp,i;
  for(i=0;i<size;i++)
  {
   rnd1             =((int) MAFRA_Random.nextUniformLong(0,size));
   rnd2             =((int) MAFRA_Random.nextUniformLong(0,size));
   tmp              = scrambleVector[rnd1];
   scrambleVector[rnd1] = scrambleVector[rnd2];
   scrambleVector[rnd2] = tmp;
  }
}



public void setErrorThreshold(double aE)
{
 errorThreshold = aE;
}

public double getErrorThreshold()
{
 return errorThreshold;
}


public void setTemperature(double aT)
{
 temperature = aT;
}

public void setK(double aK)
{
 K=aK;
}

public double getTemperature()
{
 return temperature;
}

public double getK()
{
 return K;
}


private void clearForbidTable()
{
 int i;
 
 for(i=0;i<numIt;i++)
 {
  forbidTable[0][i] = -1;
  forbidTable[1][i] = -1;
 }
 fPos = 0;
}


private void forbid(int a, int b)
{
 forbidTable[0][fPos]=a;
 forbidTable[1][fPos]=b;
 //System.out.println("Forbidding:"+a+","+b);
 fPos++;
}

private boolean forbiden(int a, int b)
{
 int i=0;
 boolean result=false;
 do
 {
  result= (forbidTable[0][i]==a)&&(forbidTable[1][i]==b);
  i++;
 } while((!result) && (i<fPos));
 /*if (result)
  System.out.println("Forbidden:"+a+","+b);*/
 return result;
}

 /* The following methods are here in order to complete the Abstract part of the superclass */
 public Individual individualLocalSearch(Individual parent1, double temperature, double sigma, double popSize,FitnessVisitor myFitnessVisitor)
    {
     return null;
    
    }
 public Individual individualLocalSearch(Individual parent1, double temperature, double K,double distances[][],FitnessVisitor myFitnessVisitor)
    {
      return null;
    }
 public Individual individualLocalSearch(Individual parent1, double temperature,double distances[][],FitnessVisitor myFitnessVisitor)
    {
      return null;
    }


}
