


package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;
import MAFRA.Factory.*;


/** @version Memetic Algorithms Framework - V 1.2 - August 2000
    @author  Natalio Krasnogor
    
    
    This class implements a general hill climber local searcher.
    At creation it receives:
    
    1) a boolean, SinOpt, such that if SinOpt=true then the algorithm will be
       a move-Opt, that is, it will try to apply move until no improvement can be found.
       If SinOpt=false, only a specified number of applications of move will be done.
    2) an int, blindFirstBest, such that if blindFirstBest=0 then a blind (random) application
       of Move will occurr. If blindFirstBest=1 the first improvement will
       be accepted, otherwise (blindFirstBest=2) then the best improvement will be accepted.
    3) an int, NumIt, that specifies the number of iterations. This is valid only when
       SinOpt = false.
    4) a move which is a string of the form:   A-->C where |A|=|C| and A,C are in Alphabet^*. A will be changed
       to C. There are  special rules:
       
        IdRule :  _-->_   that preserves the structure integrity.
	LRandom:  *-->P   where |*|=|P|, P is a fixed pattern that is applied to any pattern * of size |P|.
	RRandom:  P-->*   where |*|=|P|, P is a fixed pattern that is replaced by a random pattern * of size |P|.
	Random :  *-->*   where a random pattern of a given size replaces any pattern of the same size.
	Stretch:  *-->|   where any pattern * of size |*| is stretched. 
	Pivot1 :  *-->^1  where any pattern * of size |*| is pivoted once anticlockwise
	Pivot2 :  *-->^2  where any pattern * of size |*| is pivoted twice anticlockwise
	Pivot3 :  *-->^3  where any pattern * of size |*| is pivoted thrice anticlockwise
	Pivot4 :  *-->^4  where any pattern * of size |*| is pivoted fourth anticlockwise
	Pivot5 :  *-->^5  where any pattern * of size |*| is pivoted fifth anticlockwise
	Reflec1:  *-->%NS any pattern is reflected in the NS axis
	Reflec2:  *-->%ns any pattern is reflected in the ns axis
	Reflec3:  *-->%WE any pattern is reflected in the WE axis
    
	
     5) a boolean, HcBhc, such that if HcBhc=false then the points 1,2,3 and 4 above are performed
        in a strictly HillClimber manner. If, however, HcBhc=true then 1,2,3 and 4 are interpreted
        and performed as a BoltzmannHillclimber.
    */
public class PFGeneralHillClimberLocalSearch2112001 extends AbstractLocalSearcher{

 private static int MAXMEMENUMBER = 40;
 private boolean sinOpt;
 private int blindFirstBest;
 private int numIt;
 private String memeplex[][];
 private int    memesNumber;
 private boolean hcBhc;
 private PFProblemFactory myProblem;
 private FitnessVisitor myFitnessVisitor;
 private double temperature;
 private double K;
 private double errorThreshold;
 private int forbidTable[][];
 private int fPos;

 public PFGeneralHillClimberLocalSearch2112001(boolean SinOpt,boolean HcBhc, int BlindFirstBest, int NumIt, String Move, PFProblemFactory aProblemFactory)
 {
  sinOpt           = SinOpt;
  hcBhc            = HcBhc;
  blindFirstBest   = BlindFirstBest;
  numIt            = NumIt;
 
  myProblem        = aProblemFactory; 
  myFitnessVisitor = new FitnessVisitor(myProblem);
  forbidTable      = new int[2][NumIt];
  fPos             = 0;
  memeplex         = new String[MAXMEMENUMBER][2];
  parseMemeplex(Move);
  }

 

 public Individual individualLocalSearch(Individual parent1)
 {
  if (parent1==null) 
  {
   System.out.println("ERROR - individual in local searcher PFGeneralHillClimberLocalSearcher is null");
   System.exit(0);
  }
  
 // System.out.println("IndiFit="+parent1.getFitness());
  ((SelfAdaptingStaticPFStringIndividual)parent1).setTemperature(temperature);
 
 
   if(sinOpt)
   {/* perform an Opt-move search*/
    /* to be done later */
   }
   else
   {/* perform an interative search with numIt iterations */
    switch(blindFirstBest)
    {
     case 0: /* blind search */
     {
      if(!hcBhc) /* must be a strict hill-climber, we put the temperature to zero */
      {
       ((SelfAdaptingStaticPFStringIndividual)parent1).setTemperature(0);
       temperature = 0.0;
    //   boltzmannHillClimberBlindAscent(parent1,numIt);
      }
      else
      {
     //  boltzmannHillClimberBlindAscent(parent1,numIt);
      }
      break; 
     }
     case 1: /* first ascent */
     {
      if(!hcBhc) /* must be a strict hill-climber, we put the temperature to zero */
      {
       ((SelfAdaptingStaticPFStringIndividual)parent1).setTemperature(0);
       temperature = 0.0;
       boltzmannHillClimberFirstAscent(parent1,numIt);
      }
      else
      { 

        boltzmannHillClimberFirstAscent(parent1,numIt);
      }
      break;
     }
     case 2: /* best ascent */
     {
      break;
     }
    }
   }     	    
 
 // System.out.println("IndiFit="+parent1.getFitness());
  return(parent1);
  
 }


private void boltzmannHillClimberFirstAscent(Individual parent1,int numIt)
{ 
 int f;
 int j;
 int i;
 int its;
 int structureLength;
 int rMoveLength=0;
 int pivotPosition;
 int rndPivot;
 String newStructure=new String();
 double newFitness;
 String oldStructure;
 double oldFitness;
 double delta;
 double threshold;
 double rndNumber;
 char   randomMove[];
 boolean pivotRuleToBeProcessed = false;
 boolean reflectRuleToBeProcessed = false;
 boolean changed;
 boolean improvementFound;
 boolean macroMutRule = false;
 boolean pivotReflRul = false;
 double bestFitness=Double.NEGATIVE_INFINITY;
 String bestStructure=new String();
 int    memesPerm[];
 String lMove;
 String rMove;
 int    pivotPerm[];
 
 
 
 // System.out.println("K="+K);
 oldStructure    = (String)(((SelfAdaptingStaticPFStringIndividual)parent1).getChromosome());
 structureLength = oldStructure.length();
 oldFitness      = parent1.getFitness();
 memesPerm       = new int[memesNumber];
 for(i=0;i<memesNumber;i++)
 {
  memesPerm[i]=i;
 }
 
  //System.out.println("Start-LocalSearch:");
  changed = true; 
  for(its=0;(its<numIt)&&changed;its++)
  {
   scramble(memesPerm,memesNumber);
   for(j=0;j<memesNumber;j++)
   {
    lMove = memeplex[memesPerm[j]][0];
    rMove = memeplex[memesPerm[j]][1];
    rMoveLength= rMove.length();	
	 if(lMove.equals("*")&&((rMove.indexOf("*")!=-1)||(rMove.indexOf("]")!=-1) ))
	 {/* ########### MACRO MUTATION RULE ############# */
	  /* ###########         or          ############# */
	  /* ###########    STRETCH RULE     ############# */
	  
	  changed     = false;
	  bestFitness = parent1.getFitness();
	
	  if(rMove.indexOf("*")!=-1)
	  {/* MACRO MUT RULE */
	      //	   rMove = ((PFProblemRel)myProblem).getSubstructure((long)rMoveLength);
	   rMove = myProblem.getSubstructure((long)rMoveLength);
	  }
	  else
	  {/* STRETCH RULE */
	      //	   rMove = ((PFProblemRel)myProblem).getStretchSubstructure((long)rMoveLength);
	   rMove = myProblem.getStretchSubstructure((long)rMoveLength);
	  }
	  /* we initialize the pivots permutations */
	  pivotPerm = new int[structureLength-rMoveLength];
	  for(i=0;i<structureLength-rMoveLength;i++)
	      {
		  pivotPerm[i]=i;
	      }
	  scramble(pivotPerm,structureLength-rMoveLength);
	  for(pivotPosition=0;!changed && pivotPosition<(structureLength-rMoveLength);pivotPosition++)
	  {
	   newStructure = oldStructure.substring(0,pivotPerm[pivotPosition])+rMove+oldStructure.substring(pivotPerm[pivotPosition]+rMoveLength);
	   parent1.setChromosome(newStructure);
	   myFitnessVisitor.visitIndividual(parent1);
	   newFitness = parent1.getFitness();
	   if(bestFitness>=newFitness)
	   {/* the meme is harmful so we do a Boltzmann acceptance criteria */
	    delta      = bestFitness - newFitness;   
	    threshold  = Math.exp(-K*delta/temperature);
	    rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
	    if(rndNumber<threshold)
	    {/* the meme is not good but we accept it */
	     //    System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	     bestFitness = newFitness;
	     bestStructure = new String(newStructure);
	     changed = true;
	    }
	   }
	   else
	   {/* the meme is good so we accept it */
	    // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	    bestFitness = newFitness;
	    bestStructure = new String(newStructure);
	    changed = true;
	   }
          }/* end of for (pivotPosition...*/
	  if (changed)
	  {
	   parent1.setChromosome(bestStructure);
	   parent1.setFitness(bestFitness);
	   oldStructure = new String(bestStructure);
	  }
	  else
	  {
	   parent1.setChromosome(oldStructure);
	   parent1.setFitness(oldFitness);
	     //oldStructure = new String(bestStructure);
	  }
	 } /* end of the check for MACRO-STRETCH RULE */

         else
	 if(lMove.equals("*")&& ( (rMove.indexOf("~")!=-1) || (rMove.indexOf("+")!=-1)  || (rMove.equals("^"))) )
	 {/* ################ REFLECT RULES ################# */
	  /* ################      or       ################# */
	  /* ################ PIVOT RULES   ################# */
  	  /* ################      or       ################# */
	   /* ################ SHIFT RULES  ################# */
	  changed     = false;
	  bestFitness = parent1.getFitness();
	  /* we initialize the pivots permutations */
	  pivotPerm = new int[structureLength-rMoveLength];
	  for(i=0;i<structureLength-rMoveLength;i++)
	      {
		  pivotPerm[i]=i;
	      }
	  scramble(pivotPerm,structureLength-rMoveLength);
	  for(pivotPosition=0;!changed && pivotPosition<(structureLength-rMoveLength);pivotPosition++)
	  {
	 
	    if(lMove.equals("*") && (rMove.indexOf("~")!=-1))
	     {/* this is  rule reflect */
	      rMoveLength=rMove.length();
	      newStructure = myProblem.getReflectSubstructure(oldStructure,pivotPerm[pivotPosition],rMoveLength,0);
	      pivotReflRul=true;
	     }
	    else
	    if(lMove.equals("*") && (rMove.indexOf("+")!=-1))
	     {/* this is  rule shift*/
	      rMoveLength=rMove.length();
	      newStructure = myProblem.getShiftSubstructure(oldStructure,pivotPerm[pivotPosition],rMoveLength);
	      pivotReflRul=true;
	     }
	    else
	    if(lMove.equals("*") && rMove.equals("^"))
	    { /* this is Pivot rule */
	      newStructure = myProblem.getPivotSubstructure(oldStructure,pivotPerm[pivotPosition],structureLength-pivotPerm[pivotPosition],((int)MAFRA_Random.nextUniformLong(1,2)));     
	      pivotReflRul=false;
	    }
	   
	    parent1.setChromosome(newStructure);
	    myFitnessVisitor.visitIndividual(parent1);
	    newFitness = parent1.getFitness();
	    if(bestFitness>=newFitness)
	    {/* the meme is harmful so we do a Boltzmann acceptance criteria */
	     delta      = bestFitness - newFitness;   
	     threshold  = Math.exp(-K*delta/temperature);
	     rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
	     if(rndNumber<threshold)
	     {/* the meme is not good but we accept it */
	      //   System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	      bestFitness = newFitness;
	      bestStructure = new String(newStructure);
	      changed = true;
	     }
	    }
	    else
	    {/* the meme is good so we accept it */
	     // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	     bestFitness = newFitness;
	     bestStructure = new String(newStructure);
	     changed = true;
	    }

	  }/* end of for(pivotPosition... */
	   if (changed)
	   {
	    parent1.setChromosome(bestStructure);
	    parent1.setFitness(bestFitness);
	    oldStructure = new String(bestStructure);
	   }
	   else
	   {
	    parent1.setChromosome(oldStructure);
	    parent1.setFitness(oldFitness);
	    //oldStructure = new String(bestStructure);
	   }
	   }/* end of check for PIVOT-REFLECT-SHIFT RULES */
	  
	 else
	 if( !lMove.equals("*") && !rMove.equals("*") )
	 {/* ################   P-->P RULES ##################### */
	  changed     = false;
	  bestFitness = parent1.getFitness();
	  pivotPosition  = -1;
	  do
	  {
	   pivotPosition = oldStructure.indexOf(lMove,pivotPosition+1);
	   if((pivotPosition>-1)&&(pivotPosition+rMoveLength<structureLength))
	   {
	    newStructure = oldStructure.substring(0,pivotPosition)+rMove+oldStructure.substring(pivotPosition+rMoveLength);
	    parent1.setChromosome(newStructure);
	    myFitnessVisitor.visitIndividual(parent1);
	    newFitness = parent1.getFitness();
	    if(bestFitness>=newFitness)
	    {/* the meme is harmful so we do a Boltzmann acceptance criteria */
	     delta      = bestFitness - newFitness;   
	     threshold  = Math.exp(-K*delta/temperature);
	     rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
	     if(rndNumber<threshold)
             {/* the meme is not good but we accept it */
	      //    System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	      bestFitness = newFitness;
	      bestStructure = new String(newStructure);
	      changed = true;
	     }
	    }
	    else
	    {/* the meme is good so we accept it */
	     // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	     bestFitness = newFitness;
             bestStructure = new String(newStructure);
             changed = true;
            }
	   }
	  } while( !changed && (pivotPosition<(structureLength-rMove.length())) &&(pivotPosition>-1) );
	  if (changed)
	  {
           parent1.setChromosome(bestStructure);
           parent1.setFitness(bestFitness);
           oldStructure = new String(bestStructure);
          }
          else
          {
           parent1.setChromosome(oldStructure);
           parent1.setFitness(oldFitness);
           //oldStructure = new String(bestStructure);
          }
	 } /* end of check for P-->P RULES */

	 else
	 { /* *-->P rule */
	  changed     = false;
	  bestFitness = parent1.getFitness();
	  /* we initialize the pivots permutations */
	  pivotPerm = new int[structureLength-rMoveLength];
	  for(i=0;i<structureLength-rMoveLength;i++)
	      {
		  pivotPerm[i]=i;
	      }
	  scramble(pivotPerm,structureLength-rMoveLength);
          for(pivotPosition=0;!changed && pivotPosition<(structureLength-rMoveLength);pivotPosition++)
          {
           newStructure = oldStructure.substring(0,pivotPerm[pivotPosition])+rMove+oldStructure.substring(pivotPerm[pivotPosition]+rMoveLength);
           parent1.setChromosome(newStructure);
           myFitnessVisitor.visitIndividual(parent1);
           newFitness = parent1.getFitness();
           if(bestFitness>=newFitness)
           {/* the meme is harmful so we do a Boltzmann acceptance criteria */
            delta      = bestFitness - newFitness;   
            threshold  = Math.exp(-K*delta/temperature);
            rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
            if(rndNumber<threshold)
            {/* the meme is not good but we accept it */
	     //    System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	     bestFitness = newFitness;
             bestStructure = new String(newStructure);
             changed = true;
	    }
	    else
	    {/* the meme is good so we accept it */
	     // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
	     bestFitness = newFitness;
	     bestStructure = new String(newStructure);
	     changed = true;
            }
	   }
	  }/* end of for(pivotPosition.... */
	  if (changed)
	  {
	   parent1.setChromosome(bestStructure);
           parent1.setFitness(bestFitness);
           oldStructure = new String(bestStructure);
          }
          else
          {
           parent1.setChromosome(oldStructure);
           parent1.setFitness(oldFitness);
           //oldStructure = new String(bestStructure);
          }
         } /* end of *-->P RULES */

	
     }/* end of for(memesNumber...) */  
   }/* end of for(its...) */
 // System.out.println("OldFitness="+oldFitness+", NewFitness="+parent1.getFitness()); 
 // System.out.println("End-LocalSearch.");
  
 }/* of method */
 












/* ############################################### */
/* General           auxiliary             methods */
/* ############################################### */

int mod(int a, int b)
{
// System.out.print("mod("+a+","+b+")=");
 if (a>=0)
 {
 // System.out.println((b+a)%b);
  return (a%b);
 }
 else
 {
  while(b+a<0)
  {
   a=b+a;
  }
 // System.out.println((b+a)%b);
  return((b+a)%b);
 }
 
}  


private void scramble(int scrambleVector[],int size)
{
  int rnd1,rnd2,tmp,i;
  for(i=0;i<size;i++)
  {
   rnd1             =((int) MAFRA_Random.nextUniformLong(0,size));
   rnd2             =((int) MAFRA_Random.nextUniformLong(0,size));
   tmp              = scrambleVector[rnd1];
   scrambleVector[rnd1] = scrambleVector[rnd2];
   scrambleVector[rnd2] = tmp;
  }
}



public void setErrorThreshold(double aE)
{
 errorThreshold = aE;
}

public double getErrorThreshold()
{
 return errorThreshold;
}


public void setTemperature(double aT)
{
 temperature = aT;
}

public void setK(double aK)
{
 K=aK;
}

public double getTemperature()
{
 return temperature;
}

public double getK()
{
 return K;
}


private void clearForbidTable()
{
 int i;
 
 for(i=0;i<numIt;i++)
 {
  forbidTable[0][i] = -1;
  forbidTable[1][i] = -1;
 }
 fPos = 0;
}


private void forbid(int a, int b)
{
 forbidTable[0][fPos]=a;
 forbidTable[1][fPos]=b;
 //System.out.println("Forbidding:"+a+","+b);
 fPos++;
}

private boolean forbiden(int a, int b)
{
 int i=0;
 boolean result=false;
 do
 {
  result= (forbidTable[0][i]==a)&&(forbidTable[1][i]==b);
  i++;
 } while((!result) && (i<fPos));
 /*if (result)
  System.out.println("Forbidden:"+a+","+b);*/
 return result;
}

 /* The following methods are here in order to complete the Abstract part of the superclass */
 public Individual individualLocalSearch(Individual parent1, double temperature, double sigma, double popSize,FitnessVisitor myFitnessVisitor)
    {
     return null;
    
    }
 public Individual individualLocalSearch(Individual parent1, double temperature, double K,double distances[][],FitnessVisitor myFitnessVisitor)
    {
      return null;
    }
 public Individual individualLocalSearch(Individual parent1, double temperature,double distances[][],FitnessVisitor myFitnessVisitor)
    {
      return null;
    }

private void  parseMemeplex(String Move)
{
 int nextMemePosition = -1;
 int prevMemePosition = -1;
 String meme;
 String rMove;
 String lMove;
 
 System.out.println("BEGIN Parsing Memeplex");
 memesNumber =0;
 do
 {
  nextMemePosition=Move.indexOf(";",nextMemePosition+1);
  if(nextMemePosition>0)
  {
   meme  = Move.substring(prevMemePosition+1,nextMemePosition);
   rMove = parseRightPart(meme);
   lMove = parseLeftPart(meme); 
   memeplex[memesNumber][0]=new String(lMove);
   memeplex[memesNumber][1]=new String(rMove);
   System.out.println(  memeplex[memesNumber][0]+"-->"+  memeplex[memesNumber][1]);
   memesNumber++;
   prevMemePosition = nextMemePosition;
   
  }

 } while(nextMemePosition!=-1);
 System.out.println("END Parsing Memeplex"); 
}

 
String parseLeftPart(String Move)
{
 String leftPart;
 int    index;
 
 index = Move.lastIndexOf("-->");
 leftPart = Move.substring(0,index);
 return leftPart;

}


 
String parseRightPart(String Move)
{
 String rightPart;
 int    index;
 
 index = Move.lastIndexOf("-->");
 rightPart = Move.substring(index+3);
 return rightPart;

}


}
