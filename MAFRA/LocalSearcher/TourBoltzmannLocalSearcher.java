



package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;


/** @version Memetic Algorithms Framework - V 1.0 - September 1999
    @author  Natalio Krasnogor
    */
public class TourBoltzmannLocalSearcher extends AbstractTourLocalSearcher{



 public Individual individualLocalSearch(Individual parent1, double temperature, double K,double distances[][],FitnessVisitor aFitnessVisitor)
 
    {


      long    rndMut; 
      double  previousFitness;
      double  newFitness;
     
   
      double  threshold;
      double  deltaE;
      double  rndNumber;
      int     numberChanges;
      int     numberConsecutiveChanges;
      int     i;
      int     j;
      TourIndividual tmpIndi;       
      int inversionSize;
      double size;



      tmpIndi         = (TourIndividual)(parent1.copyIndividual());
      size            = tmpIndi.getSize();
      previousFitness = tmpIndi.getFitness();
  
     


   
     numberChanges = 1 ; //M2912-1
     numberConsecutiveChanges = 1;
     //    System.out.println("#C="+numberChanges+", #CC"+numberConsecutiveChanges);
     
      for (i=0;i<numberChanges;i++)
	{
	  for(j=0;j<numberConsecutiveChanges;j++)
	    {
	     twoChange(tmpIndi,distances);
	     // twoSwap(tmpIndi);
	    }
	  tmpIndi.acceptVisitor(aFitnessVisitor);
	  newFitness = tmpIndi.getFitness();
	  /* Boltzman acceptance */
	  if (Math.abs(previousFitness)<=Math.abs(newFitness))
	    {
	      deltaE    =  Math.abs(newFitness) - Math.abs(previousFitness) ;
	      threshold = Math.exp(-K*deltaE/temperature); 
/*	      threshold = Math.exp(-(newFitness/previousFitness-1.0)/temperature);*/
	     
	      rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
	    //  System.out.println("deltaE="+deltaE+",threshold="+threshold+", temperature="+temperature+", rndNumber="+rndNumber);
	      if(rndNumber<threshold)
		{
		  parent1.setChromosome((int[])(tmpIndi.getChromosome()));
		  parent1.setFitness(newFitness);
		  // System.out.println("Accepting worst");
		  previousFitness = newFitness;
		}
	      else
		{
		  //  System.out.println("Rejecting worst");
		  tmpIndi.setChromosome((int[])parent1.getChromosome());
		  tmpIndi.setFitness(parent1.getFitness());
		  previousFitness = parent1.getFitness();
		}
	      
	    }
	  else
	    { /* accept configuration because it is better */
	      parent1.setChromosome((int [])(tmpIndi.getChromosome()));
	      parent1.setFitness(newFitness);
	      //  System.out.println("Accepting Better");
	      previousFitness = newFitness;
	    }
	}

      return parent1;
      
    }


 public Individual individualLocalSearch(Individual parent1, double temperature,double distances[][],FitnessVisitor aFitnessVisitor)
 
    {


      long    rndMut; 
      double  previousFitness;
      double  newFitness;
       double  K=0.01;// this was the original for steady state;
      // double  K=1.0;
      //double  K = 0.0000461; //for generational
      // double K;
      double  threshold;
      double  deltaE;
      double  rndNumber;
      int     numberChanges;
      int     numberConsecutiveChanges;
      int     i;
      int     j;
      TourIndividual tmpIndi;       
      int inversionSize;
      double size;



      tmpIndi         = (TourIndividual)(parent1.copyIndividual());
      size            = tmpIndi.getSize();
      previousFitness = tmpIndi.getFitness();
  //    K               = size/previousFitness;
     


   
     numberChanges = 1 ; //M2912-1
     numberConsecutiveChanges = 1;
     //    System.out.println("#C="+numberChanges+", #CC"+numberConsecutiveChanges);
     
      for (i=0;i<numberChanges;i++)
	{
	  for(j=0;j<numberConsecutiveChanges;j++)
	    {
	     twoChange(tmpIndi,distances);
	     // twoSwap(tmpIndi);
	    }
	  tmpIndi.acceptVisitor(aFitnessVisitor);
	  newFitness = tmpIndi.getFitness();
	  /* Boltzman acceptance */
	  if (Math.abs(previousFitness)<=Math.abs(newFitness))
	    {
	      deltaE    =  Math.abs(newFitness) - Math.abs(previousFitness) ;
	      threshold = Math.exp(-K*deltaE/temperature); 
/*	      threshold = Math.exp(-(newFitness/previousFitness-1.0)/temperature);*/
	     
	      rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
	    //  System.out.println("deltaE="+deltaE+",threshold="+threshold+", temperature="+temperature+", rndNumber="+rndNumber);
	      if(rndNumber<threshold)
		{
		  parent1.setChromosome((int[])(tmpIndi.getChromosome()));
		  parent1.setFitness(newFitness);
		  // System.out.println("Accepting worst");
		  previousFitness = newFitness;
		}
	      else
		{
		  //  System.out.println("Rejecting worst");
		  tmpIndi.setChromosome((int[])parent1.getChromosome());
		  tmpIndi.setFitness(parent1.getFitness());
		  previousFitness = parent1.getFitness();
		}
	      
	    }
	  else
	    { /* accept configuration because it is better */
	      parent1.setChromosome((int [])(tmpIndi.getChromosome()));
	      parent1.setFitness(newFitness);
	      //  System.out.println("Accepting Better");
	      previousFitness = newFitness;
	    }
	}

      return parent1;
      
    }



 // does not implement
 public  Individual individualLocalSearch(Individual parent1, double temperature, long distances[][],FitnessVisitor aFitnessVisitor)
 {
  return null;
 }




 private void cityInsertion(TourIndividual parent1, double dist[][])
   {
      int   c1[];
      int   rndPoint1;
      int   rndPoint2;
      int   tmp; 
      int   i;
      double dPlus  = 0.0;
      double dMinus = 0.0;
      
       

      c1       = (int[])(parent1.getChromosome());
      rndPoint1 = (int) MAFRA_Random.nextUniformLong(1,(long)(parent1.getSize()-1));
      rndPoint2 = (int) MAFRA_Random.nextUniformLong(1,(long)(parent1.getSize()-1));

      if(rndPoint2<rndPoint1)
	{
	  tmp       = rndPoint1;
	  rndPoint1 = rndPoint2;
	  rndPoint2 = tmp;
	}

      	
      tmp = c1[rndPoint2];
      for(i=rndPoint2;i>rndPoint1;i--)
	{
	  c1[i] = c1[i-1];
	}
      c1[rndPoint1]=tmp;


      parent1.setChromosome(c1);



   }

 private void twoChange(TourIndividual indi, double dist[][])
   {
     int startInversion;
     int endInversion;
     int maxI;
     int size;
     int tmp;
     int c[];
     int i;
     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion =(int)MAFRA_Random.nextUniformLong(0,size-2);
     endInversion =(int)MAFRA_Random.nextUniformLong(0,size-1);
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     maxI = (int)((endInversion-startInversion)/2);
     for(i=0;i<maxI;i++)
     {
      tmp=c[startInversion+i];
      c[startInversion+i]=c[endInversion-i];
      c[endInversion-i]=tmp;
     }


     indi.setChromosome(c);
   }



 private void twoChangeAtI(TourIndividual indi, double dist[][],int i)
   {
     int startInversion;
     int endInversion;
     int size;
     int tmp;
     int c[];

     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion = i;
     endInversion   = i+1;
     if(endInversion>size) endInversion=0;
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }


     for (i=startInversion;i<=endInversion;i++)
       {
	 tmp=c[endInversion-i];
	 c[endInversion-i]=c[i];
	 c[i]=tmp;
       }

     indi.setChromosome(c);
   }


 private void twoChange(TourIndividual indi, double dist[][],int i,int inversionSize)
   {
     int startInversion;
     int endInversion;
     int size;
     int tmp;
     int c[];

     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion = i;
     endInversion   = i+inversionSize;

     if(endInversion>size) endInversion=0;
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     // System.out.println("Inverting from "+ startInversion + " to "+endInversion);
     for (i=startInversion;i<=endInversion;i++)
       {
	 tmp=c[endInversion-i];
	 c[endInversion-i]=c[i];
	 c[i]=tmp;
       }

     indi.setChromosome(c);
   }


/* This local search swaps two cities (not to links as twoChange)*/
private void twoSwap(TourIndividual parent1)
{
    int  c1[];
      int   rndPoint1;
      int   rndPoint2;
      int  rndMut; 
      
       

      c1       = (int[])(parent1.getChromosome());
      rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));
      rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));

	  rndMut        = c1[rndPoint1];
	  c1[rndPoint1]  = c1[rndPoint2];
	  c1[rndPoint2]= rndMut;

	


      parent1.setChromosome(c1);



      
}




 public Individual individualLocalSearch(Individual parent1, double temperature, double sigma, double popSize,FitnessVisitor aFitnessVisitor)
{
 return null;
}




}
