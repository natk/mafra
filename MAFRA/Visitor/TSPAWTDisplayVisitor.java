




package MAFRA.Visitor;

import java.awt.*;
import MAFRA.Population_Individual.*;



/** This Visitor class is responsable for moving through the population and displaying each  individual.

    @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */
public class TSPAWTDisplayVisitor extends AWTDisplayVisitor {

  protected Frame myFrame;
  protected Frame myTextAreaFrame;
  public static TextArea myTextArea;
  protected Canvas myCanvas;
  protected double  myMinX;
  protected double  myMaxX;
  protected double  myMinY;
  protected double  myMaxY;
  protected boolean displayAll;
  protected boolean displayAny;
  protected double citiesPositions[][];
  protected int sizeX;
  protected int sizeY;

  public TSPAWTDisplayVisitor(int anX, int anY,double minX, double maxX, double minY, double maxY, boolean all,boolean show,double pos[][])
    {
      super();

      displayAll = all; /* If this is true it will display all the tours within the population.
			   If false just the best member .
			*/
      displayAny = show; /* If this is true it will display path and tour accordingly to displayAll setting.
                            If false no displaying will be performed.
			 */
      myMinX = minX;
      myMaxX = maxX;
      myMinY = minY;
      myMaxY = maxY;
      sizeX  = anX;
      sizeY  = anY;
      citiesPositions = pos;


      // myTextAreaFrame = new Frame("Evolving Tours, displaying distances:");
      // myTextAreaFrame.setSize(2*sizeX+20,2*sizeY+20);
      // myTextArea = new TextArea();
      // myTextArea.setSize(2*sizeX,2*sizeY);
      // myTextAreaFrame.add(myTextArea);

      myFrame = new Frame("Evolving Tours, displaying paths:");
      myFrame.setSize(sizeX+20,sizeY+20);
      myCanvas = new Canvas();
      myCanvas.setSize(sizeX+20,sizeY+20);
      myCanvas.setBackground(Color.black);
      myFrame.add(myCanvas);

      myFrame.show();
      // myTextAreaFrame.show();

    }

  public void visitIndividual(Individual anIndividual)
    {
      // anIndividual.display(myTextArea);
      ((TourIndividual)anIndividual).display(myCanvas,citiesPositions,myMinX,myMaxX,myMinY,myMaxY);
    };


  public void visitPopulation(Population aPopulation)
    {
     Individual anIndividual;
     long i;

     if(displayAny)
     {
      // myTextArea.insert("Population "+aPopulation.getName()+"|"+aPopulation.getMembersNumber()+"| \n",myTextArea.getCaretPosition());
      i=0;
      deleteMyCanvasBackground();
      if (displayAll)
    	{
    	  for( aPopulation.first( ); aPopulation.isInList( ); aPopulation.advance( ) )
    	    {
    	      anIndividual =(Individual)(aPopulation.retrieve());
    	      // myTextArea.append("-----["+i+"]:");
    	      anIndividual.acceptVisitor(this);
    	      i = i+1;
    	      deleteMyCanvasBackground();
    	    }
    	}
          else
    	{
    	  aPopulation.first( );
    	  anIndividual =(Individual)(aPopulation.retrieve());
    	  // myTextArea.append("-----["+i+"]:");
    	  anIndividual.acceptVisitor(this);
    	  i = i+1;

    	}
      }
      //      myCanvas.paint();

    };

    public void close()
      {
      	// myTextAreaFrame.setVisible(false);
      	// myTextAreaFrame.dispose();
      	myFrame.setVisible(false);
       	myFrame.dispose();
      }


    private void deleteMyCanvasBackground()
      {
      	Graphics g;

      	g=myCanvas.getGraphics();
      	g.setColor(Color.black);
      	g.fillRect(0,0,myCanvas.getSize().width,myCanvas.getSize().height);
      }

}
