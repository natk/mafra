


package MAFRA.Visitor;

import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;
import MAFRA.AbstractPlan.*;
import MAFRA.Util.*;


/** This Visitor class is responsable for moving through the population and compute fitness of each individual as a function of fitnesses in the second population (coEvolvedPopulation).
Also provides a means to visit a particular individual. Mantains the data of the max, min, avg fitnesses.
    @version Memetic Algorithms Framework - V 1.0 - Setember 1999
    @author  Natalio Krasnogor
    */

public class CoEvolutionaryFitnessVisitor extends FitnessVisitor {


  private Population     coEvolvedPopulation;

  public CoEvolutionaryFitnessVisitor(ProblemFactory aProblem)
    {
      super(aProblem);
    }

  /** This method sets the population that is used to measure the fitnesses of the
      individual/population whose fitness is updated.
      This is the "background population" not the active one
      for which the fitness is being measured. The population is obtained from the Plan instance passed
      as argument. It will use the previousGenerationPopulation from Plan, hence keepPrevious() method
      must be invoked on the plan somewhere else.*/
  public void setCoEvolvedPlan(Plan aPlan)
    {
      coEvolvedPopulation = aPlan.getPreviousGenerationPopulation();
    }


  public void visitIndividual(Individual anIndividual)
    {
      double myAbsoluteFitness;
      double A;
      double r;
      long   betterThan = 0;
      long   actualSampled = 1;
      Individual tmp;
      DisplayVisitor aDP;

      /*aDP = new DisplayVisitor();
       
      System.out.println("Evaluating:");
      anIndividual.acceptVisitor(aDP); */
      myAbsoluteFitness = theProblem.fitness(anIndividual);
   
      for(coEvolvedPopulation.first();coEvolvedPopulation.isInList();coEvolvedPopulation.advance())
	{
	  r = MAFRA_Random.nextUniformDouble(0.0,1.0);
	
 
	  if(r<0.25)
	    {
	    
	      tmp = (Individual) coEvolvedPopulation.retrieve();	 
	      if(theProblem.fitness(tmp)<myAbsoluteFitness)
		{
		  betterThan = betterThan +1;
		}
	      actualSampled = actualSampled +1;
	    }
	}
      A=(myAbsoluteFitness*(((double)betterThan/(double)actualSampled) + 1.0));
      anIndividual.setFitness(A);
    };

  public void visitPopulation(Population aPopulation)
    {
      Population membersOfPop;
      Individual anIndividual;

      membersOfPop = aPopulation;
      membersOfPop.resetFitnesses();
      for( membersOfPop.first( ); membersOfPop.isInList( ); membersOfPop.advance( ) )
        {
         anIndividual =(Individual)membersOfPop.retrieve();
         anIndividual.acceptVisitor(this);
	 aPopulation.updateMaxMinSumFitness(anIndividual);
        }
    
    };



}

