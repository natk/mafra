package MAFRA.Visitor;

import java.util.Vector;
import DataStructures.*;
import Supporting.*;

import MAFRA.Population_Individual.*;

/** This visitor sorts the population that receives it in decreassing order.
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor */
public class SortingVisitor extends Visitor{


 public void visitPopulation(Population aPopulation)
   {

    Supporting.Comparable []toBeSort;
    long i;
    long maxNum;

    maxNum =(long)( aPopulation.getMembersNumber());
    toBeSort = new Supporting.Comparable[(int)maxNum];

    i =0;
    for(aPopulation.first();aPopulation.isInList();aPopulation.advance())
      {
	      toBeSort[(int)i]=(Supporting.Comparable) ((Individual)(aPopulation.retrieve()).copyIndividual());
	      i++;
      }

    aPopulation.extinct();
    Sort.quicksort(toBeSort);
    for (i=0;i<maxNum;i++)
    {
	     aPopulation.addIndividual((Individual)(toBeSort[(int)i]));
    }



   }



}
