package MAFRA.Visitor;

import java.awt.*;
import javax.swing.JFrame;
import MAFRA.Population_Individual.*;



/** This Visitor class is responsable for moving through the population and displaying each  individual.

    @version Memetic Algorithms Framework - V 1.0 - December 2000
    @author  Natalio Krasnogor
    */
public class PFAWTDisplayVisitor extends DisplayVisitor {

  protected Frame myFrame;
  protected Frame myTextAreaFrame;
  public static TextArea myTextArea;
  protected Canvas myCanvas;
  protected boolean displayAll;
  protected boolean displayAny;
  protected int sizeX;
  protected int sizeY;
  private String theInstance;

  public PFAWTDisplayVisitor(boolean all,boolean show,String anInstance)
    {
      super();

      displayAll = all; /* If this is true it will display all the foldings within the population.
			   If false just the best member .
			*/
      displayAny = show; /* If this is true it will display energie and folding accordingly to displayAll setting.
                            If false no displaying will be performed.
			 */
      theInstance = new String(anInstance);

      sizeX  = 1000;
      sizeY  = 1000;



      // myTextAreaFrame = new Frame("Evolving Foldings, displaying energies:");
      // myTextAreaFrame.setSize(sizeX+20,sizeY+20);
      // myTextArea = new TextArea();
      // myTextArea.setSize(sizeX+20,sizeY+20);
      // myTextAreaFrame.add(myTextArea);

      myFrame = new JFrame("Evolving Foldings, displaying structures :");
      myCanvas = new Canvas();
      myCanvas.setSize(1000,1000);
      myCanvas.setBackground(Color.black);
      myFrame.add(myCanvas);
      myFrame.pack();
      myFrame.setVisible(true);



      // myTextAreaFrame.show();
      myFrame.show();


    }

  public void visitIndividual(Individual anIndividual)
    {
      // ((SelfAdaptingStaticPFStringIndividual)anIndividual).display(myTextArea);
      ((SelfAdaptingStaticPFStringIndividual)anIndividual).display(myCanvas,theInstance,true);
    };


  public void visitPopulation(Population aPopulation)
    {
     Individual anIndividual;
     long i;

     if(displayAny)
     {
      // myTextArea.insert("Population "+aPopulation.getName()+"|"+aPopulation.getMembersNumber()+"| \n",myTextArea.getCaretPosition());
      i=0;
      deleteMyCanvasBackground();
      if (displayAll)
    	{
    	 for( aPopulation.first( ); aPopulation.isInList( ); aPopulation.advance( ) )
    	    {
    	      anIndividual =(Individual)(aPopulation.retrieve());
    	      // myTextArea.append("-----["+i+"]:");
    	      anIndividual.acceptVisitor(this);
    	      i = i+1;
            // myCanvas.repaint();
    	      deleteMyCanvasBackground();
    	    }
    	}
      else
    	{
    	  aPopulation.first( );
    	  anIndividual =(Individual)(aPopulation.retrieve());
    	  // myTextArea.append("-----["+i+"]:");
    	  anIndividual.acceptVisitor(this);
    	  i = i+1;
        // myCanvas.repaint();
    	}
    }

  };

  public void close()
    {
    	// myTextAreaFrame.setVisible(false);
    	// myTextAreaFrame.dispose();
    	myFrame.setVisible(false);
     	myFrame.dispose();
    }

 private void deleteMyCanvasBackground()
    {
    	Graphics g;

    	g=myCanvas.getGraphics();
    	g.setColor(Color.black);
    	g.fillRect(0,0,myCanvas.getSize().width,myCanvas.getSize().height);
    }


}
