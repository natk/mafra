package MAFRA.Util;


import Supporting.*;

/** This is an auxiliary class that contains an Edge from city source to destiny and its length.*/
public class Edge implements Supporting.Comparable
{

 private int sourceCity;
 private int destinyCity;
 private double length;
 
 
 public Edge(int sCity, int dCity, double lengt)
 {
  sourceCity  = sCity;
  destinyCity = dCity;
  length      = lengt;
 }


 public int getSource()
 {
  return sourceCity;
 }
 
 public int getDestiny()
 {
  return destinyCity;
 }
 
 public double getLength()
 {
  return length;
 }
 
 
 
 
 /** This method is part of the Comparable interface.
     @param rhs another Edge to which to  compare the receiver's length.
     @return true if this object length is SMALLER than rhs's.*/
 public  boolean lessThan(Supporting.Comparable rhs)
   {
     return(length<((Edge)rhs).getLength());
   } 


   /** This method is part of the Comparable interface.
       @param rhs another Edgel to which to compare the receiverlength.
       @return 1 if the receiver length is smaller that rhs's lenght,-1 if it is BIGGER
               and 0 if both are equal.
       */
 public int compares(Supporting.Comparable rhs)
   {
     if (length<((Edge)rhs).getLength())
       {
	 return (1);
       }
      else
	{
	  if(length>((Edge)rhs).getLength())
	    {
	      return (-1);
	    }
	  else
	    {
	      return (0);
	    }
	}

   }




}
