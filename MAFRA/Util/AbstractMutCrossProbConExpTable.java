package MAFRA.Util;




import java.util.*;

import MAFRA.Mutation.*;
import MAFRA.CrossOver.*;


public class AbstractMutCrossProbConExpTable {

  private Object myTable[][];
  private int freeSlot;
  private int size;

 public AbstractMutCrossProbConExpTable(int numberOfItems)
   {
     myTable  = new Object[numberOfItems][7];
     /* myTable[i][0] contains a mutation.
        myTable[i][1] contains a mu probability.
	myTable[i][2] contains a crossover.
	myTable[i][3] contains a cro probability.
	myTable[i][4] contains the concentration of meme i (fixed in the static version = 1/numberOfItems).
	myTable[i][5] contains the expression value of meme i.
	myTable[i][6] contains the evolutionary activity of meme i. 
	*/
     freeSlot = 0;
     size     = numberOfItems;
   }


 public void addMeme(AbstractMutation aMut,double aProbMut,AbstractCrossOver aCross, double aProbCross)
   {
     if(freeSlot<size)
       {
	 myTable[freeSlot][0] = aMut;
	 myTable[freeSlot][1] = new Double(aProbMut);
	 myTable[freeSlot][2] = aCross;
	 myTable[freeSlot][3] = new Double(aProbCross);
	 myTable[freeSlot][4] = new Double(1.0/size); /* concentration is fixed in the static memes */
         myTable[freeSlot][5] = new Integer(0);
	 myTable[freeSlot][6] = new Double(0.0);
	 freeSlot++;
       }
     else
       {
	 System.out.println("ERROR - addMeme FULL");
       }
   }


 public double getMutationHelperProbability(int helperId)
   {
     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][1]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(-1.0);
       }
     
   }
 
 
 public AbstractMutation getMutationHelper(int helperId)
   {
     if(helperId<freeSlot)
       {
	 return ((AbstractMutation)myTable[helperId][0]);
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(null);
       }

   }

 public double getCrossOverHelperProbability(int helperId)
   {
     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][3]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(-1.0);
       }
     
   }
 
 
 public AbstractCrossOver getCrossOverHelper(int helperId)
   {
     if(helperId<freeSlot)
       {
	 return ((AbstractCrossOver)myTable[helperId][2]);
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(null);
       }

   }



 public void setConcentrationValue(int helperId,double aVal)
   {
     myTable[helperId][4] =  (Object)(new Double(aVal));
   }

 public double getConcentrationValue(int helperId)
   {

     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][4]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(-1.0);
       }

   }



 public void setExpressionValue(int helperId, int aVal)
   {
     myTable[helperId][5] =  (Object)(new Integer(aVal));
   }

 public int getExpressionValue(int helperId)
   {

     if(helperId<freeSlot)
       {
	 return ( ((Integer)myTable[helperId][5]).intValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(-1);
       }

   }



 public void setEvolutionaryActivityValue(int helperId,double aVal)
   {
     myTable[helperId][6] =  (Object)(new Double(aVal));
   }

 public double getEvolutionaryActivityValue(int helperId)
   {

     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][6]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMutCrossProbConExpTable size");
	 return(-1.0);
       }

   }


 
 public int getNumberOfHelpers()
   {
     return size;
   }
}


