package MAFRA.Util;

import MAFRA.Executor.*;
import MAFRA.AbstractPlan.*;
import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;

/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor

Instances of this subclass of Log, write (besides all of the inmediate superclass) the temperature of the
best individual in the population passed.
    */

public  class GenFitEvsFitTempLog extends GenFitEvsFitLog{



  public GenFitEvsFitTempLog(Plan aPlan,String aFileName,long aLogEveryGenerations, ProblemFactory aProb,String fileFormatComment)
    {
      super(aPlan,aFileName,aLogEveryGenerations,aProb,fileFormatComment);
    }

  public void execute()
    {
      String sToBePadd;
      Individual tmpIndi;

      if((myPlan.getGenerationsNow() % logEveryGenerations)==0)
    	{

    	  thePopToBeLogged.first();
    	  tmpIndi = thePopToBeLogged.retrieve();
    	  sToBePadd = " ";

    	  sToBePadd = sToBePadd + ((TourIndividual) tmpIndi).getTemperature();

    	  writeLog(sToBePadd);

    	}

    }


}
