package MAFRA.Util;

import MAFRA.Executor.*;
import MAFRA.AbstractPlan.*;
import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;

/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor

Instances of this subclass of Log, write (besides all of the inmediate superclass) the temperature of the
best individual in the population passed.
    */

public  class GenFitEvsFitTempDivLog extends GenFitEvsFitTempLog{



  public GenFitEvsFitTempDivLog(Plan aPlan,String aFileName,long aLogEveryGenerations, ProblemFactory aProb,String fileFormatComment)
    {
      super(aPlan,aFileName,aLogEveryGenerations,aProb,fileFormatComment);
    }

  public void execute()
    {
      String sToBePadd;
      Population tmpPop;
      Individual tmpIndi;


      tmpIndi=null;
      if((myPlan.getGenerationsNow() % logEveryGenerations)==0)
    	{
        thePopToBeLogged.first();
        tmpIndi = thePopToBeLogged.retrieve();
    	  sToBePadd = " ";
       //  sToBePadd = sToBePadd + ((SelfAdaptingStaticPFStringIndividual) tmpIndi).getTemperature()+" "+computeDiversity()+" "+(((SelfAdaptingStaticPFStringIndividual)tmpIndi).chromosomeToString());
    	 // sToBePadd = sToBePadd + ((SelfAdaptingStaticPFStringIndividual) tmpIndi).getTemperature()+" "+computeDiversity()+" "+ ((SelfAdaptingStaticPFStringIndividual) tmpIndi).getChromosome();
    	 sToBePadd = sToBePadd + ( tmpIndi).getTemperature()+" "+computeDiversity()+" "+ (tmpIndi).chromosomeToString();
    	 writeLog(sToBePadd);
    	}

	}



  private double computeDiversity()
    {
      Individual  tmpPopArray[];
      int         tmpPopArraySize;
      double      differentFitnesses=0.0;
      int         i,j;



      tmpPopArraySize = (int)thePopToBeLogged.getMembersNumber();
      tmpPopArray     = new Individual[tmpPopArraySize];
      i = 0;
      for(thePopToBeLogged.first();thePopToBeLogged.isInList();thePopToBeLogged.advance())
    	{
    	  tmpPopArray[i]=(Individual)(thePopToBeLogged.retrieve());
    	  i++;
    	}

      for(i=0;i<tmpPopArraySize;i++)
    	{
    	  for (j=0;j<tmpPopArraySize;j++)
    	    {
    	      if((tmpPopArray[i]!=null)&&(tmpPopArray[j]!=null)&&(i!=j))
    		{
    		  //	  System.out.println("f1="+tmpPopArray[i].getFitness()+",f2="+tmpPopArray[j].getFitness());
    		  if (tmpPopArray[i].getFitness()==tmpPopArray[j].getFitness())
    		    {

    		      tmpPopArray[j] = null;
    		    }

    		}
    	    }
    	  if(tmpPopArray[i]!=null)
    	    differentFitnesses = differentFitnesses+1.0;
    	}
      // System.out.println("differentFitnesses="+differentFitnesses);
      return (differentFitnesses/(double)(tmpPopArraySize+0.0));

    } /* of the method */




}/* of the class */
