//package DataStructures;
package MAFRA.Util;


import java.awt.*;
import java.util.Random;
import javax.swing.JFrame;
import java.util.ArrayList;
import DataStructures.*;

//This class will plot the Graph with the solution (if it exist) of the GCP (Graph Coloring Problem).
public class Drawer extends JFrame {
  private Graph graph;
  private static final long serialVersionUID = 1;
  //private Solution solution;
  private Color[] palette;
  private Point[] vertexPoints;
  private int[] chromosome;
  private final int vertexDiag;
  private final int vertexRad;
    
  public Drawer(Graph graph, int[] chromosome, int amountOfColors) {
    this.chromosome = chromosome;
    this.graph = graph;
    vertexDiag = 40;
    vertexRad = 20;
    setTitle("PLOT OF THE FINAL GRAPH");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
    setBackground(Color.white);
    //palette = new Color[solution.colors];
    palette = new Color[amountOfColors];
    //for (int i = 0; i < solution.colors; i++) {
    //JN
    for (int i = 0; i < amountOfColors; i++) {
      //palette[i] = Color.getHSBColor((float) i / solution.colors, 1, 1);
      //JN
      palette[i] = Color.getHSBColor((float) i / amountOfColors, 1, 1);
    }
    int margin = 25;
    int xm = getBounds().width - margin * 2;
    int ym = getBounds().height - margin * 2;
    Random rand = new Random();
    vertexPoints = new Point[graph.V()];
    //JN
    //vertexPoints = new Point[this.cantVertex];	
    for (int i = 0; i < vertexPoints.length; i++) {
      vertexPoints[i] = new Point(rand.nextInt(xm) + margin, rand.nextInt(ym) + margin);
      for (int j = 0; j < i; j++) {
        if (distance(vertexPoints[i], vertexPoints[j]) < vertexDiag) {
          i--;
          break;
        }
      }
    }
    setVisible(true);
  }
  
    
  private int distance(Point p1, Point p2) {
    return (int) Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
  }
    
  @Override
  public void paint(Graphics g) {
    g.setColor(Color.gray);
    
    for (int v = 0; v < graph.V(); v++) {
    //JN
    //for (int v = 0; v < this.cantVertex; v++) {	
      for (int w : graph.adj(v)) {
        g.drawLine(vertexPoints[v].x, vertexPoints[v].y, vertexPoints[w].x, vertexPoints[w].y);
      }
    }
    FontMetrics fm = g.getFontMetrics();
    int h = fm.getHeight() / 2;
    for (int v = 0; v < graph.V(); v++) {
    //JN
    //for (int v = 0; v < this.cantVertex; v++) {
      g.setColor(palette[chromosome[v]]);
      g.fillOval(vertexPoints[v].x - vertexRad, vertexPoints[v].y - vertexRad, vertexDiag, vertexDiag);
      g.setColor(Color.black);
      String s = String.valueOf(v + 1);
      g.drawString(s, vertexPoints[v].x - fm.stringWidth(s) / 2, vertexPoints[v].y + h);
      }

      }
}
