package MAFRA.Util;

public class XYPoint{

	private int x;
	private int y;
	private char monomer;
	private int diam;

	public XYPoint(int xValue, int yValue, char monomerValue, int aDiam){
		x = xValue;
		y = yValue;
		monomer = monomerValue;
		diam = aDiam;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	public char getMonomer(){
		return monomer;
	}
	
	public int getDiam()
	{ 
	 return diam;
	}
}
