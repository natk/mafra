



package MAFRA.Util;
import java.util.Random;


/** This class is responsible for providing all the services associated with random
numbers.

@version Memetic Algorithms Framework - V 1.0 - August 1999
@author  Natalio Krasnogor
*/
public class MAFRA_Random{

  private static long   mySeed;
  private static Random myRandom;

  public static void initialize()
    {
      myRandom = new Random();
    }

  public static void initialize(long aSeed)
    {
      myRandom = new Random(aSeed);
    }

  public static long nextUniformLong(long min, long max)
    {
     double dMin;
     double dMax;
     double tmp;
     Double res;

      dMin = min +0.0;
      dMax = max +0.0;
      tmp  = (dMax-dMin)*myRandom.nextDouble()+min;
      res  = new Double(tmp);
      /* CAREFUL: we are not rounding up nor down */
      return ( res.longValue());
    }


  public static double nextUniformDouble(double min, double max)
    {
     double dMin;
     double dMax;
     double res;

      dMin = min;
      dMax = max;
      res  = (dMax-dMin)*myRandom.nextDouble()+min;
      return ( res );
    }

}
