package MAFRA.Util;




import java.util.*;

import MAFRA.LocalSearcher.*;


public class AbstractLocalSearcherProbConExpTable extends AbstractMemeProbConExpTable {

public AbstractLocalSearcherProbConExpTable(int numberOfItems)
   {
    super(numberOfItems);
   }

 

 public void addMeme(AbstractLocalSearcher aLoc,double aLocProb)
   {
     if(freeSlot<size)
       {
	 myTable[freeSlot][0] = aLoc;
	 myTable[freeSlot][1] = new Double(aLocProb);
	 myTable[freeSlot][2] = null;
	 myTable[freeSlot][3] = null;
	 myTable[freeSlot][4] = new Double(1.0/size); /* concentration is fixed in the static memes */
         myTable[freeSlot][5] = new Integer(0);
	 myTable[freeSlot][6] = new Double(0.0);
	 freeSlot++;
       }
     else
       {
	 System.out.println("ERROR - addMeme FULL");
       }
   }


 public double getLocalSearcherHelperProbability(int helperId)
   {
     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][1]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractLocalSearcherProbConExpTable size");
	 return(-1.0);
       }
     
   }
 
 
 public AbstractLocalSearcher getLocalSearcherHelper(int helperId)
   {
     if(helperId<freeSlot)
       {
	 return ((AbstractLocalSearcher)myTable[helperId][0]);
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractLocalSearcherProbConExpTable size");
	 return(null);
       }

   }





}


