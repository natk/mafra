


package MAFRA.SelectionStrategy;

import MAFRA.Population_Individual.*;
import MAFRA.Executor.Executor;


/** This is the super class of all the Executors  used to select the next generation of
individuals.
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
   */
public class SelectionStrategyExecutor implements Executor{

  protected Population parentsPopulation;
  protected Population offspringsPopulation;
  protected long    mu;
  protected long     lambda;


  /** This executor is in charge of performing the selection of the next generation from parents
      and offsprings. Both population are stored as REFERENCES.*/
 public SelectionStrategyExecutor(Population aParentsPop,Population anOffspringsPop, long mU, long lAMBDA)
   {
    parentsPopulation    = aParentsPop;
    offspringsPopulation = anOffspringsPop;
    mu         = mU;
    lambda     = lAMBDA;
   }


 public void execute()
   {};

 public void setParentsPopulation(Population aPop)
   {
     parentsPopulation = aPop;
   }

 public void setOffspringssPopulation(Population aPop)
   {
     offspringsPopulation = aPop;
   }

 public void setMu(long mU)
   {
     mu = mU;
   }

 public void setLambda(long lAMBDA)
   {
     lambda = lAMBDA;
   }

 public Population getParentsPopulation()
   {
     return parentsPopulation;
   }

 public Population getOffspringsPopulation()
   {
     return offspringsPopulation;
   }

 public long getMu()
   {
     return mu;
   }

 public long getLambda()
   {
     return lambda;
   }



}
