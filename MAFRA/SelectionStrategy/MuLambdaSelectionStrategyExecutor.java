


package MAFRA.SelectionStrategy;

import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;

/** This class provides a (Mu,Lambda) Selection Strategy.
  @version Memetic Algorithms Framework - V 1.0 - August 1999
  @author  Natalio Krasnogor
  */
public class MuLambdaSelectionStrategyExecutor extends SelectionStrategyExecutor{


  public MuLambdaSelectionStrategyExecutor(Population aParentsPop,Population anOffspringsPop, long mU, long lAMBDA)
    {
      super(aParentsPop,anOffspringsPop,mU,lAMBDA);
    }

  public void execute()
    {
      Population tmpPop;
      SortingVisitor aSortingVisitor;

      aSortingVisitor = new SortingVisitor();
     // parentsPopulation.acceptVisitor(aSortingVisitor);
     //System.out.println("BEFORE(P): +F:"+parentsPopulation.getMaxFitness()+"##### -F:"+parentsPopulation.getMinFitness());
      parentsPopulation.extinct();
      parentsPopulation.resetFitnesses();
      offspringsPopulation.acceptVisitor(aSortingVisitor);
     //System.out.println("BEFORE(O): +F:"+offspringsPopulation.getMaxFitness()+"##### -F:"+offspringsPopulation.getMinFitness());
      offspringsPopulation.copyTo(parentsPopulation,lambda);
      parentsPopulation.acceptVisitor(aSortingVisitor);
     //System.out.println("AFTER(P):  +F:"+parentsPopulation.getMaxFitness()+"##### -F:"+parentsPopulation.getMinFitness());
	



    }


}
